<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Libraries\Services\VipParcel;

class VipParcelServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Libraries\Services\VipParcel', function ($app) {
            return new VipParcel();
          });
    }
}
