<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Libraries\KohanaHash\KohanaHasher as KohanaHasher;

class KohanaHashServiceProvider extends ServiceProvider 
{
  /**
   * Register the service provider.
   *
   * @return void
   */
  public function register() {
    $this->app->singleton('hash', function() {
      return new KohanaHasher();
    });
  }

  /**
   * Get the services provided by the provider.
   *
   * @return array
   */
  public function provides() {
    return array('hash');
  }

}