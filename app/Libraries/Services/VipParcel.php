<?php
namespace App\Libraries\Services;
use Illuminate\Support\Facades\Config;
use Ixudra\Curl\Facades\Curl;

class VipParcel
{

    //Calculate label price 
    public function calculateLabel($packageData)
    {
        $ApiKey = Config::get('vip-parcel.vipApiKey');
        $serviceUrl = Config::get('vip-parcel.VIP_PARCEL_SERVICE_URL');

        $response = Curl::to($serviceUrl.'shipping/label/calculate')
                        ->withData([
                            'authToken'=> $ApiKey, 
                            'labelType'=> $packageData['labelType'], 
                            'mailClass'=> $packageData['mailClass'],
                            'weightOz' => $packageData['weightOz'],
                            'recipientPostalCode' => $packageData['recipientPostalCode'],
                            'senderPostalCode' => $packageData['senderPostalCode']
                            ])
                        ->post();

        return(json_decode($response)); 
    }


    //Print package label 
    public function printLabel($packageData)
    {
        $ApiKey = Config::get('vip-parcel.vipApiKey');
        $serviceUrl = Config::get('vip-parcel.VIP_PARCEL_SERVICE_URL');
        $response = Curl::to($serviceUrl.'shipping/label/print')
                        ->withData([
                            'authToken'=> $ApiKey, 
                            'labelType'=> $packageData['labelType'], 
                            'mailClass'=> $packageData['mailClass'],
                            'weightOz' => $packageData['weightOz'],
                            'description' => $packageData['description'],
                            'deliveryConfirmation' => $packageData['deliveryConfirmation'],
                            'sender' => $packageData['sender'],
                            'recipient' =>$packageData['recipient'],
                            ])
                        ->post();
        // $response = '{
        //     "id": 24567,
        //     "value": 6.4,
        //     "trackNumber": "9499907123456123456781",
        //     "images": [
        //         "https://vipparcel.com/upload/labels/2014-04-15/e19f32f913f3.png"
        //     ],
        //     "statusCode": 201,
        //     "requestId": "hash"
        // }';                
        return(json_decode($response)); 
    }

    //Track Label
    public function trackLabel($trackNumber) {
        $ApiKey = Config::get('vip-parcel.vipApiKey');
        $serviceUrl = Config::get('vip-parcel.VIP_PARCEL_SERVICE_URL');
        $response = Curl::to($serviceUrl.'shipping/tracking/getInfo')
                        ->withData([
                            'authToken'=> $ApiKey, 
                            'trackNumber'=> $trackNumber
                            ])
                        ->get();
        // $response = '{
        //     "current": "Your item was delivered in AUSTRALIA at 9:50 am on April 20, 2013.",
        //     "history": [
        //         "Arrival at Post Office, April 19, 2013, 4:42 pm, AUSTRALIA",
        //         "Processed Through Sort Facility, April 19, 2013, 1:45 pm, AUSTRALIA",
        //         "Processed Through Sort Facility, April 16, 2013, 8:29 am, ISC MIAMI FL (USPS)",
        //         "Arrived at Sort Facility, April 15, 2013, 12:50 pm, ISC MIAMI FL (USPS)",
        //         "Shipment Accepted, April 11, 2013, 3:02 pm, PALM BAY, FL 23405"
        //     ],
        //     "statusCode": 200,
        //     "requestId": "vKT4uMvltdcUJLv"
        // }';                
        return(json_decode($response)); 
    }

}