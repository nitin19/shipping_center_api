<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Config;

use Closure;

class StoreOwnerAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check() && Auth::user()->role->id == 2 || Auth::check() && Auth::user()->role->id == 1)
        {
            return $next($request);
        }

        return response()->json(['error'=>'Permission Denied'], Config::get('response-code.UNAUTHORISED'));
       
    }
}
