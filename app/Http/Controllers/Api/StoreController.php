<?php

namespace App\Http\Controllers\Api;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Models\User; 
use App\Models\Store;
use App\Models\StoreOrder;
use App\Models\StoreOrderItem;
use App\Models\StoreSystem;
use App\Models\Notifications;
use App\Models\StoreOrderProcessor;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Config;
use Validator;
use URL;

/* Calling Marketplacetraits */
use App\Http\Services\Amazon;
use App\Http\Services\Ebay;
use App\Http\Services\MagentoService;
use App\Http\Services\Shopifyservice;

use Carbon\Carbon; 
use JWTFactory;
use JWTAuth;
use Log;
use App\Models\Userapistatistics;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;
use Session;
use Illuminate\Support\Facades\Redirect;

class StoreController extends Controller 
{

    use Amazon;
    use Ebay;
    use MagentoService;
    use Shopifyservice;

    /* check whether the seller credentials is correct */
    public function checkAccountStatus(Request $request){
        try{
            $data = $request->all();
            $storeIdentifier = $data['identifier'];
            $userid =  $data['userid'];
            /* cheking seller Id for amazon US credentials */
        
            if($storeIdentifier=='amazonus'){
                return $this->amusCheckAccountStatus($data);
            }
        } catch (\Exception $e) {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => $e->getMessage(),
            'status' => '0',
            'statusCode'    => 403
            ];
            return response()->json($response);
        }
        
    }

    /**
     * saving store credentials for all stores
     *
     *@params array [$identifier , $othercredentials as needed per store wise]  
     *@return Boolean
     */
    public function saveCredentials(Request $request){
        try{
            $data = $request->all();
            $storeIdentifier = $data['identifier'];
            $validator = Validator::make($request->all(), [ 
                'identifier' => 'required',
                'storeName' => 'required'
            ]);
            
            $validator->after(function($validator) use ($data){
                //Check if store name already exist
                $store = Store::where('store_name','=',$data['storeName'])
                                ->where('owner_id','=',$data['userid'])
                                ->first();  
                if($store) {
                    $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'message' => 'The Store is already registered with same name',
                    'status' => '0',
                    'statusCode'    => 204
                    ];
                    return response()->json($response);
                }                
            });  


            if ($validator->fails()) { 
                    $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'message' => 'Validation Error',
                    'status' => '0',
                    'statusCode'    => 204
                    ];
                    return response()->json($response);
            }

            /* saving amazon US credentials */
            if($storeIdentifier=='amazonus'){
                $accountStatus = $this->amusAccountStatus($data);
                if($accountStatus == 1) {
                 return $this->amusSaveStoreCredentials($data);
                } 
            }
            /* Saving Ebay US credentials */
            if($storeIdentifier=='ebayus'){
                return $this->eBaySaveStoreCredentials($data);
            }

            /* Saving Shopify US credentials */
            if($storeIdentifier=='shopify'){
                return $this->shopifyGetAuthrozieRedirectUrl($data);
            }

            if($storeIdentifier=='magento'){
                $validator = Validator::make($request->all(), [ 
                    'storeurl' => 'required',
                    'username' => 'required',
                    'password' => 'required'
                ]);
                if ($validator->fails()) {
                    $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'message' => 'The Store is already registered with same name',
                    'status' => '0',
                    'statusCode'    => 204
                    ];
                    return response()->json($response);
                } else {
                    return $this->MagentoSaveStoreCredentials($data);
                }
            }
        } catch (\Exception $e) {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => $e->getMessage(),
            'status' => '0',
            'statusCode'    => 403
            ];
            return response()->json($response);
        }
    }


    /**
     * Getting all store list per User 
     *
     *@params none;  
     *@return json (all store information)
     */
    public function myStores(Request $request){
        try{
        $ownerId = $request->userid;
        $queryStores = Store::query()->with('system')->with('orders');
        if($request->searchdata!=''){
             $queryStores->where('store_name', 'like', '%'.$request->searchdata.'%');
        }
        $queryStores->where('owner_id',$ownerId)->where('deleted','0')->orderby('id','DESC');
        $stores =  $queryStores->paginate(Config::get('constant.pagination')); 

        if(count($stores) > 0){
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'success' => $stores,
            'status' => '1',
            'statusCode'    => 200
            ];
            return response()->json($response);
        } else {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => 'no record found',
            'status' => '0',
            'statusCode'    => 204
            ];
            return response()->json($response);
        }
        } catch (\Exception $e) {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => $e->getMessage(),
            'status' => '0',
            'statusCode'    => 403
            ];
            return response()->json($response);
        }
                                        
        /*if ($stores->isEmpty()) {
            return response()->json(['error'=>'No Stores available.'],  Config::get('response-code.ERROR'));
        }
        return response()->json(['success' => $stores],  Config::get('response-code.SUCCESS'));*/
    }


    /**
     * pulling orders from the vendor websites
     *
     *@params varchar $slug of store;  
     *@return json [Boolenan ]
     */
    public function syncStores($slug, Request $request){
        try{
        /*$data = $request->all(); 
        $slug = $data['slug'];*/
        $userId = $request->userid;
        $returnValue = [];
        $storeInfo   = Store::with('system')->whereSlug($slug)->first();
        $prev_orders = StoreOrder::where(['status'=>'1','store_id'=>$storeInfo->id])->count();


       /* 1 is the store systemid of amazon US */
        if($storeInfo->system->id == 1){        
            $returnValue = $this->amusGetamazonorders($storeInfo->id);
            
        } elseif($storeInfo->system->id == 2){
            $returnValue = $this->ebayGetEbayOrders($storeInfo->id);
        } elseif($storeInfo->system->id == 3){
            $returnValue = $this->shopifyGetShopifyOrders($storeInfo->id);
        }
        elseif($storeInfo->system->id == 4){
            $returnValue = $this->ebayGetMagentoOrders($storeInfo->id);
        }

        $recent_orders = StoreOrder::where(['status'=>'1','store_id'=>$storeInfo->id])->count();
        $assign_processor = StoreOrderProcessor::where(['store_id'=>$storeInfo->id,'status'=>'1'])->get();

        if($recent_orders > $prev_orders){
            $message = "New Order is recieved";
            $notification = Notifications::insertGetId(['notify_type' => '0','type'=>'1', 'user_id' =>$userId ,'sender'=> $userId ,'receiver'=>$storeInfo->owner_id,'message'=> $message]);
            if(count($assign_processor) > 0){
                foreach ($assign_processor as $assigned_processor) {
                   $notification = Notifications::insertGetId(['notify_type' => '0','type'=>'1', 'user_id' =>$userId ,'sender'=> $userId ,'receiver'=>$assigned_processor->processor_id,'message'=> $message]);
                }
            }
        }

        return response()->json($returnValue);
        } catch (\Exception $e) {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => $e->getMessage(),
            'status' => '0',
            'statusCode'    => 403
            ];
            return response()->json($response);
        }
    }

    public function deletestore($storeSlug){
        try{
        $storeInfo = Store::where('slug', '=', $storeSlug)->first();
        if(empty($storeInfo)){
            return response()->json(['msg'=>'Invalid store']);
        }
        /*$storeorders = StoreOrder::where('order_status', '!=', 'completed')
                        ->where('store_id', '=', $storeInfo->id)
                        ->where('status', '=', '1')->get();*/
        $storeorders = StoreOrder::where('store_id', '=', $storeInfo->id)
                        ->where('status', '=', '1')->where(
                       function($query){
                            $query->whereNull('order_status');
                            $query->orWhere('order_status', '!=', 'completed');
                        })->get();
        if(count($storeorders) > 0){
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => 'Unable to delete Store. Store Orders are pending to proccessed',
            'statusCode'    => 204,
            'status' => 'Warning',
            ];
            return response()->json($response);
        } else {
            $updatestore = Store::where('id', $storeInfo->id)->update(['deleted' => '1']);
            if($updatestore){
                $response = [
                'requestId' => strtolower(Str::random(30)),
                'message' => 'Deleted Successfully',
                'status' => 'Success',
                'statusCode'    => 200
                ];
                return response()->json($response);
            } else {
                $response = [
                'requestId' => strtolower(Str::random(30)),
                'message' => 'Some Problem Exists',
                'status' => 'Error',
                'statusCode'    => 204
                ];
                return response()->json($response);
            }
        }
        } catch (\Exception $e) {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => $e->getMessage(),
            'status' => '0',
            'statusCode'    => 403
            ];
            return response()->json($response);
        }
        
    }

    //search store 
    /*
    * @param search keyword [string]
    * @return list of stores 
    */
    public function search(Request $request) {
        try{
        $input = $request->all();
        $ownerId = Auth::user()->id;
        $queryStores = Store::query();
        $queryStores->with('system');
        $queryStores->where('owner_id',$ownerId);

        if(!empty($input['keyword'])) {
            $queryStores->where('store_name','like','%'.$input['keyword'].'%');                 
        }
        $stores = $queryStores->paginate(Config::get('constant.pagination')); 

        if ($stores->isEmpty()) {
            return response()->json(['error'=>'No Stores available.'],  Config::get('response-code.ERROR'));
        }

        return response()->json(['success' => $stores],  Config::get('response-code.SUCCESS'));
        } catch (\Exception $e) {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => $e->getMessage(),
            'status' => '0',
            'statusCode'    => 403
            ];
            return response()->json($response);
        }
    }

    public function ebayUserToken(Request $request){
        try{
            $data = $request->all();
            $returnValue = $this->ebaySaveUserToken($data);

            return Redirect::away(env('SHIPPINGCENTER_FRONTEND_URL').'/viewmystore');
        } catch (\Exception $e) {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => $e->getMessage(),
            'status' => '0',
            'statusCode'    => 403
            ];
            return response()->json($response);
        }
    }

    public function shopifyUserToken(Request $request){
        try{
        $data = $request->all();
        return $returnValue = $this->shopifySaveUserToken($data);
        } catch (\Exception $e) {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => $e->getMessage(),
            'status' => '0',
            'statusCode'    => 403
            ];
            return response()->json($response);
        }
    }

    /********* Date 27-Dec-2018 *******************************/

    /**
     * Getting all store list per User 
     *
     *@params none;  
     *@return json (all store information)
     */
    public function processorStores(Request $request){
        try{
        //$processorId = Auth::user()->id;
        $processorId = $request->userid;
        $processor_storeid = StoreOrderProcessor::where('processor_id',$processorId)->get();
        foreach ($processor_storeid as $processorstoreid) {

            $store_id[] = $processorstoreid->store_id;
        }

        $queryStores = Store::query()->with('system');
            if($request->searchdata!=''){
                 $queryStores->where('store_name', 'like', '%'.$request->searchdata.'%');
            }
            $queryStores->whereIn('id',$store_id)->where('deleted','0')->orderby('id','DESC');
            $stores =  $queryStores->paginate(Config::get('constant.pagination')); 

   
        if(count($stores) > 0){
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'success' => $stores,
            'status' => '1',
            'statusCode'    => 200
            ];
            return response()->json($response);
        } else {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => 'no record found',
            'status' => '0',
            'statusCode'    => 204
            ];
            return response()->json($response);
        }
        } catch (\Exception $e) {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => $e->getMessage(),
            'status' => '0',
            'statusCode'    => 403
            ];
            return response()->json($response);
        }

    }

    /**
     * Getting all store list per processor 
     *
     *@params none;  
     *@return json (all store information)
     */

    public function proStores(Request $request){
        try{
        //$processorId = Auth::user()->id;
        $processorId = $request->userid;
        $processor_storeid = StoreOrderProcessor::where('processor_id',$processorId)->get();
        foreach ($processor_storeid as $processorstoreid) {

            $store_id[] = $processorstoreid->store_id;
        }
        $queryStores = Store::query()->with('system');
        $queryStores->whereIn('id', $store_id)->where('deleted','0')->orderby('id','DESC');
        $stores =  $queryStores->paginate(Config::get('constant.pagination'));

        if(count($stores) > 0){
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'success' => $stores,
            'status' => '1',
            'statusCode'    => 200
            ];
            return response()->json($response);
        } else {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => 'no record found',
            'status' => '0',
            'statusCode'    => 204
            ];
            return response()->json($response);
        }
                                        
        /*if ($stores->isEmpty()) {
            return response()->json(['error'=>'No Stores available.'],  Config::get('response-code.ERROR'));
        }
        return response()->json(['success' => $stores],  Config::get('response-code.SUCCESS'));*/
        } catch (\Exception $e) {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => $e->getMessage(),
            'status' => '0',
            'statusCode'    => 403
            ];
            return response()->json($response);
        }
    }



    /**
     * Getting all store list for store owner without pagination
     *
     *@params none;  
     *@return json (all store information)
     */

    public function allStores(Request $request){
        try{
        $owner_id = $request->userid;;

        $allstores  = Store::query()->where(['owner_id'=> $owner_id,'deleted'=>'0'])->get();

        $allstores = $allstores->toArray();

        if(count($allstores) > 0){
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'records' => $allstores,
            'status' => '1',
            'statusCode'    => 200
            ];
            return response()->json($response);
        } else {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'records' => 'no record found',
            'status' => '0',
            'statusCode'    => 204
            ];
            return response()->json($response);
        }
        } catch (\Exception $e) {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => $e->getMessage(),
            'status' => '0',
            'statusCode'    => 403
            ];
            return response()->json($response);
        }
    }

    public function getdetail(Request $request){
        try{
        //$owner_id = Auth::user()->id;
        $owner_id = $request->userid;
        $slug = $request->slug;

        $queryStores = Store::query()->with('system');
        $storedata = $queryStores->where('slug', '=', $slug)->first();

        if($storedata){
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'details' => $storedata,
            'status' => '1',
            'statusCode'    => 200
            ];
            return response()->json($response);
        } else {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'details' => 'no record found',
            'status' => '0',
            'statusCode'    => 204
            ];
            return response()->json($response);
        }
        } catch (\Exception $e) {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => $e->getMessage(),
            'status' => '0',
            'statusCode'    => 403
            ];
            return response()->json($response);
        }
    }
    
}


