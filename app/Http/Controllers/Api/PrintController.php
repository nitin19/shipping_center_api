<?php

namespace App\Http\Controllers\Api;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Models\User; 
use App\Models\Store;
use App\Models\StoreOrder;
use App\Models\StoreOrderItem;
use App\Models\StoreSystem;
use App\Models\StoreOrderProcessor;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Config;
use Validator;
use URL;

use Carbon\Carbon; 
use JWTFactory;
use JWTAuth;
use Log;
use App\Models\Userapistatistics;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;

class PrintController extends Controller 
{

    public function picklist(Request $request){
        if(!$user = JWTAuth::parseToken()->authenticate()) {
            return $response = [
            'requestId' => strtolower(Str::random(30)),
            'message'    => 'User not found',
            'statusCode' => 404,
            'status' => 0,
           ];
        } else {
            $usertype = $request->usertype;
            $orders = $request->orders;
            $searchdata = $request->searchdata;
            if($usertype=='US') {
                if($orders!='all'){
                    $storeOrdersIds = explode(',', $orders);
                } else {
                    $stores = Store::where('owner_id', '=', $user->id)->where('deleted', '=', '0')->get()->toArray();
                    $storeIds = array();
                    foreach($stores as $store){
                        array_push($storeIds, $store['id']);
                    }
                    $storeOrders = StoreOrder::whereIN('store_id', $storeIds)->where('order_status', '!=', 'completed')->get()->toArray();
                    $storeOrdersIds = array();
                    foreach($storeOrders as $storeOrder){
                        array_push($storeOrdersIds, $storeOrder['id']);
                    }
                }
                $StoreItemQuery = StoreOrderItem::groupBy('sellersku');
                                    $StoreItemQuery->selectRaw('sum(item_qty) as qtys, item_name, sellersku, weight, GROUP_CONCAT(store_order_id) as orderids');
                                    $StoreItemQuery->whereIN('store_order_id', $storeOrdersIds);
                                    $StoreItemQuery->where('item_status', '=', 'new');
                                    if($searchdata!=''){
                                        $StoreItemQuery->where(
                                           function($query) use ($searchdata){
                                                $query->where('item_name', 'like', '%'.$searchdata.'%');
                                                $query->orWhere('sellersku', 'like', '%'.$searchdata.'%');
                                            });
                                        }
                $storeOrderItems =  $StoreItemQuery->paginate(Config::get('constant.pagination'))->toArray();
                $PickLIstItems1 = array();
                if($storeOrderItems['data'] > 0){
                    foreach($storeOrderItems['data'] as $items){
                        $orderids = explode(',', $items['orderids']);
                        $address = array();
                        foreach($orderids as $orderid){
                            $orderdetail = StoreOrder::where('id', '=', $orderid)->first()->toArray();
                            array_push($address, $orderdetail['shipping_address']);
                        }
                        $itemsaddress  = implode(' and ', $address);
                        array_push($items, $items["address"] = $itemsaddress); 
                        $PickLIstItems1[] = $items; 
                    }
                } else {
                    $PickLIstItems1['data'] = '';
                }
                $PickLIstItems['data']     = $PickLIstItems1;
                $PickLIstItems['current_page']     = $storeOrderItems['current_page'];
                $PickLIstItems['first_page_url']   = $storeOrderItems['first_page_url'];
                $PickLIstItems['from']             = $storeOrderItems['from'];
                $PickLIstItems['last_page']        = $storeOrderItems['last_page'];
                $PickLIstItems['last_page_url']    = $storeOrderItems['last_page_url'];
                $PickLIstItems['next_page_url']    = $storeOrderItems['next_page_url'];
                $PickLIstItems['path']             = $storeOrderItems['path'];
                $PickLIstItems['per_page']         = $storeOrderItems['per_page'];
                $PickLIstItems['prev_page_url']    = $storeOrderItems['prev_page_url'];
                $PickLIstItems['to']               = $storeOrderItems['to'];
                $PickLIstItems['total']            = $storeOrderItems['total'];
                
                if(count($PickLIstItems) > 0){
                    $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'success' => $PickLIstItems,
                    'status' => '1',
                    'statusCode'    => 200
                    ];
                    return response()->json($response);
                } else {
                    $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'message' => 'no record found',
                    'status' => '0',
                    'statusCode'    => 204
                    ];
                    return response()->json($response);
                }
            } else {
                if($orders!='all'){
                    $stores = StoreOrderProcessor::where('processor_id', '=', $user->id)->where('status', '=', '1')->get()->toArray();
                    $storeIds = array();
                    foreach($stores as $store){
                        array_push($storeIds, $store['store_id']);
                    }
                    $storeOrders = StoreOrder::whereIN('store_id', $storeIds)->where('order_status', '!=', 'completed')->get()->toArray();
                    $storeOrdersIds = array();
                    foreach($storeOrders as $storeOrder){
                        array_push($storeOrdersIds, $storeOrder['id']);
                    }
                } else {
                    $storeOrdersIds = explode(',', $orders);
                }
                
                $StoreItemQuery = StoreOrderItem::groupBy('sellersku');
                                    $StoreItemQuery->selectRaw('sum(item_qty) as qtys, item_name, sellersku, weight, GROUP_CONCAT(store_order_id) as orderids');
                                    $StoreItemQuery->whereIN('store_order_id', $storeOrdersIds);
                                    $StoreItemQuery->where('item_status', '=', 'new');
                                    if($searchdata!=''){
                                        $StoreItemQuery->where(
                                           function($query) use ($searchdata){
                                                $query->where('item_name', 'like', '%'.$searchdata.'%');
                                                $query->orWhere('sellersku', 'like', '%'.$searchdata.'%');
                                            });
                                        }
                $storeOrderItems =  $StoreItemQuery->paginate(Config::get('constant.pagination'))->toArray();
                $PickLIstItems1 = array();
                if($storeOrderItems['data'] > 0){
                    foreach($storeOrderItems['data'] as $items){
                        $orderids = explode(',', $items['orderids']);
                        $address = array();
                        foreach($orderids as $orderid){
                            $orderdetail = StoreOrder::where('id', '=', $orderid)->first()->toArray();
                            array_push($address, $orderdetail['shipping_address']);
                        }
                        $itemsaddress  = implode(' and ', $address);
                        array_push($items, $items["address"] = $itemsaddress); 
                        $PickLIstItems1[] = $items; 
                    }
                } else {
                    $PickLIstItems1['data'] = '';
                }
                $PickLIstItems['data']     = $PickLIstItems1;
                $PickLIstItems['current_page']     = $storeOrderItems['current_page'];
                $PickLIstItems['first_page_url']   = $storeOrderItems['first_page_url'];
                $PickLIstItems['from']             = $storeOrderItems['from'];
                $PickLIstItems['last_page']        = $storeOrderItems['last_page'];
                $PickLIstItems['last_page_url']    = $storeOrderItems['last_page_url'];
                $PickLIstItems['next_page_url']    = $storeOrderItems['next_page_url'];
                $PickLIstItems['path']             = $storeOrderItems['path'];
                $PickLIstItems['per_page']         = $storeOrderItems['per_page'];
                $PickLIstItems['prev_page_url']    = $storeOrderItems['prev_page_url'];
                $PickLIstItems['to']               = $storeOrderItems['to'];
                $PickLIstItems['total']            = $storeOrderItems['total'];
                
                if(count($PickLIstItems) > 0){
                    $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'success' => $PickLIstItems,
                    'status' => '1',
                    'statusCode'    => 200
                    ];
                    return response()->json($response);
                } else {
                    $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'message' => 'no record found',
                    'status' => '0',
                    'statusCode'    => 204
                    ];
                    return response()->json($response);
                }
            }
         }
    }

    public function singlestorepicklist(Request $request){
        if(!$user = JWTAuth::parseToken()->authenticate()) {
            return $response = [
            'requestId' => strtolower(Str::random(30)),
            'message'    => 'User not found',
            'statusCode' => 404,
            'status' => 0,
           ];
        } else {
            $usertype = $request->usertype;
            $orders = $request->orders;
            $slug = $request->slug;
            $searchdata = $request->searchdata;
            if($usertype=='US') {
                if($orders!='all'){
                    $storeOrdersIds = explode(',', $orders);
                } else {
                    $storeInfo   = Store::with('system')->whereSlug($slug)->first();
                    $storeOrders = StoreOrder::where('store_id', '=', $storeInfo->id)->where('order_status', '!=', 'completed')->get()->toArray();
                    $storeOrdersIds = array();
                    foreach($storeOrders as $storeOrder){
                        array_push($storeOrdersIds, $storeOrder['id']);
                    }
                }
                $StoreItemQuery = StoreOrderItem::groupBy('sellersku');
                                    $StoreItemQuery->selectRaw('sum(item_qty) as qtys, item_name, sellersku, weight, GROUP_CONCAT(store_order_id) as orderids');
                                    $StoreItemQuery->whereIN('store_order_id', $storeOrdersIds);
                                    $StoreItemQuery->where('item_status', '=', 'new');
                                    if($searchdata!=''){
                                        $StoreItemQuery->where(
                                           function($query) use ($searchdata){
                                                $query->where('item_name', 'like', '%'.$searchdata.'%');
                                                $query->orWhere('sellersku', 'like', '%'.$searchdata.'%');
                                            });
                                        }
                $storeOrderItems =  $StoreItemQuery->paginate(Config::get('constant.pagination'))->toArray();
                $PickLIstItems1 = array();
                if($storeOrderItems['data'] > 0){
                    foreach($storeOrderItems['data'] as $items){
                        $orderids = explode(',', $items['orderids']);
                        $address = array();
                        foreach($orderids as $orderid){
                            $orderdetail = StoreOrder::where('id', '=', $orderid)->first()->toArray();
                            array_push($address, $orderdetail['shipping_address']);
                        }
                        $itemsaddress  = implode(' and ', $address);
                        array_push($items, $items["address"] = $itemsaddress); 
                        $PickLIstItems1[] = $items; 
                    }
                } else {
                    $PickLIstItems1['data'] = '';
                }
                $PickLIstItems['data']     = $PickLIstItems1;
                $PickLIstItems['current_page']     = $storeOrderItems['current_page'];
                $PickLIstItems['first_page_url']   = $storeOrderItems['first_page_url'];
                $PickLIstItems['from']             = $storeOrderItems['from'];
                $PickLIstItems['last_page']        = $storeOrderItems['last_page'];
                $PickLIstItems['last_page_url']    = $storeOrderItems['last_page_url'];
                $PickLIstItems['next_page_url']    = $storeOrderItems['next_page_url'];
                $PickLIstItems['path']             = $storeOrderItems['path'];
                $PickLIstItems['per_page']         = $storeOrderItems['per_page'];
                $PickLIstItems['prev_page_url']    = $storeOrderItems['prev_page_url'];
                $PickLIstItems['to']               = $storeOrderItems['to'];
                $PickLIstItems['total']            = $storeOrderItems['total'];
                
                if(count($PickLIstItems) > 0){
                    $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'success' => $PickLIstItems,
                    'status' => '1',
                    'statusCode'    => 200
                    ];
                    return response()->json($response);
                } else {
                    $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'message' => 'no record found',
                    'status' => '0',
                    'statusCode'    => 204
                    ];
                    return response()->json($response);
                }
            } else {
                if($orders!='all'){
                    $storeOrdersIds = explode(',', $orders);
                } else {
                    $storeInfo   = Store::with('system')->whereSlug($slug)->first();
                    $storeOrders = StoreOrder::where('store_id', '=', $storeInfo->id)->where('order_status', '!=', 'completed')->get()->toArray();
                    $storeOrdersIds = array();
                    foreach($storeOrders as $storeOrder){
                        array_push($storeOrdersIds, $storeOrder['id']);
                    }
                }
                
                $StoreItemQuery = StoreOrderItem::groupBy('sellersku');
                                    $StoreItemQuery->selectRaw('sum(item_qty) as qtys, item_name, sellersku, weight, GROUP_CONCAT(store_order_id) as orderids');
                                    $StoreItemQuery->whereIN('store_order_id', $storeOrdersIds);
                                    $StoreItemQuery->where('item_status', '=', 'new');
                                    if($searchdata!=''){
                                        $StoreItemQuery->where(
                                           function($query) use ($searchdata){
                                                $query->where('item_name', 'like', '%'.$searchdata.'%');
                                                $query->orWhere('sellersku', 'like', '%'.$searchdata.'%');
                                            });
                                        }
                $storeOrderItems =  $StoreItemQuery->paginate(Config::get('constant.pagination'))->toArray();
                $PickLIstItems1 = array();
                if($storeOrderItems['data'] > 0){
                    foreach($storeOrderItems['data'] as $items){
                        $orderids = explode(',', $items['orderids']);
                        $address = array();
                        foreach($orderids as $orderid){
                            $orderdetail = StoreOrder::where('id', '=', $orderid)->first()->toArray();
                            array_push($address, $orderdetail['shipping_address']);
                        }
                        $itemsaddress  = implode(' and ', $address);
                        array_push($items, $items["address"] = $itemsaddress); 
                        $PickLIstItems1[] = $items; 
                    }
                } else {
                    $PickLIstItems1['data'] = '';
                }
                $PickLIstItems['data']     = $PickLIstItems1;
                $PickLIstItems['current_page']     = $storeOrderItems['current_page'];
                $PickLIstItems['first_page_url']   = $storeOrderItems['first_page_url'];
                $PickLIstItems['from']             = $storeOrderItems['from'];
                $PickLIstItems['last_page']        = $storeOrderItems['last_page'];
                $PickLIstItems['last_page_url']    = $storeOrderItems['last_page_url'];
                $PickLIstItems['next_page_url']    = $storeOrderItems['next_page_url'];
                $PickLIstItems['path']             = $storeOrderItems['path'];
                $PickLIstItems['per_page']         = $storeOrderItems['per_page'];
                $PickLIstItems['prev_page_url']    = $storeOrderItems['prev_page_url'];
                $PickLIstItems['to']               = $storeOrderItems['to'];
                $PickLIstItems['total']            = $storeOrderItems['total'];
                
                if(count($PickLIstItems) > 0){
                    $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'success' => $PickLIstItems,
                    'status' => '1',
                    'statusCode'    => 200
                    ];
                    return response()->json($response);
                } else {
                    $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'message' => 'no record found',
                    'status' => '0',
                    'statusCode'    => 204
                    ];
                    return response()->json($response);
                }
            }
         }
    }

    public function packagingList(Request $request){
        $validator = Validator::make($request->all(), [ 
            'usertype' => 'required',
            'userid' => 'required'
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], Config::get('response-code.ERROR'));
        } else {
            $usertype = $request->usertype;
            $userid = $request->userid;
            if($usertype=='OP'){
                $userstores = StoreOrderProcessor::where('processor_id', '=', $userid)->where('status', '=', '1')->get(); 
                if(count($userstores) > 0){
                    $processorStoresIds = array();
                    foreach($userstores as $store){
                        array_push($processorStoresIds, $store->store_id);
                    }
                    $storeorders = StoreOrder::whereIn('store_id', $processorStoresIds)->where('status', '=', '1')->get();
                    $packaginglist = array();
                    $totalorderitems = 0;
                    if(count($storeorders) > 0){
                        foreach($storeorders as $storeorder){
                            $data = array();
                            $storeOrder = $storeorder->toArray();
                            $data['orderdetails'] = $storeOrder;
                            $StoreOrderItems = StoreOrderItem::where('store_order_id', $storeorder->id)->where('status', '=', '1')->where('item_status', '!=', 'completed')->get();
                            $orderItems = $StoreOrderItems->toArray();
                            $data['orderitems'] = $orderItems; 
                            $packaginglist[] = $data;
                            $totalorderitems = $totalorderitems + count($StoreOrderItems);
                        }
                    }

                    if (count($packaginglist) >0) {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'totalorders' => count($packaginglist),
                        'totalorderitems' => $totalorderitems,
                        'orders'    => $packaginglist,
                        'statusCode' => 200,
                       ];
                       return response()->json($response);
                    } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'No Order Found.',
                        'statusCode' => 204,
                       ];
                       return response()->json($response);
                    }
                } else {
                    return $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'message'    => 'No store exist',
                    'statusCode' => 204,
                    'status' => '0',
                   ];
                }
            } elseif($usertype=='US') {
                $userstores = Store::where('owner_id', '=', $userid)->where('status', '=', '1')->get();
                if(count($userstores) > 0){
                    $ownerStoresIds = array();
                    foreach($userstores as $store){
                        array_push($ownerStoresIds, $store->id);
                    }
                    $storeorders = StoreOrder::whereIn('store_id', $ownerStoresIds)->where('status', '=', '1')->get();
                    $packaginglist = array();
                    $totalorderitems = 0;
                    if(count($storeorders) > 0){
                        foreach($storeorders as $storeorder){
                            $data = array();
                            $storeOrder = $storeorder->toArray();
                            $data['orderdetails'] = $storeOrder;
                            $StoreOrderItems = StoreOrderItem::where('store_order_id', $storeorder->id)->where('status', '=', '1')->where('item_status', '!=', 'completed')->get();
                            $orderItems = $StoreOrderItems->toArray();
                            $data['orderitems'] = $orderItems; 
                            $packaginglist[] = $data;
                            $totalorderitems = $totalorderitems + count($StoreOrderItems);
                        }        

                    } 

                    if (count($packaginglist) > 0) {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'totalorders' => count($packaginglist),
                        'totalorderitems' => $totalorderitems,
                        'orders'    => $packaginglist,
                        'statusCode' => 200,
                       ];
                       return response()->json($response);
                    } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'No Order Found.',
                        'statusCode' => 204,
                       ];
                       return response()->json($response);
                    }
                } else {
                    return $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'message'    => 'No store exist',
                    'statusCode' => 204,
                    'status' => '0',
                   ]; 
                }
            } else {
                return $response = [
                'requestId' => strtolower(Str::random(30)),
                'message'    => 'User type not allowed',
                'statusCode' => 403,
                'status' => '0',
               ];
            }
        }
    }
    
}


