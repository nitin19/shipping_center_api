<?php

namespace App\Http\Controllers\Api;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Models\User; 
use App\Models\Store;
use App\Models\StoreOrder;
use App\Models\StoreOrderItem;
use App\Models\OrderStatus;
use App\Models\StoreSystem;
use App\Models\OrderProcessor;
use App\Models\StoreOrderProcessor;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Config;
use Validator;
use URL;

use Carbon\Carbon; 
use JWTFactory;
use JWTAuth;
use Log;
use App\Models\Userapistatistics;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;

class StoreOrderController extends Controller 
{
    /**
     * pulling orders from our databse for a given store ID
     *
     *@params int $storeId 
     *@return json [ $storeOrders , sucessStatus]
    */
    public function getStoreOrders($storeSlug, Request $request){
        $input = $request->all();
        $ownerId = $input['userid'];
        $storeInfo = Store::where('slug', '=', $storeSlug)->first();
        if(empty($storeInfo)){
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => 'Invalid store.',
            'status' => '0',
            'statusCode'    => 204
            ];
            return response()->json($response);
        }

        $queryOrders = StoreOrder::query()->with('store');
        if(!empty($input['order_status'])) {
            $queryOrders->where('order_status','=',$input['order_status']);
        }
        if(!empty($input['searchdata'])) {
            $queryOrders->where('order_details','like', '%'.$input['searchdata'].'%');
        }
        if(!empty($input['start_date']) && !empty($input['end_date'])) {
           $start_date = $input['start_date'];
           $end_date = $input['end_date'];
            $queryOrders->whereBetween('order_date', [$start_date, $end_date]);
        }
        $queryOrders->where('store_id', '=', $storeInfo->id)->orderby('id','DESC');
        $storeOrders =  $queryOrders->paginate(Config::get('constant.pagination'))->toArray();
        
        $storeOrders1 = array();
        if($storeOrders['data'] > 0){
            foreach($storeOrders['data'] as $items){
                $storeid = $items['store_id'];
                $orderprocessors = StoreOrderProcessor::where('store_id', '=', $storeid)->where('status', '=', '1')->get()->toArray();
                if(count($orderprocessors) > 0){
                    $processornames = array();
                    foreach($orderprocessors as $orderprocessor){
                        $processordetail = OrderProcessor::where('id', '=', $orderprocessor["processor_id"])->first();
                        $processorname = $processordetail->first_name.' '.$processordetail->last_name;
                        array_push($processornames, $processorname);
                    }
                } else {
                    $processornames = array();
                }
                $processorDetails = implode(', ', $processornames);
                array_push($items, $items["orderprocessors"] = $processorDetails);
                $storeOrders1[] = $items;
            }
        } else {
            $storeOrders1['data'] = '';
        }

        $Orders['data']     = $storeOrders1;
        $Orders['current_page']     = $storeOrders['current_page'];
        $Orders['first_page_url']   = $storeOrders['first_page_url'];
        $Orders['from']             = $storeOrders['from'];
        $Orders['last_page']        = $storeOrders['last_page'];
        $Orders['last_page_url']    = $storeOrders['last_page_url'];
        $Orders['next_page_url']    = $storeOrders['next_page_url'];
        $Orders['path']             = $storeOrders['path'];
        $Orders['per_page']         = $storeOrders['per_page'];
        $Orders['prev_page_url']    = $storeOrders['prev_page_url'];
        $Orders['to']               = $storeOrders['to'];
        $Orders['total']            = $storeOrders['total'];

        if(count($Orders) > 0){
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'success' => $Orders,
            'status' => '1',
            'statusCode'    => 200
            ];
            return response()->json($response);
        } else {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => 'No record found',
            'status' => '0',
            'statusCode'    => 204
            ];
            return response()->json($response);
        }  
    }

    /**
     * pulling orders from our databse for a given store ID
     *
     *@params int $storeId 
    *@return json [ $storeOrders , sucessStatus]
    */
    public function getAllStoreOrders(Request $request){
        $input = $request->all();
        $ownerId = $input['userid'];
        $queryStores = Store::query();

        $storeInfo =  $queryStores->where('owner_id',$ownerId)->get();
        $userStores = $storeInfo->toArray();
        $userStoresIds = array();
        if(count ($userStores) > 0){
            foreach($userStores as $userStore){
                array_push($userStoresIds, $userStore['id']);
            }
        }

        if(empty($storeInfo)){
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => 'No store added.',
            'status' => '0',
            'statusCode'    => 204
            ];
            return response()->json($response);
        }

        $queryOrders = StoreOrder::query()->with('store');
        if(!empty($input['order_status'])) {
            $queryOrders->where('order_status','=',$input['order_status']);
        }
        if(!empty($input['searchdata'])) {
            $queryOrders->where('order_details','like', '%'.$input['searchdata'].'%');
        }
        if(!empty($input['start_date']) && !empty($input['end_date'])) {
           $start_date = $input['start_date'];
           $end_date = $input['end_date'];
            $queryOrders->whereBetween('order_date', [$start_date, $end_date]);
        }
        if(!empty($input['store_name'])) {
            $queryOrders->where('store_id','=',$input['store_name'])->orderby('id','DESC');
        } else {
            $queryOrders->whereIn('store_id', $userStoresIds)->orderby('id','DESC');
        }
        $storeOrders =  $queryOrders->paginate(Config::get('constant.pagination'))->toArray();
        
        $storeOrders1 = array();
        if($storeOrders['data'] > 0){
            foreach($storeOrders['data'] as $items){
                $storeid = $items['store_id'];
                $orderprocessors = StoreOrderProcessor::where('store_id', '=', $storeid)->where('status', '=', '1')->get()->toArray();
                if(count($orderprocessors) > 0){
                    $processornames = array();
                    foreach($orderprocessors as $orderprocessor){
                        $processordetail = OrderProcessor::where('id', '=', $orderprocessor["processor_id"])->first();
                        $processorname = $processordetail->first_name.' '.$processordetail->last_name;
                        array_push($processornames, $processorname);
                    }
                } else {
                    $processornames = array();
                }
                $processorDetails = implode(', ', $processornames);
                array_push($items, $items["orderprocessors"] = $processorDetails);
                $storeOrders1[] = $items;
            }
        } else {
            $storeOrders1['data'] = '';
        }

        $Orders['data']     = $storeOrders1;
        $Orders['current_page']     = $storeOrders['current_page'];
        $Orders['first_page_url']   = $storeOrders['first_page_url'];
        $Orders['from']             = $storeOrders['from'];
        $Orders['last_page']        = $storeOrders['last_page'];
        $Orders['last_page_url']    = $storeOrders['last_page_url'];
        $Orders['next_page_url']    = $storeOrders['next_page_url'];
        $Orders['path']             = $storeOrders['path'];
        $Orders['per_page']         = $storeOrders['per_page'];
        $Orders['prev_page_url']    = $storeOrders['prev_page_url'];
        $Orders['to']               = $storeOrders['to'];
        $Orders['total']            = $storeOrders['total'];

        if(count($Orders) > 0){
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'success' => $Orders,
            'status' => '1',
            'statusCode'    => 200
            ];
            return response()->json($response);
        } else {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => 'No record found',
            'status' => '0',
            'statusCode'    => 204
            ];
            return response()->json($response);
        }

    }


    /* Get Stores items */
    public function getOrderDetails($orderId, Request $request) {
        $ownerId = $request->userid;

        $orderInfo = StoreOrder::find($orderId);
        if(empty($orderInfo)){
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => 'Invalid Order',
            'status' => '0',
            'statusCode'    => 204
            ];
            return response()->json($response);
        }

        $queryOrder = StoreOrder::query()->with('store')->with('orderprocessor')->with('orderItems');
        $orderDetails = $queryOrder->where('id', $orderId)->first();

        $queryOrderItems = StoreOrderItem::query()->with('order');
        if($request->searchdata!=''){
            $searchdata = $request->searchdata;
            $queryOrderItems->where(
                   function($query) use ($searchdata){
                        $query->where('item_name', 'like', '%'.$searchdata.'%');
                        $query->orWhere('sellersku', 'like', '%'.$searchdata.'%');
                    });
        }
        $queryOrderItems->where('store_order_id', $orderId)->orderby('id','DESC');
        $orderitems =  $queryOrderItems->paginate(Config::get('constant.pagination'));
        if(count($orderitems) > 0){
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'OrderItems' => $orderitems,
            'Order' =>$orderDetails,
            'status' => '1',
            'statusCode'    => 200
            ];
            return response()->json($response);
        } else {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => 'No Product Found',
            'status' => '0',
            'statusCode'    => 204
            ];
            return response()->json($response);
        }

    }


    /**
     * Search order by  pending orders (to be filled), partially complete orders, and completed orders
     *
     *@params int $storeId 
    *@return json [ $storeOrders , sucessStatus]
    */

    public function searchOrders(Request $request) {
        $input = $request->all();
        $ownerId = Auth::user()->id;
        $queryStores = Store::query();
        $storeInfo =  $queryStores->where('owner_id',$ownerId)->get();
        $userStores = $storeInfo->toArray();
        $userStoresIds = array();
        if(count ($userStores) > 0){
            foreach($userStores as $userStore){
                array_push($userStoresIds, $userStore['id']);
            }

        }

        if(empty($storeInfo)){
            return response()->json(['error'=>'No store added.'],  Config::get('response-code.ERROR'));
        }

        //$queryStoreOrder = StoreOrder::query();

        if(!empty($input['keyword'])) {
            $queryStoreOrder->where('order_status','=',$input['keyword']);
        }

         $queryOrders = StoreOrder::query()->with('store')->with('orderprocessor');
         if(!empty($input['store_name'])) {
            $queryStoreOrder->where('store_id','=',$input['store_name']);
        }
        $queryOrders->whereIn('store_id', $userStoresIds)->orderby('id','DESC');
        $storeOrders =  $queryOrders->paginate(Config::get('constant.pagination'));

       /* $storeOrders =  $queryStoreOrder->join('stores', 'store_orders.store_id','=', 'stores.id')
                                        ->where('stores.owner_id','=',$ownerId)
                                        ->select('store_orders.*','store_orders.id as OrderId','stores.store_name','stores.slug')
                                        ->paginate(Config::get('constant.pagination')); */

        if(empty($storeOrders)) {
            return response()->json(['error'=>'No Orders Found'], Config::get('response-code.ERROR'));
        }
        return response()->json(['success' =>$storeOrders], Config::get('response-code.SUCCESS'));  

    }  

    public function getOrderStatus(){

        $queryStores = OrderStatus::query()->get();

        if(empty($queryStores)) {
            return response()->json(['error'=>'No record Found'], Config::get('response-code.ERROR'));
        }
        return response()->json(['success' =>$queryStores], Config::get('response-code.SUCCESS'));  
    }

    /**
     * pulling orders from our database for a given store IDs for Order Processor
     *
     *@params int $storeIds 
    *@return json [ $storeOrders , sucessStatus]
    */
    public function processorStoreOrders(Request $request){
        $input = $request->all();
        $processorId = $input['userid'];
        $processor_storeid = StoreOrderProcessor::where('processor_id',$processorId)->get();
        foreach ($processor_storeid as $processorstoreid) {

            $store_id[] = $processorstoreid->store_id;
        }

        $queryOrders = StoreOrder::query()->with('store');
        if(!empty($input['order_status'])) {
            $queryOrders->where('order_status','=',$input['order_status']);
        }
        if(!empty($input['searchdata'])) {
            $queryOrders->where('order_details','like', '%'.$input['searchdata'].'%');
        }
        if(!empty($input['start_date']) && !empty($input['end_date'])) {
           $start_date = $input['start_date'];
           $end_date = $input['end_date'];
            $queryOrders->whereBetween('order_date', [$start_date, $end_date]);
        }
        if(!empty($input['store_name'])) {
            $queryOrders->where('store_id','=',$input['store_name'])->orderby('id','DESC');
        } else {
            $queryOrders->whereIn('store_id', $store_id)->orderby('id','DESC');
        }
        $storeOrders =  $queryOrders->paginate(Config::get('constant.pagination'));

        if(count($storeOrders) > 0){
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'success' => $storeOrders,
            'status' => '1',
            'statusCode'    => 200
            ];
            return response()->json($response);
        } else {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => 'No record found',
            'status' => '0',
            'statusCode'    => 204
            ];
            return response()->json($response);
        }

    }

    public function OrderItemDetail(Request $request){
        $itemid = $request->itemId;
        $queryOrderItem = StoreOrderItem::query()->with('order');
        $orderItemDetails = $queryOrderItem->where('id', $itemid)->first();
        $orderitem = $orderItemDetails->toArray();
        $queryOrders = StoreOrder::query()->with('store')->with('orderprocessor');
        $orderDetails = $queryOrders->where('id', $orderitem['store_order_id'])->first();
        $order = $orderDetails->toArray();
        $orderitemdetail = array_merge($orderitem, $order);
        $storeDetail = $orderitemdetail['store'];
        $queryStores = Store::query()->with('system');
        $StoreDetails = $queryStores->where('id', $storeDetail['id'])->first();
        $store = $StoreDetails->toArray();
        $data = array_merge($orderitemdetail, $store);
        if($data){
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'detail' => $data,
            'status' => '1',
            'statusCode'    => 200
            ];
            return response()->json($response);
        } else {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => 'Error',
            'status' => '0',
            'statusCode'    => 204
            ];
            return response()->json($response);
        }
        
    }

    public function updateorder(Request $request){
        $orderid = $request->orderid;
        $orderstatus = $request->orderstatus;
        $updateorder = StoreOrder::where('id', '=', $orderid)->update(['order_status' => $orderstatus]);
        if($updateorder){
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => 'Order status change successfully',
            'status' => '1',
            'statusCode'    => 200
            ];
            return response()->json($response);
        } else {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => 'Some problem exists',
            'status' => '0',
            'statusCode'    => 403
            ];
            return response()->json($response);
        }

    }

    public function updateorderItem(Request $request){
        $itemid = $request->itemid;
        $orderstatus = $request->orderstatus;
        $updateorder = StoreOrderItem::where('id', '=', $itemid)->update(['item_status' => $orderstatus]);
        if($updateorder){
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => 'Order status change successfully',
            'status' => '1',
            'statusCode'    => 200
            ];
            return response()->json($response);
        } else {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message' => 'Some problem exists',
            'status' => '0',
            'statusCode'    => 403
            ];
            return response()->json($response);
        }

    }

}