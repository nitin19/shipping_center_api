<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\OrderProcessor;
use App\Http\Resources\UserResource;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Hash;
use Carbon\Carbon; 
use JWTFactory;
use JWTAuth;
use Validator;
use Config;
use Log;
use Event;
use App\Events\UserRegistered;

use DB;
use Mail;
use App\Models\Country;
use App\Models\State;
use App\Models\Couponcode;
use App\Models\Setting;
use App\Models\Ipinfo;
use App\Models\Roles;
use App\Models\Referrals;
use App\Models\Apirequests;
use App\Models\Userapi;
use App\Models\Userapistatistics;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;


class AuthController extends Controller
{   
  public function __construct()
  {
   // $this->middleware('auth:api', ['except' => ['login', 'register']]);
    $this->nexmo_key  = Config::get('nexmo.api_key');
    $this->nexmo_secret = Config::get('nexmo.api_secret');
  }

  public function login(Request $request)
  {

    try {
    $headers = array();
    $headers[] = 'Authorization: '.$_SERVER['HTTP_AUTHORIZATION'];
    $url = env('VIPPARCEL_URL').'login';
    $userData = array("email" => $request->email, "password" => $request->password);
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $userData);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);


    $response = curl_exec($ch);
    $result = json_decode($response);
    curl_close($ch);

    if($result->errorMsg==''){
        return $response;
      } else {
        $credentials = $request->only('email', 'password');
          if($token = Auth::guard('orderprocesser')->attempt($credentials)){
              $auth_user = Auth::guard('orderprocesser')->user();
              $auth_user['type'] = 'OP';
              return response(compact('token', 'auth_user'), 200);
          } else{
            $errorMsg = 'Invalid credentials';
            return response(compact('errorMsg'));
          }
      }
    } catch (\Exception $e) {
        return response()->json(['status' => 0, 'error' => $e->getMessage()], 403);
    }  
  }

  public function register(Request $request)
  {
    try {
      $url = env('VIPPARCEL_URL').'register';
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $request->all());

      $response = curl_exec($ch);
      $data = json_decode($response,true);
      curl_close($ch);
      $auth_user = $data['auth_user'];
      $userid  = $data['auth_user']['id'];
      if($userid) {
        $userdata = array('username' => $auth_user['username'],'email' => $auth_user['email'], 'active' => $auth_user['active'], 'active_code' => $auth_user['active_code'], 'new_pass_code' => $auth_user['new_pass_code'], 'type' => $auth_user['type'], 'hear_about_us' => $auth_user['hear_about_us'], 'insurance_commission' => $auth_user['insurance_commission'], 'domestic_commission_additional' => $auth_user['domestic_commission_additional'], 'disable_check_print_label' => $auth_user['disable_check_print_label'], 'verified' => $auth_user['verified'], 'provider_account' => $auth_user['provider_account'], 'balance' => $auth_user['balance'], 'cnt_labels' => $auth_user['cnt_labels'], 'inheritance_ignore_fields' => $auth_user['inheritance_ignore_fields'], 'register_ipinfo_id' => $auth_user['register_ipinfo_id'], 'updated' => $auth_user['updated'], 'created' => $auth_user['created'], 'vip_user_id' => $auth_user['id']);
        $user = User::create($userdata);
        if($user){
            $token = $data['token'];
            return response()->json(compact('auth_user','token'),201);
        } else {
          return response()->json(['status' => 0, 'error' => 'User not saved.'], 403);
        }
      } else {
        return response()->json(['status' => 0, 'error' => 'User not registered'], 403);
      }
    } catch (\Exception $e) {
         return response()->json(['status' => 0, 'error' => $e->getMessage()], 403);
    }
    
  }

  public function smsVerify(Request $request)
  { 
  
    try {

    $url = env('VIPPARCEL_URL').'verify-sms'; 
    $userData = array("code" => $request->code);
    $headers = [
        'Authorization: '.$_SERVER['HTTP_AUTHORIZATION'],
     ];

    $curl = curl_init();
    curl_setopt_array($curl, array(
    CURLOPT_URL            => $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_POSTFIELDS => $userData,
    CURLOPT_CUSTOMREQUEST  => 'POST',
    CURLOPT_HTTPHEADER     => $headers
    ));
       
     $response = json_decode(curl_exec($curl));         
     $err = curl_error($curl);         
     curl_close($curl);  
     return response()->json($response, 200); 
    } catch (\Exception $e) {
         return response()->json(['status' => 'Error', 'error' => $e->getMessage()], 403);
    }

  }

  public function resendSms(Request $request)
  { 
    try {

    $url = env('VIPPARCEL_URL').'resend-sms'; 
  //  $userData = array("code" => $request->code);
    $headers = [
        'Authorization: '.$_SERVER['HTTP_AUTHORIZATION'],
     ];

    $curl = curl_init();
    curl_setopt_array($curl, array(
    CURLOPT_URL            => $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_POSTFIELDS => $request->all(),
    CURLOPT_CUSTOMREQUEST  => 'POST',
    CURLOPT_HTTPHEADER     => $headers
    ));
       
     $response = json_decode(curl_exec($curl));         
     $err = curl_error($curl);         
     curl_close($curl);  
     return response()->json($response, 200); 
    } catch (\Exception $e) {
         return response()->json(['status' => 'Error', 'error' => $e->getMessage()], 403);
    }
  
  }

  public function me()
  {
    $user = $this->guard()->user(); 
    return (new UserResource($user))->response()->setStatusCode(202);
  }

  public function logout()
  {
    $this->guard()->logout();

    return response()->json(['message' => 'Successfully logged out']);
  }

  /**
   * Refresh a token.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function refresh()
  {
    
    try {

      if (! $user = $this->guard()->user()) {
            return response()->json(['user_not_found'], 404);
        } 

        return $this->respondWithToken($this->guard()->refresh()); 

      } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

              return response()->json(['token_expired'], $e->getStatusCode());

      } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

              return response()->json(['token_invalid'], $e->getStatusCode());

      } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

              return response()->json(['token_absent'], $e->getStatusCode());
      }
  }

  /**
   * Get the token array structure.
   *
   * @param  string $token
   *
   * @return \Illuminate\Http\JsonResponse
   */
  protected function respondWithToken($token)
  {
    return response()->json([
      'access_token' => $token,
      'token_type' => 'bearer',
      'expires_in' => $this->guard()->factory()->getTTL() * 60
    ]);
  }

  /**
   * Get the guard to be used during authentication.
   *
   * @return \Illuminate\Contracts\Auth\Guard
   */
  public function guard()
  {
    return Auth::guard();
  }
}
