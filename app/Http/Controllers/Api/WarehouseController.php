<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Models\User; 
use App\Models\Warehouses;
use App\Models\Store; 
use App\Models\StoreOrder; 
use App\Models\StoreOrderItem; 
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Config;
use Validator;
use URL;

use Carbon\Carbon; 
use JWTFactory;
use JWTAuth;
use Log;
use Hash;
use DB;
use Image;
use App\Models\Userapistatistics;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Services\Projects;

class WarehouseController extends Controller 
{
  use Projects;

    public function saveWarehouse(Request $request) {

      try {
          $userid = $request->userid;
       /* if (! $user = JWTAuth::parseToken()->authenticate()) {
          return response()->json(['user_not_found'], 404);
        } else {
          $user_stores = Store::where('owner_id',$user->id)->get();*/
        if (! $userid) {
          return response()->json(['user_not_found'], 404);
        } else {
          $user_stores = Store::where('owner_id',$userid)->get();

          if(count($user_stores) > 0){
              $rules = [];
             /* $rules['warehouse_name']    = 'required|max:35|regex:/^[a-zA-Z_\s]+$/';
              $rules['warehouse_address'] = 'required|max:200|regex:/^[a-zA-Z0-9_\s]+$/';
              $rules['warehouse_city']    = 'required|regex:/^[a-zA-Z_\s]+$/';
              $rules['warehouse_state']   = 'required|regex:/^[a-zA-Z_\s]+$/';
              $rules['warehouse_country'] = 'required|regex:/^[a-zA-Z_\s]+$/';
              $rules['warehouse_phone']   = 'required|regex:/^[0-9]+$/|max:14|min:10';*/

              $validator = Validator::make($request->all(), $rules);

              if ($validator->fails()) { 
                  return response()->json(['error'=>$validator->errors()], Config::get('response-code.ERROR'));            
              }

              $store_owner_id    = $userid;
              $warehouse_name    = trim($request->warehouse_name);
              $warehouse_address = trim($request->warehouse_address);
              $warehouse_city    = trim($request->warehouse_city);
              $warehouse_state   = trim($request->warehouse_state);
              $warehouse_country = trim($request->warehouse_country);
              $warehouse_phone   = trim($request->warehouse_phone);

              $address = $warehouse_address.','.$warehouse_city;
              $prepAddr = str_replace(' ','+',$address);
              $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false&key=AIzaSyAmECi5THi1W3CHDdmC2bI6q6dDZNRAY-I');
              $output= json_decode($geocode);
              $warehouse_lat = $output->results[0]->geometry->location->lat;
              $warehouse_long = $output->results[0]->geometry->location->lng;

              if ($request->hasFile('warehouse_image')) {
               
                  if($request->file('warehouse_image')->isValid()) {
                      $file = $request->file('warehouse_image');
                      $image_name = time() . '.' . $file->getClientOriginalExtension();
                      $destinationPath = '/uploads/warehouseimage/';
                      $file->move(public_path(). $destinationPath, $image_name); 

                      $orderprocessor = Warehouses::insertGetId(
                                ['store_owner_id'=> $store_owner_id,'warehouse_name' => $warehouse_name, 'warehouse_address' =>$warehouse_address ,'warehouse_city'=> $warehouse_city ,'warehouse_state'=>$warehouse_state,'warehouse_country'=> $warehouse_country,'phone_number'=>$warehouse_phone,'warehouse_image'=>$image_name,'warehouse_lat'=>$warehouse_lat,'warehouse_long'=>$warehouse_long,'status'=>'1'] 
                      );
                      //$warehouse_data = Warehouses::where('id',$orderprocessor)->first();
                  } 
              } else {
                      $orderprocessor = Warehouses::insertGetId(
                            ['store_owner_id'=> $store_owner_id,'warehouse_name' => $warehouse_name, 'warehouse_address' =>$warehouse_address ,'warehouse_city'=> $warehouse_city ,'warehouse_state'=>$warehouse_state,'warehouse_country'=> $warehouse_country,'phone_number'=>$warehouse_phone,'warehouse_lat'=>$warehouse_lat,'warehouse_long'=>$warehouse_long,'status'=>'1'] 
                      );
                      //$warehouse_data = Warehouses::where('id',$orderprocessor)->first();
              }      
              //return response()->json(compact('warehouse_data'),201);
              return response()->json([
                                  'requestId' => strtolower(Str::random(30)),
                                  'statusCode' => 200,
                                  'status'=>'Success',
                                  'message'=>"Warehouse has been created successfully"
                                ]);
          } else {
              return response()->json([
                              'requestId' => strtolower(Str::random(30)),
                              'statusCode' => 403,
                              'status'=>'Warning',
                              'message'=>"You don't have any store. Please registered a store."
                            ]);
          } 
        }
      } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

              return response()->json(['token_expired'], $e->getStatusCode());

      } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

              return response()->json(['token_invalid'], $e->getStatusCode());

      } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

              return response()->json(['token_absent'], $e->getStatusCode());

      }
  }

  public function updateWarehouse(Request $request, $id) {
      try{
        $userid = $request->userid;
        /*if (! $user = JWTAuth::parseToken()->authenticate()) {
          return response()->json(['user_not_found'], 404);
        } else {*/
        if (! $userid) {
          return response()->json(['user_not_found'], 404);
        } else {
                $warehouseExists = Warehouses::where(['id'=> $id , 'status'=> 1])->first();
                if($warehouseExists){
                    $rules = [];
                    if(trim($request->warehouse_name)) {
                       // $rules['warehouse_name'] = 'required|max:35|regex:/^[a-zA-Z_\s]+$/|unique:warehouses';   
                        $warehouse_name = trim($request->warehouse_name);
                    } else {
                        $warehouse_name = $warehouseExists->warehouse_name;   
                    }

                    if(trim($request->warehouse_address)) {
                      //  $rules['warehouse_address'] = 'required|max:200|regex:/^[a-zA-Z0-9_\s]+$/'; 
                        $warehouse_address = trim($request->warehouse_address);  
                    } else {
                        $warehouse_address = $warehouseExists->warehouse_address;     
                    }

                    if(trim($request->warehouse_city)) {
                    // $rules['warehouse_city'] = 'required|regex:/^[a-zA-Z_\s]+$/'; 
                     $warehouse_city = trim($request->warehouse_city);  
                    } else {
                     $warehouse_city = $warehouseExists->warehouse_city;     
                    }

                    if(trim($request->warehouse_state)) {
                    // $rules['warehouse_state'] = 'required|regex:/^[a-zA-Z_\s]+$/'; 
                     $warehouse_state = trim($request->warehouse_state);  
                    } else {
                     $warehouse_state = $warehouseExists->warehouse_state;     
                    }

                    if(trim($request->warehouse_country)) {
                    // $rules['warehouse_country'] = 'required|regex:/^[a-zA-Z_\s]+$/'; 
                     $warehouse_country = trim($request->warehouse_country);  
                    } else {
                     $warehouse_country = $warehouseExists->warehouse_country;     
                    }
          
                    if(trim($request->phone_number)) {
                    // $rules['warehouse_phone'] = 'required|regex:/^[0-9]+$/|max:14|min:10'; 
                     $warehouse_phone = trim($request->phone_number);  
                    } else {
                     $warehouse_phone = $warehouseExists->phone_number;     
                    }

                    if(trim($request->warehouse_image)) {
                     $warehouse_image = trim($request->warehouse_image);  
                    } else {
                     $warehouse_image = $warehouseExists->warehouse_image;     
                    }

                    $address = $warehouse_address.''.$warehouse_city;
                    $prepAddr = str_replace(' ','+',$address);
                    $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false&key=AIzaSyAmECi5THi1W3CHDdmC2bI6q6dDZNRAY-I');
                    $output= json_decode($geocode);
                    $warehouse_lat = $output->results[0]->geometry->location->lat;
                    $warehouse_long = $output->results[0]->geometry->location->lng;

                    $validator = Validator::make($request->all(), $rules);

                    if ($validator->fails()) { 
                        return response()->json([
                                        'requestId' => strtolower(Str::random(30)),
                                        'statusCode' => 200,
                                        'status' => 'Warning',
                                        'message'=>$validator->errors()
                                        ]);            
                    }

                    if ($request->hasFile('warehouse_image')) {

                      $warehouse_data = Warehouses::where('id',$id)->first();
                      $warehouseimage = $warehouse_data->warehouse_image;
                      $image_path = public_path() . "/uploads/warehouseimage/".$warehouseimage;
                    
                      if($warehouseimage!='') {
                          $image_path = public_path() . "/uploads/warehouseimage/".$warehouseimage;
                           unlink($image_path);
                        }
                     
                      if($request->file('warehouse_image')->isValid()) {
                        $file = $request->file('warehouse_image');
                        $image_name = time() . '.' . $file->getClientOriginalExtension();
                        $destinationPath = '/uploads/warehouseimage/';
                        $file->move(public_path(). $destinationPath, $image_name); 

                        $warehouses = Warehouses::where('id',$id)->update(
                                      ['warehouse_name' => $warehouse_name, 'warehouse_address' =>$warehouse_address ,'warehouse_city'=> $warehouse_city ,'warehouse_state'=>$warehouse_state,'warehouse_country'=> $warehouse_country,'phone_number'=> $warehouse_phone,'warehouse_image'=>$image_name,'warehouse_lat'=>$warehouse_lat,'warehouse_long'=>$warehouse_long] 
                        );
                      } 
                    } else {
                        $warehouses = Warehouses::where('id',$id)->update(
                                      ['warehouse_name' => $warehouse_name, 'warehouse_address' =>$warehouse_address ,'warehouse_city'=> $warehouse_city ,'warehouse_state'=>$warehouse_state,'warehouse_country'=> $warehouse_country,'phone_number'=>$warehouse_phone,'warehouse_image'=>$warehouse_image,'warehouse_lat'=>$warehouse_lat,'warehouse_long'=>$warehouse_long] 
                        );
                    }
                    return response()->json([
                                        'requestId' => strtolower(Str::random(30)),
                                        'statusCode' => 200,
                                        'status'=>'Success',
                                        'message'=>"Warehouse successfully updated."
                            ]); 
                } else {
                      return response()->json([
                          'requestId' => strtolower(Str::random(30)),
                          'statusCode' => 403,
                          'status'=>'Warning',
                          'message'=>"Warehouse doesn't exits."
                      ]);  
                }
        }
    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

          return response()->json(['token_expired'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

          return response()->json(['token_invalid'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

          return response()->json(['token_absent'], $e->getStatusCode());
    }
  }

  public function getWarehouse(Request $request) {
      try{
        $userid = $request->userid;
        /*if (! $user = JWTAuth::parseToken()->authenticate()) {
          return response()->json(['user_not_found'], 404);
        } else {*/
        if (! $userid) {
          return response()->json(['user_not_found'], 404);
        } else {
                $searchdata = $request->searchdata;
                  $query = DB::table('warehouses');
                  if ($searchdata) { 
                     $keyword = $searchdata;
                     $query->where('warehouse_name', 'LIKE', '%' . $keyword . '%');
                     $query->orWhere('warehouse_address', 'LIKE', '%' . $keyword . '%');
                     $query->orWhere('warehouse_city', 'LIKE', '%' . $keyword . '%');
                     $query->orWhere('warehouse_state', 'LIKE', '%' . $keyword . '%');
                     $query->orWhere('warehouse_country', 'LIKE', '%' . $keyword . '%');
                     $query->orWhere('phone_number', 'LIKE', '%' . $keyword . '%');
                  }
                  $query->where('store_owner_id', $userid);
                  $query->where('status', 1);
                  $query->orderBy('id','DESC');
                  $warehousedata = $query->paginate(10);
                //$warehousedata = Warehouses::where(['store_owner_id'=>$user->id , 'status'=> 1])->orderby('id', 'DESC')->paginate(5);
                if(count($warehousedata)>0){
                  $warehouse_data = $warehousedata->toArray();
                    return response()->json([
                            'requestId' => strtolower(Str::random(30)),
                            'message'=>$warehouse_data,
                            'statusCode' => 200
                    ]);
                } else {
                      return response()->json([
                          'requestId' => strtolower(Str::random(30)),
                          'statusCode' => 403,
                          'status'=>'Warning',
                          'message'=>"No Records Found!"
                      ]);  
                }
        }
    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

          return response()->json(['token_expired'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

          return response()->json(['token_invalid'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

          return response()->json(['token_absent'], $e->getStatusCode());
    }
  }

  public function viewWarehouse(Request $request, $id) {
      try{
        $userid = $request->userid;
        /*if (! $user = JWTAuth::parseToken()->authenticate()) {
          return response()->json(['user_not_found'], 404);
        } else {*/
        if (! $userid) {
          return response()->json(['user_not_found'], 404);
        } else {
                $warehousedata = Warehouses::where(['id'=>$id , 'status'=> 1])->first();
                if($warehousedata!=''){
                  //$warehouse_data = $warehousedata->toArray();
                    return response()->json([
                            'requestId' => strtolower(Str::random(30)),
                            'message'=>$warehousedata,
                            'statusCode' => 200
                    ]);
                } else {
                      return response()->json([
                          'requestId' => strtolower(Str::random(30)),
                          'statusCode' => 403,
                          'status'=>'Warning',
                          'message'=>"No Records Found!"
                      ]);  
                }
        }
    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

          return response()->json(['token_expired'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

          return response()->json(['token_invalid'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

          return response()->json(['token_absent'], $e->getStatusCode());
    }
  }

  public function deleteWarehouse(Request $request, $id) {
      try{
        $userid = $request->userid;
        /*if (! $user = JWTAuth::parseToken()->authenticate()) {
          return response()->json(['user_not_found'], 404);
        } else {*/
        if (! $userid) {
          return response()->json(['user_not_found'], 404);
        } else {
                //$warehousesid = $request->id;
                $warehousedelete = Warehouses::where('id',$id)->update(['status'=>0]);
                if($warehousedelete){
                  return response()->json([
                            'requestId' => strtolower(Str:: random(30)),
                            'message'=>"Warehouse Successfully deleted",
                            'statusCode' => 200,
                            'status'=>'Success'
                    ]);
                } else {
                  return response()->json([
                            'requestId' => strtolower(Str:: random(30)),
                            'message'=>"No record found!",
                            'statusCode' => 200,
                            'status'=>'Warning'
                    ]);
                }
        }
    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

          return response()->json(['token_expired'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

          return response()->json(['token_invalid'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

          return response()->json(['token_absent'], $e->getStatusCode());
    }
  
  }

  public function getMyWarehousesCount(Request $request)
  {     
        //$user = Auth::user();
        //$userid = $user->id;
        $userid = $request->userid;

        $warehousescount = Warehouses::where(['store_owner_id' => $userid, 'status' => '1'])->count(); 
        $response = [
            'status' => true,
            'total_warehouses' => $warehousescount
           ];

        return response()->json($response);
    } 

}