<?php

namespace App\Http\Controllers\Api;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Models\User; 
use App\Models\Store;
use App\Models\StoreOrder;
use App\Models\StoreOrderItem;
use App\Models\StoreSystem;
use App\Models\StoreOrderProcessor;
use App\Models\Notifications;

use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Config;
use Validator;
use URL;

/* Calling Marketplacetraits */
use App\Http\Services\Amazon;
use App\Http\Services\Ebay;
use App\Http\Services\MagentoService;

use Carbon\Carbon; 
use JWTFactory;
use JWTAuth;
use Log;
use App\Models\Userapistatistics;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;
use Session;
use Illuminate\Support\Facades\Redirect;

class NotificationController extends Controller 
{


    /* check count of notifications for logged in users */

    public function getNotify(Request $request){

     try {

        $userid = $request->userid;
        $url   =  env('VIPPARCEL_URL').'user';
        $headers = [
            'Authorization: '.$_SERVER['HTTP_AUTHORIZATION'],
         ];
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL            => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST  => 'GET',
        CURLOPT_HTTPHEADER     => $headers
        ));
        $result = curl_exec($curl);
        $response = json_decode($result);

        $total_notification = Notifications::where(
                                ['receiver' => $userid, 'status' => '1', 'is_deleted' => '0', 'is_read' => '0', 'notify_type' => '0'])
                              ->count();

        $readNotification   = Notifications::query()->with('notification_types');
        $readnotifyDetails  = $readNotification->where(
                                ['receiver' => $userid, 'status' => '1','is_read' => '0', 'is_deleted' => '0', 'notify_type' => '0'])
                              ->orderby('notifications.id','DESC')
                              ->get(); 
        $readnotify         = $readnotifyDetails->toArray();


        $queryNotify        = Notifications::query()->with('notification_types');
        $notifyDetails      = $queryNotify->where(
                                ['receiver' => $userid, 'status' => '1', 'is_deleted' => '0', 'notify_type' => '0'])
                              ->orderby('notifications.id','DESC')
                              ->paginate(Config::get('constant.pagination')); 
        $notify             = $notifyDetails->toArray();

        $response = [
            'status' => '1',
            'statusCode'    => 200,
            'total_notification' => $total_notification,
            'read_notification' => $readnotify,
            'records' => $notify 
           ];

        return response()->json($response);

      } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

              return response()->json(['token_expired'], $e->getStatusCode());

      } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

              return response()->json(['token_invalid'], $e->getStatusCode());

      } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

              return response()->json(['token_absent'], $e->getStatusCode());

      }
      
    }

    /* Update notifications(marked as read) on click of "bell" icon */
    public function readNotify(Request $request){

     try {
        $userid = $request->userid;
        $readnotify = Notifications::where(
                        ['receiver' => $userid, 'is_deleted' => '0', 'is_read' => '0'])
                      ->update(['is_read'=>'1']);
       

        $response = [
            'status'        => '1',
            'statusCode'    => 200,
           ];

        return response()->json($response);

      } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

              return response()->json(['token_expired'], $e->getStatusCode());

      } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

              return response()->json(['token_invalid'], $e->getStatusCode());

      } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

              return response()->json(['token_absent'], $e->getStatusCode());

      }
    }
    
    /* Update notifications(marked as deleted) on click of "bell" icon */
    public function deleteNotify($id){

      try {

        $readnotify = Notifications::where(['id'=>$id])->update(['is_deleted'=>'1']);
      
        $response = [
            'status'        => '1',
            'statusCode'    => 200,
            'message'       => 'Record successfully deleted.'
           ];

        return response()->json($response);

      }catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

              return response()->json(['token_expired'], $e->getStatusCode());

      } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

              return response()->json(['token_invalid'], $e->getStatusCode());

      } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

              return response()->json(['token_absent'], $e->getStatusCode());

      }
      
    }


}


