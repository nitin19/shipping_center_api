<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\Models\User;
use App\Models\PasswordReset;
use Illuminate\Support\Facades\Hash;
use Validator;
use Config;
use Log;
use Event;

class PasswordResetController extends Controller
{
    /**
     * Create token password reset
     *
     * @param  [string] email
     * @return [string] message
     */
    public function create(Request $request)
    {

        try {
        $userData = array("email" => $request->email);
        $url   =  env('VIPPARCEL_URL').'password/create';
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL            => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST  => 'POST',
        CURLOPT_POSTFIELDS  => $userData,
        ));

        $result = curl_exec($curl);
        $data = json_decode($result);
        
          if($data){
            $responsemsg = [
            'message' => $data->message,
            'status' => 1,
            ];
            return response()->json($responsemsg);

          } else {
            $responsemsg = [
              'message' => 'Error',
              'status' => 0,
              ];
              return response()->json($responsemsg);
          }
        } catch (\Exception $e) {
            return response()->json(['status' => 0, 'error' => $e->getMessage()], 403);
        }
    }

    /**
     * Find token password reset
     *
     * @param  [string] $token
     * @return [string] message
     * @return [json] passwordReset object
     */
    public function find($token)
    {
      try{
        $url   =  env('VIPPARCEL_URL').'password/find/'.$token;
        $result = file_get_contents($url);
        $data = json_decode($result);
        if($data){
            $responsemsg = [
            'message' => $data->message,
            'status' => 1,
            ];
            return response()->json($responsemsg);

          } else {
            $responsemsg = [
              'message' => 'Error',
              'status' => 0,
              ];
              return response()->json($responsemsg);
          }
        } catch (\Exception $e) {
            return response()->json(['status' => 0, 'error' => $e->getMessage()], 403);
        }
    }

     /**
     * Reset password
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @param  [string] token
     * @return [string] message
     * @return [json] user object
     */
    public function reset(Request $request)
    {
      try{
        $userData = array("token" => $request->token, "email" => $request->email, "password" => $request->password);
        $url   =  env('VIPPARCEL_URL').'password/reset';

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL            => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST  => 'POST',
        CURLOPT_POSTFIELDS  => $userData,
        ));

        $result = curl_exec($curl);
        $data = json_decode($result);

        curl_close($curl);
        if($data){
              $responsemsg = [
              'message' => $data->message,
              'status' => $data->status,
              ];
              return response()->json($responsemsg);

            } else {
              $responsemsg = [
                'message' => 'Error',
                'status' => 0,
                ];
                return response()->json($responsemsg);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 0, 'error' => $e->getMessage()], 403);
        }
    }
}