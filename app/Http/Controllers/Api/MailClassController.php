<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Http\Resources\UserResource;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Hash;
use Carbon\Carbon; 
use JWTFactory;
use JWTAuth;
use Validator;
use Config;
use Log;
use Event;
use App\Events\UserRegistered;

use DB;
use App\Models\MailClass;
use App\Models\Country;
use App\Models\State;
use App\Models\Apirequests;
use App\Models\Userapi;
use App\Models\Userapistatistics;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;

class MailClassController extends Controller
{   

    public function get_Classes()
    {

        try {
        $url = env('VIPPARCEL_URL').'v1/shipping/label/mailClasses/';

        /*$opts = [
            "http" => [
                "method" => "GET",
                "header" => "Authorization: ".$_SERVER['HTTP_AUTHORIZATION'],
            ]
        ];*/

        //$context = stream_context_create($opts);
        $result = file_get_contents($url);
        $response = json_decode($result);
        if($response){
            if($response->statusCode==200){
                $mailClasses = array();
                foreach($response->records as $recordkey => $records){
                    if($recordkey=='domestic'){
                        foreach ($records as $key => $value) {
                           $mailClasses[] = array('sub_type_key' => $key, 'sub_type_title' => $value, 'type' => 'domestic');
                        }
                    } else {
                        foreach ($records as $key => $value) {
                           $mailClasses[] = array('sub_type_key' => $key, 'sub_type_title' => $value, 'type' => 'international');
                        }
                    }
                }

                $responsemsg = [
                'requestId' => $response->requestId,
                'status' => '1',
                'records'    => $mailClasses,
                'statusCode' => $response->statusCode,
                ];
                return response()->json($responsemsg);

            } else {
                $responsemsg = [
                'requestId' => $response->requestId,
                'status' => '0',
                'message'    => 'No record found',
                'statusCode' => $response->statusCode,
                ];
                return response()->json($responsemsg);
            }
        } else {
            $responsemsg = [
            'requestId' => strtolower(Str::random(30)),
            'message' => 'Page not found',
            'statusCode'    => 403,
            'status' => 0,
            ];
            return response()->json($responsemsg);
        }
        
        } catch (\Exception $e) {
         return response()->json(['status' => 0, 'message' => $e->getMessage()], 403);
        } 

    }
}