<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Http\Resources\UserResource;
use App\Http\Resources\ProfileResource;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Hash;
use Carbon\Carbon; 
use JWTFactory;
use JWTAuth;
use Validator;
use Config;
use Log;
use Event;
use App\Events\UserRegistered;

use DB;
use App\Models\Country;
use App\Models\State;
use App\Models\Apirequests;
use App\Models\Userapi;
use App\Models\Userapistatistics;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\Models\UserDetail;
use URL;

class UserController extends Controller
{   

 public function getAuthenticatedUser(Request $request)
    {
      try {

        $url   =  env('VIPPARCEL_URL').'user';
        $headers = [
            'Authorization: '.$_SERVER['HTTP_AUTHORIZATION'],
         ];
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL            => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST  => 'GET',
        CURLOPT_HTTPHEADER     => $headers
        ));
        $result = curl_exec($curl);
        $response = json_decode($result);
        return response()->json($response);
       // return (new UserResource($user))->response()->setStatusCode(202);
      
        } catch (\Exception $e) {
            return response()->json(['status' => 0, 'error' => $e->getMessage()], 403);
        }  
                    
    }

    public function put_update(Request $request)
    {
      try {
        $url = env('VIPPARCEL_URL').'update';
        $headers = [
            'Authorization: '.$_SERVER['HTTP_AUTHORIZATION'],
         ];

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL            => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST  => 'PUT',
        CURLOPT_POSTFIELDS  => http_build_query($request->all()),
        CURLOPT_HTTPHEADER     => $headers
        ));

        $result = curl_exec($curl);
        $data = json_decode($result);
        curl_close($curl);
        
        if($data){
          
            if($data->statusCode=200){
                $responsemsg = [
                'requestId' => $data->requestId,
                'id' => $data->id,
                'user_id' => $data->user_id,
                'statusCode'    => $data->statusCode,
                'status' => 1,
                ];
                return response()->json($responsemsg);
            } else {
                $responsemsg = [
                'requestId' => $data->requestId,
                'message' => $data->message,
                'statusCode'    => $data->statusCode,
                'status' => 0,
                ];
                return response()->json($responsemsg);
            }

        } else {
          $responsemsg = [
            'requestId' => strtolower(Str::random(30)),
            'message' => 'Error',
            'statusCode'    => 404,
            'status' => 0,
            ];
            return response()->json($responsemsg);
        }
      } catch (\Exception $e) {
            return response()->json(['status' => 0, 'error' => $e->getMessage()], 403);
        }

    }

    public function post_apitoken(Request $request)
    {
                
        try {
        $url = env('VIPPARCEL_URL').'requesttoken';
        $headers = [
            'Authorization: '.$_SERVER['HTTP_AUTHORIZATION'],
         ];

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL            => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST  => 'POST',
        CURLOPT_POSTFIELDS  => http_build_query($request->all()),
        CURLOPT_HTTPHEADER     => $headers
        ));

        $result = curl_exec($curl);
        $data = json_decode($result);
        curl_close($curl);
        
        if($data){
            $responsemsg = [
                'requestId' => $data->requestId,
                'message' => $data->message,
                'statusCode'    => $data->statusCode,
                'status' => 1,
                ];
                return response()->json($responsemsg);

        } else {
          $responsemsg = [
            'requestId' => strtolower(Str::random(30)),
            'message' => 'Error',
            'statusCode'    => 404,
            'status' => 0,
            ];
            return response()->json($responsemsg);
        }
      } catch (\Exception $e) {
            return response()->json(['status' => 0, 'error' => $e->getMessage()], 403);
        }
    }

    public function get_apitoken(Request $request)
    {
        try {
        $url = env('VIPPARCEL_URL').'gettoken';

        $opts = [
            "http" => [
                "method" => "GET",
                "header" => "Authorization: ".$_SERVER['HTTP_AUTHORIZATION'],
            ]
        ];

        $context = stream_context_create($opts);

        $result = file_get_contents($url, false, $context);
        $data = json_decode($result);
        
          if($data){
              if($data->statusCode=200){
                $responsemsg = [
                'requestId' => $data->requestId,
                'Approved' => $data->Approved,
                'Token' => $data->Token,
                'statusCode'    => $data->statusCode,
                'status' => 1,
                ];
                return response()->json($responsemsg);
            } else {
                $responsemsg = [
                'requestId' => $data->requestId,
                'message' => $data->message,
                'statusCode'    => $data->statusCode,
                'status' => 0,
                ];
                return response()->json($responsemsg);
            }

          } else {
            $responsemsg = [
              'requestId' => strtolower(Str::random(30)),
              'message' => 'Error',
              'statusCode'    => 404,
              'status' => 0,
              ];
              return response()->json($responsemsg);
          }
        } catch (\Exception $e) {
            return response()->json(['status' => 0, 'error' => $e->getMessage()], 403);
        }      
                    
        }

}