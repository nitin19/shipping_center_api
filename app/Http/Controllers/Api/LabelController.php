<?php

namespace App\Http\Controllers\Api;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Models\User; 
use App\Models\Store;
use App\Models\StoreOrder;
use App\Models\StoreOrderItem;
use App\Models\StoreSystem;
use App\Models\StoreOrderProcessor;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Config;
use Validator;
use URL;

use Carbon\Carbon; 
use JWTFactory;
use JWTAuth;
use Log;
use App\Models\Userapistatistics;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;
use EasyPost\Address;
use EasyPost\EasyPost;
use EasyPost\Shipment;
use EasyPost\Parcel;
use EasyPost\EasypostResource;
use EasyPost\Rate;

class LabelController extends Controller 
{

    public function calculate(Request $request){
        try {

            $url   =  env('VIPPARCEL_URL').'gettoken';
            $headers = [
                'Authorization: '.$_SERVER['HTTP_AUTHORIZATION'],
             ];
            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST  => 'GET',
            CURLOPT_HTTPHEADER     => $headers
            ));
            $result = curl_exec($curl);
            $response = json_decode($result);
            curl_close($curl);

            if($response->statusCode==200){
                $orderId = $request->orderid;
                $mailClass = $request->mailClass;
                $is_apo_fpo = '0';
                $senderpostalCode = $request->senderpostalCode;
                $orderInfo = StoreOrder::find($orderId)->toArray();
                $shippingaddressArr = explode(', ', $orderInfo['shipping_address']);
                $recipientpostalCode1 =  end($shippingaddressArr);
                $recipientpostalCodearr = explode('-', $recipientpostalCode1);
                $recipientpostalCode = $recipientpostalCodearr[0];
                $storeOrderItems = StoreOrderItem::selectRaw('sum(weight) as totalweight')
                                        ->where('store_order_id', $orderId)
                                        ->where('item_status', '=', 'new')
                                        ->first()->toArray();
                $weightOz = $storeOrderItems['totalweight'] * 16;

                if($weightOz < 1){
                    $responsemsg = [
                    'requestId' => strtolower(Str::random(30)),
                    'message' => 'Order has zero weight.',
                    'statusCode'    => 403,
                    'status' => 0,
                    ];
                    return response()->json($responsemsg);
                }

                $calurl   =  env('VIPPARCEL_URL').'v1/shipping/label/calculate';
                $calculationData = array("authToken" => $response->Token, "weightOz" => $weightOz, "labelType" => $request->labelType, "senderPostalCode" => $senderpostalCode, "recipientPostalCode" => $recipientpostalCode, "mailClass" => $request->mailClass);
                $headers = [
                    'Authorization: '.$_SERVER['HTTP_AUTHORIZATION'],
                 ];
                $curl_cal = curl_init();
                curl_setopt_array($curl_cal, array(
                CURLOPT_URL            => $calurl,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST  => 'POST',
                CURLOPT_POSTFIELDS  => $calculationData,
                CURLOPT_HTTPHEADER     => $headers
                ));
                $calresult = curl_exec($curl_cal);
                $calresponse = json_decode($calresult);
                curl_close($curl_cal);

                if($calresponse->statusCode==200){
                    $responsemsg = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode'    => $calresponse->statusCode,
                    'value' => $calresponse->value,
                    'valueUsps' => $calresponse->valueUsps,
                    'deliverDays' => $calresponse->deliverDays,
                    'maxWeightOz' => $calresponse->maxWeightOz,
                    'status' => 1,
                    ];
                    return response()->json($responsemsg);
                } else {
                    $responsemsg = [
                    'requestId' => strtolower(Str::random(30)),
                    'message' => $calresponse->message,
                    'statusCode'    => $calresponse->statusCode,
                    'status' => 0,
                    ];
                    return response()->json($responsemsg);
                }
            } else {
                $responsemsg = [
                'requestId' => strtolower(Str::random(30)),
                'message' => 'Error',
                'statusCode'    => 403,
                'status' => 0,
                ];
                return response()->json($responsemsg);
            }
               
        } catch (\Exception $e) {
            return $response = [
            'requestId' => strtolower(Str::random(30)),
            'message'    => $e->getMessage,
            'statusCode' => 403,
            'status' => 0,
           ];
        }
    } 

    
    
}


