<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Http\Resources\UserResource;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Hash;
use Carbon\Carbon; 
use JWTFactory;
use JWTAuth;
use Validator;
use Config;
use Log;
use Event;
use App\Events\UserRegistered;

use DB;
use App\Models\Country;
use App\Models\State;
use App\Models\Apirequests;
use App\Models\Userapi;
use App\Models\Userapistatistics;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;

class StateController extends Controller
{   

    public function get_States()
    {
        try {
        $result = file_get_contents(env('VIPPARCEL_URL').'getStates');
        $response = json_decode($result, true);
        $records = $response['records'];
        if ($records) {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'records'    => $records,
            'statusCode' => 200,
           ];

          } else {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message'    => 'Record does not exist.',
            'statusCode' => 404,
           ];
        }
        return response()->json($response);
        } catch (\Exception $e) {
        return response()->json(['status' => 0, 'error' => $e->getMessage()], 403);
        }
    }
}