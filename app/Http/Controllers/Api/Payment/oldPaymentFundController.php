<?php

namespace App\Http\Controllers\Api\Payment;

use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\APIBaseController as APIBaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;

/** All Paypal Details class **/
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use URL;
use Auth;
use DB;
use Exception;
use Log;
use Session;
use Hash;
use Image;

class PaymentFundController extends ApiBaseController
{
   /**
   * Create a new AuthController instance.
   *
   * @return void
   */


  private $_api_context;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);

    }

 public function payWithpaypal(Request $request)
    {
        try {

            $token = trim($request->authToken);            
            $AMT= trim($request->amount);

            $rules = [];
            $rules['amount'] = 'required|regex:/^[0-9]+$/|max:5|min:2';
            $rules['authToken'] = 'required';
           
            $validator = Validator::make($request->all(), $rules);

            if($validator->fails() || $AMT < 10) {

                $response = [
                'requestId'  => strtolower(Str::random(30)),
                'message'    => 'Validation Error.',
                'error'      => $validator->errors(),
                'statusCode' => 422,
                ];

                if($AMT < 10){
                $response = [
                'Amount' => 'Enter amount greater than $10.',
                ];
                }

                return response()->json($response);  

            }else{

                $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                if (is_null($auth_token)) { 
                    $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'message'    => 'Token does not exist.',
                    'statusCode' => 401,
                    ];
                    return response()->json($response);
                }else {

                    if($auth_token->active == 1) {
                    $payer = new Payer();
                    $payer->setPaymentMethod('paypal');

                    $item_1 = new Item();

                    $item_1->setName('Item 1') /** item name **/
                        ->setCurrency('USD')
                        ->setQuantity(1)
                        ->setPrice($request->get('amount')); /** unit price **/

                    $item_list = new ItemList();
                    $item_list->setItems(array($item_1));
                
                    $amount = new Amount();
                    $amount->setCurrency('USD')
                        ->setTotal($request->get('amount'));

                    $transaction = new Transaction();
                    $transaction->setAmount($amount)
                        ->setItemList($item_list)
                        ->setDescription('Your transaction description');

                    $redirect_urls = new RedirectUrls();
                    $redirect_urls->setReturnUrl(URL::to('http://iebasketball.com/ievipparcelshippingcenterapi/api/v1/payment/statusfund?authToken='.$token)) /** Specify return URL **/
                                  ->setCancelUrl(URL::to('http://vipparcel.com'));

                    $payment = new Payment();
                    $payment->setIntent('Sale')
                        ->setPayer($payer)
                        ->setRedirectUrls($redirect_urls)
                        ->setTransactions(array($transaction));

                    /** dd($payment->create($this->_api_context));exit; **/

                    try {

                        $payment->create($this->_api_context);
                           
                        } catch (\PayPal\Exception\PPConnectionException $ex) {

                        if (\Config::get('app.debug')) {

                            \Session::put('error', 'Connection timeout');
                            return Redirect::to('/');

                        } else {

                            \Session::put('error', 'Some error occur, sorry for inconvenient');
                            return Redirect::to('/');
                        }
                    }

                    foreach ($payment->getLinks() as $link) {

                        if ($link->getRel() == 'approval_url') {

                            $redirect_url = $link->getHref();
                            break;

                        }

                    }

                    /** add payment ID to session **/

                   Session::put('paypal_payment_id', $payment->getId());

                    if (isset($redirect_url)) {

                        /** redirect to paypal **/
                         return response()->json([$redirect_url], 200);
                    }

                    \Session::put('error', 'Unknown error occurred');
                    return Redirect::to('/');
                }
             }
          }
    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());
    }
 }

    public function getPaymentStatus()
    {
        
    try {
        /** Get the payment ID before session clear **/

        //$payment_id = Session::get('paypal_payment_id');
        //$user = Auth::user();
        $token       = Input::get('authToken');  
        $auth_token  = DB::table('user_api')->where('auth_token', $token)->first(); 
        $user        = DB::table('users')->where('id', $auth_token->user_id)->first(); 
        $profileInfo = DB::table('profile')->where('user_id', $user->id)->first();

        if($profileInfo){

            $first_name = $profileInfo->first_name;
            $last_name  =  $profileInfo ->last_name;     
        }

        $payment_id = Input::get('paymentId');

        /** clear the session payment ID **/

        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {

            \Session::put('error', 'Payment failed');
            
           return Redirect::to('/');

        }

        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));

        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
  
          if ($result->getState() == 'approved') {
            $rr         = array();
            $sales_info = array();
            $rr         = $result->payer->payer_info;
            $sales_info = $result->transactions[0]->related_resources[0];
            $amount     = $result->transactions[0]->amount;

            $txn_id     = $sales_info->sale->id;


           $data = serialize([
                    'payer_email'=> $rr->email,
                    'first_name' => $rr->first_name,
                    'last_name'  => $rr->last_name,
                    'billing'    => $rr->shipping_address,
                    'response'   => $payment->toArray()
            ]);

            $payment_log = DB::table('payment_logs')->insertGetId(
                            ['user_id' => $user->id, 'service' =>'paypal' ,'txn_id'=> $txn_id ,'status'=>'1','data'=> $data] 
                           );

            if($payment_log){

                $payments = DB::table('payments')->insertGetId(
                            ['to_user_id' => $user->id, 'amount'=>$amount->total,'type' =>'paypal' ,'txn_id'=> $txn_id ] 
                            );
                if($payments){  
                    $response = [
                        'requestId'  => strtolower(Str::random(30)),
                        'message'    => 'Funds added Successfully',
                        'statusCode' => 200,
                    ];         
                return response()->json($response);

                }else{

                    $response = [
                        'requestId'  => strtolower(Str::random(30)),
                        'message'    => 'Some problem occured.',
                        'statusCode' => 400,
                    ];

                return response()->json($response);

                }

            }else{
            
            return response()->json("Server Error occured", 400);

            }
         
       }
     } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());
    }

    }  

   public function payWithcards(Request $request)
    {

        try {

           $token       = trim($request->authToken); 
           $uri         = $request->path();
          // $user        = Auth::user();
           $current     = date('ym');
           $cardexpire  = trim($request->year).trim($request->month);
           $AMT         = trim($request->amount);

           if($uri=='api/v1/payment/cardfund'){

                $rules = [];
                $rules['name']      = 'required|max:35|regex:/^[a-zA-Z0-9_\s]+$/';
                $rules['number']    = 'required|regex:/^[0-9]+$/|max:19|min:13';
                $rules['month']     = 'required|regex:/^[0-9]+$/|max:2|min:2';
                $rules['year']      = 'required|regex:/^[0-9]+$/|max:2|min:2';
                $rules['cvv']       = 'required|regex:/^[0-9]+$/|max:3|min:3';
                $rules['amount']    = 'required|regex:/^[0-9]+$/|max:5|min:2';
                $rules['authToken'] = 'required';

                
                $validator = Validator::make($request->all(), $rules);

                if($validator->fails() || $current >= $cardexpire || $AMT < 10) {

                $response = [
                'requestId'  => strtolower(Str::random(30)),
                'message'    => 'Validation Error.',
                'error'      => $validator->errors(),
                'statusCode' => 422,
                ];

                if($current >= $cardexpire){
                $response = [
                'CardExpiry' => 'Enter valid month or year',
                ];
                }

                if($AMT < 10){
                $response = [
                'Amount' => 'Enter amount greater than $10.',
                ];
                }

                return response()->json($response);

            }else{
               $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
           
                if (is_null($auth_token)) { 
                    $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'message'    => 'Token does not exist.',
                    'statusCode' => 401,
                    ];
                    return response()->json($response);
                }else {

                if($auth_token->active == 1) {

                $user        = DB::table('users')->where('id', $auth_token->user_id)->first();
                $name        = trim($request->name);
                $number      = trim($request->number);
                $month       = trim($request->month);
                $year        = trim($request->year);
                $cvv         = trim($request->cvv);
                $default     = trim($request->default);
                $description = trim($request->description);

                $cardInfo = DB::table('user_credit_cards')
                                ->where(['user_id'=> $user->id,'is_verified'=>'1','number'=> $number])
                                ->where(function($query){
                                    $query->whereNull('is_deleted');
                                    $query->orWhere('is_deleted', '=', '0');
                                    })
                                ->first();
                $userDetail = DB::table('profile')->where('user_id', '=', $user->id)->first();

                $countryDetail = DB::table('countries')->where('idCountry', '=', $userDetail->country_id)->first();

                if($user->test_mode=='1'){
                $PayflowUrl = 'https://pilot-payflowpro.paypal.com/';
                } else {
                $PayflowUrl = 'https://payflowpro.paypal.com';
                }
                $payment_log = array();
                $transactions = array();

            if($cardInfo){

                $AMT        = $request->amount;
                $sandbox    = TRUE;
                $request_params = array(
                    'PARTNER' => 'PayPal', 
                    'USER'    => env('PAYFLOW_USER'), 
                    'PWD'     => env('PAYFLOW_PWD'), 
                    'VENDOR'  => env('PAYFLOW_VENDOR'), 
                    'TENDER'  => 'C', 
                    'TRXTYPE' => 'S',                   
                    'IPADDRESS' => '192.168.1.185',
                    'VERBOSITY' => 'MEDIUM',
                    'ACCT'      => $cardInfo->number,                        
                    'EXPDATE'   => $cardInfo->exp_month.$cardInfo->exp_year,           
                    'CVV2'      => $cardInfo->cvv, 
                    'FIRSTNAME' => $userDetail->first_name, 
                    'LASTNAME'  => $userDetail->last_name, 
                    'STREET'    => $userDetail->address.' '.$userDetail->address_2, 
                    'CITY'      => $userDetail->city, 
                    'STATE'     => $userDetail->state,                     
                    'COUNTRYCODE' => $countryDetail->countryCode, 
                    'ZIP'       => $userDetail->zip, 
                    'AMT'       => $AMT, 
                );
                $nvp_string = '';
                foreach($request_params as $var=>$val){
                    $nvp_string .= '&'.$var.'='.urlencode($val);    
                }
                $nvp_string = substr($nvp_string, 1);
                $curl       = curl_init();
                curl_setopt($curl, CURLOPT_VERBOSE, 1);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($curl, CURLOPT_TIMEOUT, 30);
                curl_setopt($curl, CURLOPT_URL, $PayflowUrl);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $nvp_string);
                $headers    = array();
                $headers[]  = "Content-Type: application/x-www-form-urlencoded";
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                $NVPString  = curl_exec($curl);   
                curl_close($curl);

                $transaction = array();
                while(strlen($NVPString)){
                    $keypos     = strpos($NVPString,'=');
                    $keyval     = substr($NVPString,0,$keypos);
                    $valuepos   = strpos($NVPString,'&') ? strpos($NVPString,'&'): strlen($NVPString);
                    $valval     = substr($NVPString,$keypos+1,$valuepos-$keypos-1);
                    $transaction[$keyval] = urldecode($valval);
                    $NVPString  = substr($NVPString,$valuepos+1,strlen($NVPString));
                }
                $payment_log['card'] = array('card_name'=>$cardInfo->name,'card_number'=>$cardInfo->number,'card_month'=>$cardInfo->exp_month,'card_year'=>$cardInfo->exp_year);

                $payment_log['billing'] = array('id'=>$userDetail->id,'user_id'=>$userDetail->user_id,'last_name'=>$userDetail->last_name,'middle_name'=>$userDetail->middle_name,'first_name'=>$userDetail->first_name,'reg_last_name'=>$userDetail->reg_last_name,'reg_first_name'=>$userDetail->reg_first_name,'phone'=>$userDetail->phone,'created'=>$userDetail->created,'updated'=>$userDetail->updated,'address'=>$userDetail->address,'address_2'=>$userDetail->address_2,'city'=>$userDetail->city,'state'=>$userDetail->state,'country_id'=>$userDetail->country_id,'zip'=>$userDetail->zip,'skype'=>$userDetail->skype,'driver_licence'=>$userDetail->driver_licence,'is_international'=>$userDetail->is_international,'phone_number_info'=>$userDetail->phone_number_info,'phone_number_info_request_id'=>$userDetail->phone_number_info_request_id,'province'=>$userDetail->province,'phone_number_info_error'=>$userDetail->phone_number_info_error);

                $payment_log['response'] = $transaction;
                $PaymentLog              = serialize($payment_log); 
                $created                 = date('Y-m-d h:i:s');

                $newtransaction          = DB::table('payment_logs')
                                            ->insertGetId(['user_id' => $user->id, 'service' => 'paypal', 'txn_id' => $transaction['PNREF'] , 'status' => '1', 'data' => serialize($payment_log), 'created' => $created]);
                if($newtransaction){ 
                    $transaction['AMT'] = $AMT;
                    array_push($transactions,$transaction); 
                } else {
                    $transaction['AMT'] = '';
                }

                if(count($transactions) > 0 && $transactions[0]['RESULT']=='0'){

                    $payment = DB::table('payments')
                                ->insertGetId(['to_user_id' => $user->id, 'amount' => $AMT, 'description' => $description , 'created' => $created, 'type' =>'card', 'txn_id' => $transactions[0]['PNREF'],'txn_ppref' => $transactions[0]['PPREF']]);
                    if($payment){
                        $response = [
                        'requestId'  => strtolower(Str::random(30)),
                        'statusCode' => 200,
                        'message'    => "Funds Added successfully.",
                       ];
                       return response()->json($response);
                    } else {
                        $response = [
                        'requestId'  => strtolower(Str::random(30)),
                        'statusCode' => 403,
                        'message'    => "Failed",
                       ];
                       return response()->json($response);
                    }
                } else {
                    $response = [
                    'requestId'  => strtolower(Str::random(30)),
                    'statusCode' => 403,
                    'message'    => "Failed",
                   ];
                   return response()->json($response);
                }

            }else{

                $response = [
                'requestId'  => strtolower(Str::random(30)),
                'statusCode' => 400,
                'message'    => 'Card does not match with database',
                ];
                return response()->json($response);
              }
             }
            } 
            } 
           } else {
                $response = [
                'requestId' => strtolower(Str::random(30)),
                'statusCode'=> 405,
                'error'     => 'The requested URL '. $uri .' was not found on this server.',
                ];
                return response()->json($response);
            }

    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());
    }
 }
 
}