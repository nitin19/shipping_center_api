<?php

namespace App\Http\Controllers\Api\Payment;

use App\Models\User;
use App\Models\Profile;
use App\Models\UserCreditCards;
use App\Models\PaymentLogs;
use App\Models\Payments;
use App\Models\Country;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;


use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;

/** All Paypal Details class **/
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use URL;
use Auth;
use DB;
use Exception;
use Log;
use Session;
use Hash;
use Image;

class PaymentController extends Controller
{
   /**
   * Create a new AuthController instance.
   *
   * @return void
   */


  private $_api_context;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);

    }

 public function payWithpaypal(Request $request)
    {

        try {
            //$userid = $request->userid;
            $url = env('VIPPARCEL_URL').'payment/paypal';
            $headers = [
                'Authorization: '.$_SERVER['HTTP_AUTHORIZATION'],
            ];

            $amount = $request->amount;
            $redirecturl = url('/').'/api/payment/status/';
            $cancelurl = env('SHIPPINGCENTER_FRONTEND_URL').'/addfunds';
            $userData = array("amount" => $amount, 'redirecturl' => $redirecturl, 'cancelurl' => $cancelurl);
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $userData);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $paypal_url = curl_exec($ch);
            curl_close($ch);
            if($paypal_url){
               return $paypal_url;
            } else {
                $responsemsg = [
                'requestId' => strtolower(Str::random(30)),
                'message' => 'Error',
                'statusCode'    => 403,
                'status' => 0,
                ];
                return response()->json($responsemsg);
            }
            

    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());
    }
 }

    public function getPaymentStatus($userid, Request $request)
    {
        
        try {
            $redirecturl = env('SHIPPINGCENTER_FRONTEND_URL').'/addfunds';
            if($userid!=''){
                $url   =  env('VIPPARCEL_URL').'payment/status/'.$userid;
                $PaymentData = array("paymentId" => $request->paymentId, "token" => $request->token, "PayerID" => $request->PayerID, 'redirecturl' => $redirecturl);
                $nvp_string = '';
                foreach($PaymentData as $var=>$val){
                    $nvp_string .= '&'.$var.'='.urlencode($val);    
                }
                $nvp_string = substr($nvp_string, 1);
                return Redirect::away($url.'/?'.$nvp_string);
            } else {
                return Redirect::away($redirecturl);
            } 
        
        } catch (\Exception $e) {
         return response()->json(['status' => 0, 'message' => $e->getMessage()], 403);
        } 

    }  

   public function payWithcards(Request $request) {

        try {

            $url = env('VIPPARCEL_URL').'payment/card';

            $cardData = array("amount" => $request->amount, "cvv" => $request->cvv, 'cardId' => $request->cardId);
            $headers = [
                'Authorization: '.$_SERVER['HTTP_AUTHORIZATION'],
            ];

            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST  => 'POST',
            CURLOPT_POSTFIELDS  => $cardData,
            CURLOPT_HTTPHEADER     => $headers
            ));

            $result = curl_exec($curl);
            $response = json_decode($result);

            curl_close($curl);
            if($response){
                $responsemsg = [
                'requestId' => $response->requestId,
                'message' => $response->message,
                'statusCode'    => $response->statusCode,
                'status' => $response->status,
                ];
                return response()->json($responsemsg);
            } else {
                $responsemsg = [
                'requestId' => strtolower(Str::random(30)),
                'message' => 'Error',
                'statusCode'    => 403,
                'status' => 0,
                ];
                return response()->json($responsemsg);
            }
        } catch (\Exception $e) {
         return response()->json(['status' => 0, 'message' => $e->getMessage()], 403);
        }

    }

 public function getbalance(Request $request){

        //$ownerId = Auth::user()->id;
        $userid = $request->userid;
        $url = env('VIPPARCEL_URL').'payment/balance';
        $headers = [
                'Authorization: '.$_SERVER['HTTP_AUTHORIZATION'],
        ];
        $userData = array("userid" => $userid);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $userData);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);
        $getbalance = json_decode($response);
        curl_close($ch);
        if($getbalance){
            echo 'yes';
        } else {
            echo 'no';
        }
        /*$ownerId = $request->userid;
        $queryUser = User::query();
        $getbalance =  $queryUser->where('id','=',$ownerId)
                                        ->first();*/

        /*if(empty($getbalance)) {
            return response()->json(['status' => false, 'total_balance' =>$getbalance]);  
        }*/
        return response()->json(['status' => true, 'total_balance' =>$getbalance]);  
    }
 
}