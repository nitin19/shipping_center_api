<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller; 
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use JWTFactory;
use JWTAuth;
use Log;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Str;
use Validator;
use URL;
use Carbon\Carbon;
use App\Models\Userapistatistics;
use App\Models\User; 
use Illuminate\Http\Request; 
//use RocketCode\Shopify\API as Shopify;
use Shopify;

Class ShopifyController extends Controller
{
  protected $shop = "vipparcel-2.myshopify.com";
  protected $foo;
  protected $scopes = ['read_products','write_products'];
  
  public function getPermission()
  {
    $this->foo = Shopify::make($this->shop, $this->scopes);
    echo  $this->foo->redirect();
  }
  
  public function getResponse(Request $request)
  {
    $data= $request->all();
    $query = array(
            "client_id" => env('SHOPIFY_KEY'), 
            "client_secret" => env('SHOPIFY_SECRET'), 
            "code" => $data['code']
    );
    // Generate access token URL
    $access_token_url = "https://" . $data['shop'] . "/admin/oauth/access_token";

    // Configure curl client and execute request
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $access_token_url);
    curl_setopt($ch, CURLOPT_POST, count($query));
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($query));
    $result = curl_exec($ch);
    print_r($result);
    exit();
    curl_close($ch);
    
    

    /*$this->getPermission();
    $access_token = '';
    $this->foo = Shopify::retrieve($this->shop, $access_token);
    $user = $this->foo->getUser();
    //exit();
    
    // Get user data, you can store it in the data base
    $user = $this->foo->auth()->getUser();

    
    //GET request to products.json
    return $this->foo->auth()->get('products.json', ['fields'=>'id,images,title']);*/
  }
  
/*  public function shopifyauth(Request $request){
      echo 'hello';
  }*/
}