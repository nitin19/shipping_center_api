<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\Models\StoreSystem ;
use Illuminate\Support\Facades\Config;
use URL;

use Carbon\Carbon; 
use JWTFactory;
use JWTAuth;
use Log;
use App\Models\Userapistatistics;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;

class MarketplaceController extends Controller
{
    /** 
     * list api 
     * 
     * @return list of marketplaces 
     */ 
    public function list(Request $request){
       // $marketplaces =  StoreSystem :: all();
       /* $marketplaces = StoreSystem::where('status', '=', 1)->get();
        if ( is_null($marketplaces) ) {
            return response()->json(['error'=>'No marketplace available.'], Config::get('response-code.ERROR'));
        }
        return response()->json(['success' => $marketplaces], Config::get('response-code.SUCCESS'));*/

        $querymarketplaces = StoreSystem::query();
        if($request->searchdata!=''){
            $querymarketplaces->where('name','like','%'.$request->searchdata.'%');
            /*$querymarketplaces->where(
                       function($query){
                            $query->where('name','like','%'.$request->searchdata.'%');
                            $query->orWhere('title','like','%'.$request->searchdata.'%');
                        });*/
            /*$querymarketplaces->where('name','like','%'.$request->searchdata.'%') 
                             ->orWhere('title','like','%'.$request->searchdata.'%');*/                  
        }

        $querymarketplaces->where('status', '=', 1)->orderby('id','ASC');
        $marketplaces =  $querymarketplaces->paginate(Config::get('constant.pagination')); 
        if(count($marketplaces) > 0){
            $response = [
            'status' => '1',    
            'success' => $marketplaces,
            ];
            return response()->json($response);
        } else {
            $response = [
            'status' => '0',
            'message' => 'no record found',
            ];
            return response()->json($response);
        }
    }

    //search Marketplace 
    /*
    * @param search keyword [string]
    * @return list of marketplaces 
    */
    public function search(Request $request) {
        $querymarketplaces = StoreSystem::query();
        $input = $request->all();
        if(!empty($input['keyword'])) {
            $querymarketplaces->where('name','like','%'.$input['keyword'].'%') 
                             ->orWhere('title','like','%'.$input['keyword'].'%');                  
        }

        $marketplaces = $querymarketplaces->get();

        if ( is_null($marketplaces) ) {
            return response()->json(['error'=>'No marketplace available.'], Config::get('response-code.ERROR'));
        }

        return response()->json(['success' => $marketplaces], Config::get('response-code.SUCCESS'));
    }

}
