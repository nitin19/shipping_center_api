<?php

namespace App\Http\Controllers\Api;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Models\User; 
use App\Models\Store;
use App\Models\StoreOrder;
use App\Models\StoreOrderItem;
use App\Models\StoreSystem;
use App\Models\StoreOrderProcessor;
use App\Models\Warehouses;
use App\Models\OrderProcessor;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Config;
use Validator;
use URL;
use Mail;

use Carbon\Carbon; 
use JWTFactory;
use JWTAuth;
use Log;
use App\Models\Userapistatistics;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;
use Session;
use Illuminate\Support\Facades\Redirect;

class DashboardController extends Controller 
{
   
  public function getStoreCount(Request $request)
  {     
        //$user = Auth::user();
        $userid = $request->userid;

        $storescount = Store::where(['owner_id' => $userid, 'status' => '1', 'deleted' => '0'])->count(); 
        $response = [
            'status' => true,
            'total_stores' => $storescount
           ];

        return response()->json($response);
    }

    public function getOrdersCount(Request $request){

        //$ownerId = Auth::user()->id;
        $ownerId = $request->userid;
       
        /*$queryStores = Store::query();
        $storeInfo =  $queryStores->where('owner_id',$ownerId)->get();
        if(empty($storeInfo)){
            return response()->json(['status' => false, 'total_orders' => '0']);  
        }*/

        $queryStoreOrder = StoreOrder::query();
        $storeOrders =  $queryStoreOrder->join('stores', 'store_orders.store_id','=','stores.id')
                                        ->where('stores.owner_id','=',$ownerId)
                                        ->where('stores.status','=','1')
                                        ->where('stores.deleted','=','0')
                                        ->select('store_orders.*','store_orders.id as OrderId','stores.store_name','stores.slug')
                                        ->where('store_orders.status','=','1')
                                        ->orderby('id','DESC')
                                        ->get()
                                        ->toArray(); 

        if(empty($storeOrders)) {
            return response()->json(['status' => false, 'total_orders' =>count($storeOrders)]);  
        }
        return response()->json(['status' => true, 'total_orders' =>count($storeOrders)]);  
    }

    public function getProductsCount(Request $request) {

        //$ownerId = Auth::user()->id;

        $ownerId = $request->userid;

        /*$orderInfo = StoreOrder::find($orderId);
        if(empty($orderInfo)) {
            return response()->json(['error'=>'Some Error Occured'], Config::get('response-code.ERROR'));
        }*/

        $orderDetails = StoreOrderItem::join('store_orders', 'store_order_items.store_order_id','=','store_orders.id')
                                ->join('stores', 'store_orders.store_id','=','stores.id')
                                ->where('stores.owner_id','=',$ownerId)
                                ->where('stores.status','=','1')
                                ->where('stores.deleted','=','0')
                                ->where('store_orders.status','=','1')
                              //  ->where('store_orders.id','=',$orderId)
                              //  ->where('store_order_items.store_order_id','=',$orderId)
                                ->select('store_order_items.*','store_orders.id as OrderId','stores.store_name','stores.slug')
                                ->where('store_order_items.status','=','1')
                                ->orderby('id','DESC')
                                ->get()
                                ->toArray();

        if(empty($orderDetails)) {
            return response()->json(['status' => true, 'total_products' =>count($orderDetails)]); 
        }
        
        return response()->json(['status' => true, 'total_products' =>count($orderDetails)]); 
                
    }

  public function getWarehousesCount(Request $request)
  {     
        //$user = Auth::user();
        //$userid = $user->id;
        $userid = $request->userid;

        $warehousescount = Warehouses::where(['store_owner_id' => $userid, 'status' => '1'])->count(); 
        $response = [
            'status' => true,
            'total_warehouses' => $warehousescount
           ];

        return response()->json($response);
    } 

    public function getWarehousesList(Request $request)
    {     
        //$user = Auth::user();
        //$userid = $user->id;

        $userid = $request->userid;

        $warehouseslist = Warehouses::where(['store_owner_id' => $userid, 'status' => '1'])->take(5)->orderBy('id', 'DESC')->get(); 
        $response = [
            'status' => true,
            'total_warehouses' => $warehouseslist
           ];

        return response()->json($response);
    }

    public function getOrderStatus(Request $request)
    {     
        //$user = Auth::user();
        //$userid = $user->id;
        $userid = $request->userid;

        $allstoredata = array();
        $allorderdetails = array();
        $store_item_data = StoreOrderItem::join('store_orders', 'store_order_items.store_order_id','=','store_orders.id')
                                ->join('stores', 'store_orders.store_id','=','stores.id')
                                ->join('warehouses', 'store_orders.id','=','warehouses.store_order_id')
                                ->where('stores.owner_id','=',$userid)
                                ->where('stores.status','=','1')
                                ->where('stores.deleted','=','0')
                                ->select('store_order_items.*','store_orders.id as OrderId','store_orders.order_id as orderid','store_orders.order_details as order_details','stores.store_name','stores.store_system_id','stores.id as storeId','stores.slug','warehouses.warehouse_city')
                                ->where('store_order_items.status','=','1')
                                ->orderby('id','DESC')
                                ->get()
                                ->toArray();
        
        /*$order_details = $store_item_data[0]['order_details'];
        $ggg = array(json_decode($order_details));
        $allorderdetailsfff = array_merge($store_item_data,$ggg);*/

        //$warehouseslist = Warehouses::where(['store_owner_id' => $userid, 'status' => '1'])->get()->toArray(); 
        
        //$allstoredata = array_merge($store_item_data, $warehouseslist);
        if($store_item_data!=''){
        $response = [
            'status' => true,
            'order_status' => $store_item_data
           ];
        } else {
           $response = [
            'status' => false,
            'message' => $store_item_data
           ]; 
        }
        return response()->json($response);
    }

    public function getOrdersComplete(Request $request){

        //$ownerId = Auth::user()->id;
        $ownerId = $request->userid;

        $queryStoreOrder = StoreOrder::query();
        $storeOrderscompleted =  $queryStoreOrder->join('stores', 'store_orders.store_id','=','stores.id')
                                        ->where('stores.owner_id','=',$ownerId)
                                        ->where('stores.status','=','1')
                                        ->where('stores.deleted','=','0')
                                        ->select('store_orders.*','store_orders.id as OrderId','stores.store_name','stores.slug')
                                        ->where('store_orders.status','=','1')
                                        ->where('store_orders.order_status','=','completed')
                                        ->orderby('id','DESC')
                                        ->get()
                                        ->toArray(); 

        if(empty($storeOrderscompleted)) {
            return response()->json(['status' => false, 'total_orders' =>count($storeOrderscompleted)]);  
        }
        return response()->json(['status' => true, 'total_orders' =>count($storeOrderscompleted)]);  
    }

    public function getOrdersPending(Request $request){

        //$ownerId = Auth::user()->id;

        $ownerId = $request->userid;

        $queryStoreOrder = StoreOrder::query();
        $storeOrderspending =  $queryStoreOrder->join('stores', 'store_orders.store_id','=','stores.id')
                                        ->where('stores.owner_id','=',$ownerId)
                                        ->where('stores.status','=','1')
                                        ->where('stores.deleted','=','0')
                                        ->select('store_orders.*','store_orders.id as OrderId','stores.store_name','stores.slug')
                                        ->where('store_orders.status','=','1')
                                        ->where('store_orders.order_status','=','in_process')
                                        ->orderby('id','DESC')
                                        ->get()
                                        ->toArray(); 

        if(empty($storeOrderspending)) {
            return response()->json(['status' => false, 'total_orders' =>count($storeOrderspending)]);  
        }
        return response()->json(['status' => true, 'total_orders' =>count($storeOrderspending)]);  
    }

    public function getOrdersPartial(Request $request){

        //$ownerId = Auth::user()->id;

        $ownerId = $request->userid;

        $queryStoreOrder = StoreOrder::query();
        $storeOrderspartial =  $queryStoreOrder->join('stores', 'store_orders.store_id','=','stores.id')
                                        ->where('stores.owner_id','=',$ownerId)
                                        ->where('stores.status','=','1')
                                        ->where('stores.deleted','=','0')
                                        ->select('store_orders.*','store_orders.id as OrderId','stores.store_name','stores.slug')
                                        ->where('store_orders.status','=','1')
                                        ->where('store_orders.order_status','=','partial')
                                        ->orderby('id','DESC')
                                        ->get()
                                        ->toArray(); 

        if(empty($storeOrderspartial)) {
            return response()->json(['status' => false, 'total_orders' =>count($storeOrderspartial)]);  
        }
        return response()->json(['status' => true, 'total_orders' =>count($storeOrderspartial)]);  
    }
    public function getOrdersnew(Request $request){

        //$ownerId = Auth::user()->id;

        $ownerId = $request->userid;

        $queryStoreOrder = StoreOrder::query();
        $storeOrdersnew =  $queryStoreOrder->join('stores', 'store_orders.store_id','=','stores.id')
                                        ->where('stores.owner_id','=',$ownerId)
                                        ->where('stores.status','=','1')
                                        ->where('stores.deleted','=','0')
                                        ->select('store_orders.*','store_orders.id as OrderId','stores.store_name','stores.slug')
                                        ->where('store_orders.status','=','1')
                                        ->where('store_orders.order_status','=','new')
                                        ->orderby('id','DESC')
                                        ->get()
                                        ->toArray(); 

        if(empty($storeOrdersnew)) {
            return response()->json(['status' => false, 'total_orders' =>count($storeOrdersnew)]);  
        }
        return response()->json(['status' => true, 'total_orders' =>count($storeOrdersnew)]);  
    }

    public function getOrderprocessorCount(Request $request){

        //$ownerId = Auth::user()->id;

        $ownerId = $request->userid;

        $queryStoreOrder = OrderProcessor::query();
        $storeOrderprocessor =  $queryStoreOrder->where('status','=','1')
                                        ->where('created_by','=',$ownerId)
                                        ->get();

        if(empty($storeOrderprocessor)) {
            return response()->json(['status' => false, 'total_order_processor' =>count($storeOrderprocessor)]);  
        }
        return response()->json(['status' => true, 'total_order_processor' =>count($storeOrderprocessor)]);  
    }


    public function getallOrderCount(Request $request){

        //$ownerId = Auth::user()->id;

        $ownerId = $request->userid;

        $allcountdata = array();
        $allfinalcountdata = 
        $queryStoreOrder = StoreOrder::query();
        $allcountdata[] =  $queryStoreOrder->join('stores', 'store_orders.store_id','=','stores.id')
                                        ->where('stores.owner_id','=',$ownerId)
                                        ->where('stores.status','=','1')
                                        ->where('stores.deleted','=','0')
                                        ->select('store_orders.*','store_orders.id as OrderId','stores.store_name','stores.slug')
                                        ->where('store_orders.status','=','1')
                                        ->where('store_orders.order_status','=','completed')
                                        ->orderby('id','DESC')
                                        ->count();

        $queryStoreOrder = StoreOrder::query();
        $allcountdata[] =  $queryStoreOrder->join('stores', 'store_orders.store_id','=','stores.id')
                                        ->where('stores.owner_id','=',$ownerId)
                                        ->where('stores.status','=','1')
                                        ->where('stores.deleted','=','0')
                                        ->select('store_orders.*','store_orders.id as OrderId','stores.store_name','stores.slug')
                                        ->where('store_orders.status','=','1')
                                        ->where('store_orders.order_status','=','in_process')
                                        ->orderby('id','DESC')
                                        ->count();

        $queryStoreOrder = StoreOrder::query();
        $allcountdata[] =  $queryStoreOrder->join('stores', 'store_orders.store_id','=','stores.id')
                                        ->where('stores.owner_id','=',$ownerId)
                                        ->where('stores.status','=','1')
                                        ->where('stores.deleted','=','0')
                                        ->select('store_orders.*','store_orders.id as OrderId','stores.store_name','stores.slug')
                                        ->where('store_orders.status','=','1')
                                        ->where('store_orders.order_status','=','partial')
                                        ->orderby('id','DESC')
                                        ->count();

        $queryStoreOrder = StoreOrder::query();
        $allcountdata[] =  $queryStoreOrder->join('stores', 'store_orders.store_id','=','stores.id')
                                        ->where('stores.owner_id','=',$ownerId)
                                        ->where('stores.status','=','1')
                                        ->where('stores.deleted','=','0')
                                        ->select('store_orders.*','store_orders.id as OrderId','stores.store_name','stores.slug')
                                        ->where('store_orders.status','=','1')
                                        ->where('store_orders.order_status','=','new')
                                        ->orderby('id','DESC')
                                        ->count();
                                        
        return response()->json(['status' => true, 'total_order' =>$allcountdata]);  
    }

    public function getTotalOrderCount(Request $request){

        //$ownerId = Auth::user()->id;

        $ownerId = $request->userid;

        $allcountdata = array();

        $queryStoreOrder = StoreOrder::query();
        $allcountdata[] =  $queryStoreOrder->join('stores', 'store_orders.store_id','=','stores.id')
                                        ->where('stores.owner_id','=',$ownerId)
                                        ->where('stores.status','=','1')
                                        ->where('stores.deleted','=','0')
                                        ->select('store_orders.*','store_orders.id as OrderId','stores.store_name','stores.slug')
                                        ->where('store_orders.status','=','1')
                                        ->orderby('id','DESC')
                                        ->count();

        $queryStoreOrder = StoreOrder::query();
        $allcountdata[] =  $queryStoreOrder->join('stores', 'store_orders.store_id','=','stores.id')
                                        ->where('stores.owner_id','=',$ownerId)
                                        ->where('stores.status','=','1')
                                        ->where('stores.deleted','=','0')
                                        ->select('store_orders.*','store_orders.id as OrderId','stores.store_name','stores.slug')
                                        ->where('store_orders.status','=','1')
                                        ->where('store_orders.order_status','=','completed')
                                        ->orderby('id','DESC')
                                        ->count();

        $queryStoreOrder = StoreOrder::query();
        $allcountdata[] =  $queryStoreOrder->join('stores', 'store_orders.store_id','=','stores.id')
                                        ->where('stores.owner_id','=',$ownerId)
                                        ->where('stores.status','=','1')
                                        ->where('stores.deleted','=','0')
                                        ->select('store_orders.*','store_orders.id as OrderId','stores.store_name','stores.slug')
                                        ->where('store_orders.status','=','1')
                                        ->where('store_orders.order_status','=','in_process')
                                        ->orderby('id','DESC')
                                        ->count();

        $queryStoreOrder = StoreOrder::query();
        $allcountdata[] =  $queryStoreOrder->join('stores', 'store_orders.store_id','=','stores.id')
                                        ->where('stores.owner_id','=',$ownerId)
                                        ->where('stores.status','=','1')
                                        ->where('stores.deleted','=','0')
                                        ->select('store_orders.*','store_orders.id as OrderId','stores.store_name','stores.slug')
                                        ->where('store_orders.status','=','1')
                                        ->where('store_orders.order_status','=','partial')
                                        ->orderby('id','DESC')
                                        ->count();

        $queryStoreOrder = StoreOrder::query();
        $allcountdata[] =  $queryStoreOrder->join('stores', 'store_orders.store_id','=','stores.id')
                                        ->where('stores.owner_id','=',$ownerId)
                                        ->where('stores.status','=','1')
                                        ->where('stores.deleted','=','0')
                                        ->select('store_orders.*','store_orders.id as OrderId','stores.store_name','stores.slug')
                                        ->where('store_orders.status','=','1')
                                        ->where('store_orders.order_status','=','new')
                                        ->orderby('id','DESC')
                                        ->count();

        $queryStoreOrder = OrderProcessor::query();
        $allcountdata[] =  $queryStoreOrder->where('status','=','1')
                                        ->where('created_by','=',$ownerId)
                                        ->count();
                                        
        return response()->json(['status' => true, 'total_order' =>$allcountdata]);  
    }
    
    
  /*public function getOrdersCount()
  {     
        $user = Auth::user();
        $userid = $user->id;

        $orderscount = StoreOrder::where(['status' => '1'])->count(); 
        $response = [
            'status' => true,
            'total_orders' => $orderscount
           ];

        return response()->json($response);
    }*/ 
    
/*  public function getProductsCount()
  {     
        $user = Auth::user();
        $userid = $user->id;

        $productscount = StoreOrderItem::where(['status' => '1'])->count(); 
        $response = [
            'status' => true,
            'total_products' => $productscount
           ];

        return response()->json($response);
    }*/ 


    
}


