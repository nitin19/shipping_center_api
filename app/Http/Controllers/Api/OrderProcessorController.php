<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Models\User; 
use App\Models\OrderProcessor;
use App\Models\StoreOrderProcessor;
use App\Models\Store; 
use App\Models\StoreOrder;
use App\Models\StoreSystem;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Config;
use Validator;
use URL;
use DB;
use Mail;

use Carbon\Carbon; 
use JWTFactory;
use JWTAuth;
use Log;
use Hash;
use File;
use App\Models\Userapistatistics;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;

class OrderProcessorController extends Controller 
{

  public function saveProcessor(Request $request) {

      try {

        $userid      = $request->userid;
        $headers  = [
            'Authorization: '.$_SERVER['HTTP_AUTHORIZATION'],
        ];

           // $url   =  env('VIPPARCEL_URL').'v1/account/personalInfo/details/?authToken='.$userinfo->Token;
            $url    =  env('VIPPARCEL_URL').'user';
  
            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST  => 'GET',
            CURLOPT_HTTPHEADER     => $headers
            ));

            $result   = curl_exec($curl);
            $user = json_decode($result);
            curl_close($curl);
          $user_stores = Store::where('owner_id',$userid)->get();

          if(count($user_stores) > 0){ 

          $rules = [];
          $rules['first_name']   = 'required|max:35|regex:/^[a-zA-Z0-9_\s]+$/';
          $rules['last_name']    = 'required|max:35|regex:/^[a-zA-Z0-9_\s]+$/';
          $rules['email']        = 'required|string|email|max:255|unique:order_processor';
          $rules['password']     = 'required|string|min:8|confirmed';
          $rules['phone']        = 'required|regex:/^[0-9]+$/|max:14|min:10';
          
          $validator = Validator::make($request->all(), $rules);

          if ($validator->fails()) { 
              return response()->json(['error'=>$validator->errors()], Config::get('response-code.ERROR'));            
          }

          $first_name    = trim($request->first_name);
          $last_name     = trim($request->last_name);
          $email         = trim($request->email);
          $hd_password   = trim($request->password);
          $password      = Hash::make(trim($request->password));
          $phone         = trim($request->phone);
          $is_fulfillment_center         = trim($request->is_fulfillment_center);
          $assigndstores = $request->assignedstores; 

          $email_exists = User::where('email',$email)->first();
          if($email_exists){
            return response()->json([
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'status' =>'Error',
                                'message'=>"Email already taken."
                              ]); 
          }

          if ($request->hasFile('processor_image')) {
              if($request->file('processor_image')->isValid()) {
                  $file = $request->file('processor_image');
                  $image_name = time() . '.' . $file->getClientOriginalExtension();
                  $destinationPath = '/uploads/processorimage/';
                  $file->move(public_path(). $destinationPath, $image_name); 
              } 
            } else {
              $image_name = '';
            }  

            if(($assigndstores !='') && count(array_filter($assigndstores))>0){
              $orderprocessor = OrderProcessor::insertGetId(
                    ['first_name' => $first_name, 'last_name' =>$last_name ,'email'=> $email ,'password'=>$password,'phone'=> $phone,'is_fulfillment_center'=> $is_fulfillment_center,'processor_image'=>$image_name,'created_by'=> $userid] 
                    );

              foreach ($assigndstores as $key => $storeid) {

                $stores = Store::where(['owner_id'=>$userid ,'id'=>$storeid])->first();
                $store_assign = StoreOrderProcessor::insert(
                                ['store_id' => $storeid, 'processor_id' =>$orderprocessor ,'owner_id'=> $userid ,'store_system_id'=>$stores->store_system_id]
                                );
              }

            $data = OrderProcessor::where('id',$orderprocessor)->first();
            $data = $data->toArray();
            $data['hd_password'] = $hd_password;
            
              Mail::send('emails.processor', ['data' => $data], function ($m) use ($data,$user) {
                $m->from($user->email, 'Your login credentials');
                $m->to($data['email'], $data['first_name'])->subject('Order Processor Credentials');
              });

            }else{

              $orderprocessor = OrderProcessor::insertGetId(
                    ['first_name' => $first_name, 'last_name' =>$last_name ,'email'=> $email ,'password'=>$password,'phone'=> $phone,'is_fulfillment_center'=> $is_fulfillment_center,'processor_image'=>$image_name,'created_by'=> $userid] 
                    );
              $data = OrderProcessor::where('id',$orderprocessor)->first();
              $data = $data->toArray();
              $data['hd_password'] = $hd_password;

            Mail::send('emails.processor', ['data' => $data], function ($m) use ($data,$user) {
              $m->from($user->email, 'Your login credentials');
              $m->to($data['email'], $data['first_name'])->subject('Order Processor Credentials');
              });
            }
            
          return response()->json([
                              'requestId' => strtolower(Str::random(30)),
                              'statusCode' => 200,
                              'status' =>'Success',
                              'message'=>"OrderProcessor saved successfully"
                            ]); 
        }else{

          return response()->json([
                              'requestId' => strtolower(Str::random(30)),
                              'statusCode' => 403,
                              'status' =>'Error',
                              'message'=>"You don't have any store. Firstly,Please registered a store."
                            ]);
          } 
    
      } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

              return response()->json(['token_expired'], $e->getStatusCode());

      } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

              return response()->json(['token_invalid'], $e->getStatusCode());

      } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

              return response()->json(['token_absent'], $e->getStatusCode());

      }
  }

  /* Updating  the Order Processor */

  public function updateProcessor(Request $request , $id) {

      try{
/*        $abc = $request->all();
        echo"<pre>";
        print_r($abc);
        echo"</pre>";
        exit();*/
        $userid = $request->userid;

        $processorExists = OrderProcessor::where(['id'=> $id ,'created_by'=>$userid])->first();

          if($processorExists){
          $rules = [];

          if(trim($request->first_name)) {
           $rules['first_name'] = 'required|max:35|regex:/^[a-zA-Z0-9_\s]+$/';   
           $first_name = trim($request->first_name);
          } else {
           $first_name = $processorExists->first_name;   
          }

          if(trim($request->last_name)) {
           $rules['last_name'] = 'required|max:35|regex:/^[a-zA-Z0-9_\s]+$/'; 
           $last_name = trim($request->last_name);  
          } else {
           $last_name = $processorExists->last_name;     
          }

          if(trim($request->email)) {
           $rules['email'] = 'required|string|email|max:255|unique:order_processor'; 
           $email = trim($request->email);  
          } else {
           $email = $processorExists->email;     
          }

          if(trim($request->password)) {
           $rules['password'] = 'required|string|min:8|confirmed'; 
           $password = Hash::make(trim($request->password));  
          } else {
           $password = $processorExists->password;     
          }
          
          if(trim($request->phone)) {
           $rules['phone'] = 'required|regex:/^[0-9]+$/|max:14|min:10'; 
           $phone = trim($request->phone);  
          } else {
           $phone = $processorExists->phone;     
          }

          if(trim($request->is_fulfillment_center)!='') {
           $is_fulfillment_center = trim($request->is_fulfillment_center);  
          } else {
           $is_fulfillment_center = $processorExists->is_fulfillment_center;     
          }

          if(trim($request->status)!='') {
           $status = trim($request->status);  
          } else {
           $status = $processorExists->status;     
          }
      
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) { 
            return response()->json([
                            'requestId' => strtolower(Str::random(30)),
                            'statusCode' => 200,
                            'status'=>'Error',
                            'message'=>$validator->errors()
                            ]);            
        }

          if ($request->hasFile('processor_image')) {

            $processor_data = OrderProcessor::where('id',$id)->first();
            $processorimage = $processor_data->processor_image;

            $image_path = public_path() . "/uploads/processorimage/".$processorimage;

            if(File::exists($image_path)) {
            File::delete($image_path);
            }


            if($request->file('processor_image')->isValid()) {
                  $file = $request->file('processor_image');
                  $image_name = time() . '.' . $file->getClientOriginalExtension();
                  $destinationPath = '/uploads/processorimage/';
                  $file->move(public_path(). $destinationPath, $image_name); 

                    $orderprocessor = OrderProcessor::where('id',$id)->update(
                                    ['first_name' => $first_name, 'last_name' =>$last_name ,'email'=> $email ,'password'=>$password,'phone'=> $phone,'is_fulfillment_center'=>$is_fulfillment_center,'status'=>$status,'processor_image'=>$image_name,'updated_by'=>$userid] 
                                    );
              } 
          } else {
                    $orderprocessor = OrderProcessor::where('id',$id)->update(
                                      ['first_name' => $first_name, 'last_name' =>$last_name ,'email'=> $email ,'password'=>$password,'phone'=> $phone,'is_fulfillment_center'=>$is_fulfillment_center,'status'=>$status,'updated_by'=>$userid]);
          } 

        $assignedstores = $request->assignedstores; 
        if($assignedstores){

            $savedprocessorstore = StoreOrderProcessor::where(['processor_id'=>$id ,'owner_id'=>$userid])->get();
            $storearr = array();
            if(count($savedprocessorstore) > 0)  {                    
              foreach ($savedprocessorstore as $key) {             
                  array_push($storearr, $key->store_id);
              }
            }

            foreach ($assignedstores as $key => $storeid) {

              if(array_search($storeid, $storearr) !== false){
   
                $savedprocessorstore = StoreOrderProcessor::where(['processor_id'=>$id ,'store_id'=>$storeid])
                                       ->first();
                if($savedprocessorstore['status']== '0' ){

                   $updateDetail = StoreOrderProcessor::where(
                                   ['store_id'=>$storeid, 'processor_id'=>$id])
                                    ->update(['status' => '1']); 
                   unset($storearr[array_search($storeid, $storearr)]);
                }else{

                  unset($storearr[array_search($storeid, $storearr)]);
                }

              }else{

                $stores = Store::where(['owner_id'=>$userid ,'id'=>$storeid])->first();
                if($stores){

                $store_assign = StoreOrderProcessor::insert(
                              ['store_id' => $storeid, 'processor_id' =>$id ,'owner_id'=> $userid ,'store_system_id'=>$stores['store_system_id']]
                              );
                $message = "Stores are assigned successfully.";
                }else{

                  $message = "You do'nt have this store.";
                }
             }
            }

             if(count($storearr) > 0){
                foreach ($storearr as $key => $value) {
                    $updateDetail = StoreOrderProcessor::where(
                                    ['store_id'=>$value, 'processor_id'=>$id])
                                    ->update(['status' => '0']);
                }
                 $message = "Stores are updated successfully.";
             }
          }

        return response()->json([
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode' => 200,
                        'status'    =>'Success',
                        'message'=>"OrderProcessor successfully updated."
                      ]);  
        }else{
          return response()->json([
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode' => 403,
                        'status'    =>'Warning',
                        'message'=>"OrderProcessor doesn't exits for this user."
                      ]);  
        }
    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

          return response()->json(['token_expired'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

          return response()->json(['token_invalid'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

          return response()->json(['token_absent'], $e->getStatusCode());
    }
  }



  public function myProcessors(Request $request) {

      try{
          $userid      = $request->userid;
          $searchdata  = $request->searchdata; 

          $OrderProcessorsdetails  = array();
          $OrderProcessorsdetails1 = array();
         
          $query = OrderProcessor::where('created_by',$userid);
                  if ($searchdata) { 
                   $keyword = $searchdata;
                   $query->where('first_name', 'LIKE', '%' . $keyword . '%');
                   $query->orWhere('last_name', 'LIKE', '%' . $keyword . '%');
                   $query->orWhere('phone', 'LIKE', '%' . $keyword . '%');
                   $query->orWhere('email', 'LIKE', '%' . $keyword . '%');
                  }
                  $query->orderBy('id','DESC');
          $OrderProcessors = $query->paginate(Config::get('constant.pagination'))->toArray();
   
          if(count($OrderProcessors['data']) > 0){
            foreach($OrderProcessors['data'] as $OrderProcessor){
                $StoreOrderProcessors = StoreOrderProcessor::where(['processor_id' => $OrderProcessor['id'], 'status' => '1'])->get();
                $storeorderprocessors = $StoreOrderProcessors->toArray();
                $storesystems = '';
                if(count($storeorderprocessors) > 0){
                  $storesystemnames  = array();
                  foreach($storeorderprocessors as $storeorderprocessor){
                      $StoreSystem = Store::where('id', $storeorderprocessor['store_id'])->first();
                      $Storesystem = $StoreSystem->toArray();
                      array_push($storesystemnames, $Storesystem['store_name']);
                  }
                  $storesystems = implode(', ', $storesystemnames);
                }else{
                  $storesystems = "No store assigned";
                }
                $OrderProcessor['storesystems'] = $storesystems;
                $OrderProcessorsdetails1[]      = $OrderProcessor;
            }
          } 
          $OrderProcessorsdetails['data']             = $OrderProcessorsdetails1;
          $OrderProcessorsdetails['current_page']     = $OrderProcessors['current_page'];
          $OrderProcessorsdetails['first_page_url']   = $OrderProcessors['first_page_url'];
          $OrderProcessorsdetails['from']             = $OrderProcessors['from'];
          $OrderProcessorsdetails['last_page']        = $OrderProcessors['last_page'];
          $OrderProcessorsdetails['last_page_url']    = $OrderProcessors['last_page_url'];
          $OrderProcessorsdetails['next_page_url']    = $OrderProcessors['next_page_url'];
          $OrderProcessorsdetails['path']             = $OrderProcessors['path'];
          $OrderProcessorsdetails['per_page']         = $OrderProcessors['per_page'];
          $OrderProcessorsdetails['prev_page_url']    = $OrderProcessors['prev_page_url'];
          $OrderProcessorsdetails['to']               = $OrderProcessors['to'];
          $OrderProcessorsdetails['total']            = $OrderProcessors['total'];

          if(count($OrderProcessorsdetails) > 0){
              return response()->json([
                              'requestId' => strtolower(Str::random(30)),
                              'statusCode' => 200,
                              'records'=> (object)$OrderProcessorsdetails,
                            ]);  
          }else{
              return response()->json([
                          'requestId' => strtolower(Str::random(30)),
                          'statusCode' => 403,
                          'message'=>"No OrderProcessor is created by this user."
                        ]);  
          }
     
    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

          return response()->json(['token_expired'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

          return response()->json(['token_invalid'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

          return response()->json(['token_absent'], $e->getStatusCode());
    }
  }


/*    public function myProcessors(Request $request) {

      try{

        if (! $user = JWTAuth::parseToken()->authenticate()) {

          return response()->json(['user_not_found'], 404);

        } else {


          $searchdata = $request->searchdata; 

          $query  = OrderProcessor::where('created_by' ,$user->id);
                    if ($searchdata) { 
                     $keyword = $searchdata;
                     $query->where('first_name', 'LIKE', '%' . $keyword . '%');
                     $query->orWhere('last_name', 'LIKE', '%' . $keyword . '%');
                     $query->orWhere('phone', 'LIKE', '%' . $keyword . '%');
                     $query->orWhere('email', 'LIKE', '%' . $keyword . '%');
                    }
                    $query->orderBy('id','DESC');
          $Orderprocessors = $query->paginate(10);
          
          $OrderProcessors = $Orderprocessors->toArray();

          DB::enableQueryLog();
          $orderDetails = OrderProcessor::join('store_order_processor', 'order_processor.id','=','store_order_processor.processor_id')
                                ->join('stores', 'store_order_processor.store_id','=','stores.id')
                                ->where('order_processor.created_by','=',$user->id)
                                ->where('stores.status','=','1')
                                ->where('store_order_processor.status','=','1')
                                ->select('order_processor.*','store_order_processor.id as opID','stores.store_name','stores.slug')
                                ->where('order_processor.status','=','1')
                                ->orderby('id','DESC')
                                ->get()
                                ->toArray(); 

           $query = DB::getQueryLog();

           print_r($query);
           echo '<pre>';
           print_r($orderDetails);
           echo '</pre>';   
           exit;                 

          $queryOrderProcessors = OrderProcessor::query()->with('storeorderProcessor');
          if ($searchdata) { 
           $keyword = $searchdata;
           $queryOrderProcessors->where('first_name', 'LIKE', '%' . $keyword . '%');
           $queryOrderProcessors->orWhere('last_name', 'LIKE', '%' . $keyword . '%');
           $queryOrderProcessors->orWhere('phone', 'LIKE', '%' . $keyword . '%');
           $queryOrderProcessors->orWhere('email', 'LIKE', '%' . $keyword . '%');
          }
          $queryOrderProcessors->where('created_by',$user->id)->orderby('id','DESC');
          $OrderProcessorsRes =  $queryOrderProcessors->paginate(Config::get('constant.pagination')); 

          $OrderProcessors = $OrderProcessorsRes->toArray();

          if(count($OrderProcessors) > 0){
              return response()->json([
                              'requestId' => strtolower(Str::random(30)),
                              'statusCode' => 200,
                              'records'=> $OrderProcessors,
                            ]);  
          }else{
              return response()->json([
                          'requestId' => strtolower(Str::random(30)),
                          'statusCode' => 403,
                          'message'=>"No OrderProcessor is created by this user."
                        ]);  
          }
      }
    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

          return response()->json(['token_expired'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

          return response()->json(['token_invalid'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

          return response()->json(['token_absent'], $e->getStatusCode());
    }
  }*/

    public function viewProcessor(Request $request,$id) {

      try{

          $processorDetail = OrderProcessor::where('id',$id)->first();

          if($processorDetail){

            $processor_str = StoreOrderProcessor::where(['processor_id' => $id, 'status' => '1'])->get();

            $storeName = '';
            $storeId = array();

            foreach ($processor_str as $processorstr) {
                  $storeId[]= $processorstr->store_id;
                  $str_name = Store::where('id',$processorstr->store_id)->first();
                  $storeName.= $str_name->store_name.',';
            }

            $storeName  = rtrim($storeName,',');
           // $storeId    = rtrim($storeId,',');

            $processorDetail['storename'] =  $storeName;
            $processorDetail['storeid']   =  $storeId;
            if($processorDetail->processor_image != ""){
                $processorDetail['imgurl']   = url('/public').'/uploads/processorimage/'.$processorDetail->processor_image;
               }else{
                $processorDetail['imgurl']   = "noimage";
              }

               return response()->json([
                              'requestId'  => strtolower(Str::random(30)),
                              'statusCode' => 200,
                              'message'    => $processorDetail,
                             ]);  
          }else{
                return response()->json([
                              'requestId'   => strtolower(Str::random(30)),
                              'statusCode'  => 403,
                              'message'     => "No OrderProcessor is created by this user."
                            ]);  
        }
 
    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

          return response()->json(['token_expired'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

          return response()->json(['token_invalid'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

          return response()->json(['token_absent'], $e->getStatusCode());
    }
  }

  public function suspendProcessor(Request $request,$processor_id) {

     try{

        if (! $user = JWTAuth::parseToken()->authenticate()) {

          return response()->json(['user_not_found'], 404);

        } else {

        $storeprocessorsDetail = StoreOrderProcessor::where(['processor_id' =>$processor_id])->get();
        $processorsDetail = OrderProcessor::where(['id' =>$processor_id])->first();

       if((count($storeprocessorsDetail)>0)||($processorsDetail->is_fulfillment_center=='1')){

/*            $updateDetail = StoreOrderProcessor::where(['store_id'=>$store_id, 'processor_id' =>$processor_id])->               update(['status' => '0']);*/

            return response()->json([
                            'requestId' => strtolower(Str::random(30)),
                            'statusCode' => 403,
                            'status'    =>'Warning',
                            'message'=> "Processor can not be suspended."
                          ]);  
        }else{

           $updateDetail = OrderProcessor::where(['id' =>$processor_id])->update(['status' => '0']);
          return response()->json([
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode' => 200,
                        'status'    =>'Success',
                        'message'=>"Processor suspended successfully."
                      ]);  
        }
      }
    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

          return response()->json(['token_expired'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

          return response()->json(['token_invalid'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

          return response()->json(['token_absent'], $e->getStatusCode());
    }

   }

public function allOrders(Request $request) {

     try{

        $userid      = $request->userid;

        $order_type     = trim($request->order_type);
        $allstores      = Store::where(['owner_id' =>$userid,'status' => 1])->get();
 
        if(count($allstores)>0){
          $arr =array();
          foreach ($allstores as $allstore) {
            array_push($arr , $allstore->id);
          }

       if($order_type == "completed"){

          $type_order =StoreOrder::whereIN('store_id', $arr)->where('order_status','completed')->orderby('id', 'DESC')->paginate(10);

               if(count($type_order)> 0){
               $newshipped_order = $type_order;
              }else{
               $newshipped_order = "No records for pending orders.";
              }
          
          return response()->json([
                            'requestId' => strtolower(Str::random(30)),
                            'statusCode' => 200,
                            'message'=> $newshipped_order
                          ]);  
        }elseif($order_type == "partially completed"){

          $type_order = StoreOrder::whereIN('store_id', $arr)->where('order_status','partiallycompleted')->orderby('id', 'DESC')->paginate(10);
         
              if(count($type_order)> 0){
               $newshipped_order = $type_order;
              }else{
               $newshipped_order = "No records for partially completed orders.";
              }
          
          return response()->json([
                            'requestId' => strtolower(Str::random(30)),
                            'statusCode' => 200,
                            'message'=> $newshipped_order
                          ]);  
        }elseif($order_type == "pending"){

          $type_order =StoreOrder::whereIN('store_id', $arr)->where('order_status','pending')->orderby('id', 'DESC')->paginate(10);
         
              if(count($type_order)> 0){
               $newshipped_order = $type_order;
              }else{
               $newshipped_order = "No records for pending orders.";
              }
          
          return response()->json([
                            'requestId' => strtolower(Str::random(30)),
                            'statusCode' => 200,
                            'message'=> $newshipped_order
                          ]);  
        }else{
            $newshipped_order =StoreOrder::whereIN('store_id', $arr)->orderby('id', 'DESC')->paginate(10);
            return response()->json([
                              'requestId' => strtolower(Str::random(30)),
                              'statusCode' => 200,
                              'message'=> $newshipped_order
                            ]);  
        }
      }else{
          return response()->json([
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode' => 403,
                        'message'=>"No store is assigned to the processor."
                      ]);  
          }
    }catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

          return response()->json(['token_expired'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

          return response()->json(['token_invalid'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

          return response()->json(['token_absent'], $e->getStatusCode());
    }
 }


   public function assignStores(Request $request) {

      try{

            $userid      = $request->userid;
            $rules = [];
            $rules['processor_id'] = 'required|regex:/^[0-9_\s]+$/';
            $rules['assignedstores'] = 'required';

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) { 
              return response()->json([
                            'requestId' => strtolower(Str::random(30)),
                            'statusCode' => 200,
                            'message'=>$validator->errors()
                            ]);            
            }

          $assignedstores = $request->assignedstores; 
          $processor_id   = $request->processor_id;
        
          if($assignedstores){

            foreach ($assignedstores as $key => $storeid) {

              $stores = Store::where(['owner_id'=>$userid ,'id'=>$storeid])->first();

              if($stores){

              $processorstore = StoreOrderProcessor::where(['processor_id'=>$processor_id ,'store_id'=>$storeid])
                                ->first();
              if($processorstore){
                if($processorstore->status == '0')
                {
                  $updateDetail = StoreOrderProcessor::where(['store_id'=>$storeid, 'processor_id' =>$processor_id]              )->update(['status' => '1']);
                }
                $message = "Stores are assigned successfully.";

              }else{

              $store_assign = StoreOrderProcessor::insert(
                              ['store_id' => $storeid, 'processor_id' =>$processor_id ,'owner_id'=> $userid ,'store_system_id'=>$stores->store_system_id]
                              );
                $message = "Stores are assigned successfully.";
              }
            }else{
               return response()->json([
                              'requestId' => strtolower(Str::random(30)),
                              'statusCode' => 403,
                              'message'=>"Store is not registered by you."
                            ]); 
              }
            }
            return response()->json([
                          'requestId' => strtolower(Str::random(30)),
                          'statusCode' => 403,
                          'message'=>$message
                        ]); 

          }
    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

          return response()->json(['token_expired'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

          return response()->json(['token_invalid'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

          return response()->json(['token_absent'], $e->getStatusCode());
    }
  }

}