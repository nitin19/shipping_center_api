<?php

namespace App\Http\Controllers\Api;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Models\User; 
use App\Models\Store;
use App\Models\StoreOrder;
use App\Models\StoreOrderItem;
use App\Models\StoreSystem;
use App\Models\StoreOrderProcessor;
use App\Models\Warehouses;
use App\Models\OrderProcessor;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Config;
use Validator;
use URL;
use Mail;

use Carbon\Carbon; 
use JWTFactory;
use JWTAuth;
use Log;
use App\Models\Userapistatistics;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;
use Session;
use Illuminate\Support\Facades\Redirect;

class ProcessorDashboardController extends Controller 
{
   
  public function getStoreCount(Request $request)
  {     
        $userid = $request->userid;

        $storescount = StoreOrderProcessor::where(['processor_id' => $userid, 'status' => '1'])->count(); 
        $response = [
            'status' => 'Success',
            'total_stores' => $storescount
           ];

        return response()->json($response);
    }

    public function getProductsCount(Request $request) {


        $ProcessorId      = $request->userid;

        $processordetails = OrderProcessor::where(['id' => $ProcessorId, 'status' => '1'])->first(); 

        $orderDetails   = StoreOrderItem::join('store_orders', 'store_order_items.store_order_id','=','store_orders.id')
                                ->join('stores', 'store_orders.store_id','=','stores.id')
                                ->where('stores.owner_id','=',$processordetails->created_by)
                                ->where('stores.status','=','1')
                                ->where('stores.deleted','=','0')
                                ->where('store_orders.status','=','1')
                                ->select('store_order_items.*','store_orders.id as OrderId','stores.store_name','stores.slug')
                                ->where('store_order_items.status','=','1')
                                ->orderby('id','DESC')
                                ->get()
                                ->toArray();

        return response()->json(['status' => 'Success', 'total_products' =>count($orderDetails)]); 
                
    }
       public function getOrdersCount(Request $request){

        $ProcessorId = $request->userid;

        $processordetails = OrderProcessor::where(['id' => $ProcessorId, 'status' => '1'])->first(); 
       
        $queryStoreOrder = StoreOrder::query();
        $storeOrders =  $queryStoreOrder->join('stores', 'store_orders.store_id','=','stores.id')
                                        ->where('stores.owner_id','=',$processordetails->created_by)
                                        ->where('stores.status','=','1')
                                        ->where('stores.deleted','=','0')
                                        ->select('store_orders.*','store_orders.id as OrderId','stores.store_name','stores.slug')
                                        ->where('store_orders.status','=','1')
                                        ->orderby('id','DESC')
                                        ->get()
                                        ->toArray(); 

        if(empty($storeOrders)) {
            return response()->json(['status' => 'Success', 'total_orders' =>count($storeOrders)]);  
        }
        return response()->json(['status' => 'Success', 'total_orders' =>count($storeOrders)]);  
    }

    public function getWarehousesList(Request $request)
    {     

        $ProcessorId = $request->userid;

        $processordetails = OrderProcessor::where(['id' => $ProcessorId, 'status' => '1'])->first(); 

        $warehouseslist = Warehouses::where(['store_owner_id' => $processordetails->created_by, 'status' => '1'])->take(5)->orderBy('id', 'DESC')->get(); 
        $response = [
            'status' => 'Success',
            'total_warehouses' => $warehouseslist
           ];

        return response()->json($response);
    }
}


