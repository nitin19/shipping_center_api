<?php

namespace App\Http\Controllers\Api\Creditcard;

use App\Models\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Auth;
use DB;
use App\Models\Country;
use App\Models\UserCreditCards;
use App\Models\BlackListCards;
use App\Models\Profile;
use App\Models\PaymentLogs;
use App\Models\CardVerifications;
use App\Models\Payments;
use App\Models\Userapistatistics;
use Exception;
use Log;
use Session;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class CardsInfoController extends Controller
{

    public $successStatus = 200;

    public function __construct()
    {
       //
    }
    public function getList(Request $request){
        try {
        $url   =  env('VIPPARCEL_URL').'accounts/card/getList';

        $headers = [
            'Authorization: '.$_SERVER['HTTP_AUTHORIZATION'],
         ];

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL            => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST  => 'GET',
        CURLOPT_HTTPHEADER     => $headers
        ));

        $result = curl_exec($curl);
        $response = json_decode($result);

        curl_close($curl);
        if($response){
            if($response->statusCode==200){
                $responsemsg = [
                'requestId' => $response->requestId,
                'totalRecords' => $response->totalRecords,
                'records'    => $response->records,
                'statusCode' => $response->statusCode,
                ];
                return response()->json($responsemsg);
            } else {
                $responsemsg = [
                'requestId' => $response->requestId,
                'message' => $response->message,
                'statusCode'    => $response->statusCode,
                ];
                return response()->json($responsemsg);
                }
            
        } else {
            $responsemsg = [
            'requestId' => strtolower(Str::random(30)),
            'message' => 'Error',
            'statusCode'    => 403,
            'status' => 0,
            ];
            return response()->json($responsemsg);
        }
        
        } catch (\Exception $e) {
         return response()->json(['status' => 0, 'message' => $e->getMessage()], 403);
        } 
    
    }

    public function create(Request $request){
        try {
        $cardData = array("year" => $request->year, "month" => $request->month, "name" => $request->name, "number" => $request->number, "cvv" => $request->cvv, "default" => $request->default);
        $url   =  env('VIPPARCEL_URL').'accounts/card/add';

        $headers = [
            'Authorization: '.$_SERVER['HTTP_AUTHORIZATION'],
         ];

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL            => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST  => 'POST',
        CURLOPT_POSTFIELDS  => $cardData,
        CURLOPT_HTTPHEADER     => $headers
        ));

        $result = curl_exec($curl);
        $response = json_decode($result);

        curl_close($curl);
        if($response){

            $responsemsg = [
            'requestId' => $response->requestId,
            'message' => $response->message,
            'statusCode'    => $response->statusCode,
            'status' => $response->status,
            ];
            return response()->json($responsemsg);
        } else {
            $responsemsg = [
            'requestId' => strtolower(Str::random(30)),
            'message' => 'Error',
            'statusCode'    => 403,
            'status' => 0,
            ];
            return response()->json($responsemsg);
        }
        
        } catch (\Exception $e) {
         return response()->json(['status' => 0, 'message' => $e->getMessage()], 403);
        } 
        
    }

    public function card_verification(Request $request){
        try {
        $cardData = array("cardId" => $request->cardId);
        $url   =  env('VIPPARCEL_URL').'accounts/card/verify';

        $headers = [
            'Authorization: '.$_SERVER['HTTP_AUTHORIZATION'],
         ];

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL            => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST  => 'POST',
        CURLOPT_POSTFIELDS  => $cardData,
        CURLOPT_HTTPHEADER     => $headers
        ));

        $result = curl_exec($curl);
        $response = json_decode($result);

        curl_close($curl);
        if($response){

            $responsemsg = [
            'requestId' => $response->requestId,
            'message' => $response->message,
            'statusCode'    => $response->statusCode,
            'status' => $response->status,
            ];
            return response()->json($responsemsg);
        } else {
            $responsemsg = [
            'requestId' => strtolower(Str::random(30)),
            'message' => 'Error',
            'statusCode'    => 403,
            'status' => 0,
            ];
            return response()->json($responsemsg);
        }
        
        } catch (\Exception $e) {
         return response()->json(['status' => 0, 'message' => $e->getMessage()], 403);
        }
        
    }

    public function charge_verification(Request $request){
        try {

        $cardData = array("cardId" => $request->cardId, "amount1" => $request->amount1, "amount2" => $request->amount2);
        $url   =  env('VIPPARCEL_URL').'accounts/card/chargeverify';

        $headers = [
            'Authorization: '.$_SERVER['HTTP_AUTHORIZATION'],
         ];

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL            => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST  => 'POST',
        CURLOPT_POSTFIELDS  => $cardData,
        CURLOPT_HTTPHEADER     => $headers
        ));

        $result = curl_exec($curl);
        $response = json_decode($result);

        curl_close($curl);
        if($response){
            $responsemsg = [
            'requestId' => $response->requestId,
            'message' => $response->message,
            'statusCode'    => $response->statusCode,
            'status' => $response->status,
            ];
            return response()->json($responsemsg);
        } else {
            $responsemsg = [
            'requestId' => strtolower(Str::random(30)),
            'message' => 'Page not found',
            'statusCode'    => 404,
            'status' => 0,
            ];
            return response()->json($responsemsg);
        }
        
        } catch (\Exception $e) {
         return response()->json(['status' => 0, 'message' => $e->getMessage()], 403);
        }
        
    }

    public function IsCardVerified(Request $request){
        try {
        $url = env('VIPPARCEL_URL').'accounts/card/iscardverified/?cardId='.$request->cardId;

        $opts = [
            "http" => [
                "method" => "GET",
                "header" => "Authorization: ".$_SERVER['HTTP_AUTHORIZATION'],
            ]
        ];

        $context = stream_context_create($opts);

        $result = file_get_contents($url, false, $context);
        $response = json_decode($result);

        if($response){
            $responsemsg = [
                'requestId' => $response->requestId,
                'statusCode' => 200,
                'message'    => $response->message,
                'status' => $response->status,
               ];
               return response()->json($responsemsg);
        } else {
            $responsemsg = [
                'requestId' => strtolower(Str::random(30)),
                'statusCode' => 200,
                'message'    => $response->message,
                'status' => 0,
               ];
               return response()->json($responsemsg);
        }
        } catch (\Exception $e) {
         return response()->json(['status' => 0, 'message' => $e->getMessage()], 403);
        }

    }

    public function getdetails(Request $request){
        try {
        $url = env('VIPPARCEL_URL').'accounts/card/details/?cardId='.$request->cardId;

        $opts = [
            "http" => [
                "method" => "GET",
                "header" => "Authorization: ".$_SERVER['HTTP_AUTHORIZATION'],
            ]
        ];

        $context = stream_context_create($opts);

        $result = file_get_contents($url, false, $context);
        $response = json_decode($result);

        if($response){
            $responsemsg = [
                'requestId' => $response->requestId,
                'statusCode' => 200,
                'carddetails'    => $response->carddetails,
                'status' => 1,
               ];
               return response()->json($responsemsg);
        } else {
            $responsemsg = [
                'requestId' => strtolower(Str::random(30)),
                'statusCode' => 200,
                'carddetails'    => '',
                'status' => 0,
               ];
               return response()->json($responsemsg);
        }
        } catch (\Exception $e) {
         return response()->json(['status' => 0, 'message' => $e->getMessage()], 403);
        }
    }

    public function deletecard(Request $request){
        try {

        $cardData = array("cardId" => $request->cardId);
        $url   =  env('VIPPARCEL_URL').'accounts/card/delete';

        $headers = [
            'Authorization: '.$_SERVER['HTTP_AUTHORIZATION'],
         ];

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL            => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST  => 'POST',
        CURLOPT_POSTFIELDS  => $cardData,
        CURLOPT_HTTPHEADER     => $headers
        ));

        $result = curl_exec($curl);
        $response = json_decode($result);

        curl_close($curl);
        if($response){
            $responsemsg = [
            'requestId' => $response->requestId,
            'message' => $response->message,
            'statusCode'    => $response->statusCode,
            'status' => $response->status,
            ];
            return response()->json($responsemsg);
        } else {
            $responsemsg = [
            'requestId' => strtolower(Str::random(30)),
            'message' => 'Page not found',
            'statusCode'    => 404,
            'status' => 0,
            ];
            return response()->json($responsemsg);
        }
        
        } catch (\Exception $e) {
         return response()->json(['status' => 0, 'message' => $e->getMessage()], 403);
        }
    }


    public function defaultcard(Request $request){
        try {

        $cardData = array("cardId" => $request->cardId);
        $url   =  env('VIPPARCEL_URL').'accounts/card/default';

        $headers = [
            'Authorization: '.$_SERVER['HTTP_AUTHORIZATION'],
         ];

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL            => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST  => 'POST',
        CURLOPT_POSTFIELDS  => $cardData,
        CURLOPT_HTTPHEADER     => $headers
        ));

        $result = curl_exec($curl);
        $response = json_decode($result);

        curl_close($curl);
        if($response){
            $responsemsg = [
            'requestId' => $response->requestId,
            'message' => $response->message,
            'statusCode'    => $response->statusCode,
            'status' => $response->status,
            ];
            return response()->json($responsemsg);
        } else {
            $responsemsg = [
            'requestId' => strtolower(Str::random(30)),
            'message' => 'Page not found',
            'statusCode'    => 404,
            'status' => 0,
            ];
            return response()->json($responsemsg);
        }
        
        } catch (\Exception $e) {
         return response()->json(['status' => 0, 'message' => $e->getMessage()], 403);
        }
    }

    public function getverifycard(Request $request){
        try {
        $url   =  env('VIPPARCEL_URL').'accounts/card/getverifycard';

        $headers = [
            'Authorization: '.$_SERVER['HTTP_AUTHORIZATION'],
         ];

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL            => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST  => 'GET',
        CURLOPT_HTTPHEADER     => $headers
        ));

        $result = curl_exec($curl);
        $response = json_decode($result);

        curl_close($curl);
        if($response){
            if($response->statusCode==200){
                $responsemsg = [
                'requestId' => $response->requestId,
                'totalRecords' => $response->totalRecords,
                'records'    => $response->records,
                'statusCode' => $response->statusCode,
                ];
                return response()->json($responsemsg);
            } else {
                $responsemsg = [
                'requestId' => $response->requestId,
                'message' => $response->message,
                'statusCode'    => $response->statusCode,
                ];
                return response()->json($responsemsg);
                }
            
        } else {
            $responsemsg = [
            'requestId' => strtolower(Str::random(30)),
            'message' => 'Error',
            'statusCode'    => 403,
            'status' => 0,
            ];
            return response()->json($responsemsg);
        }
        
        } catch (\Exception $e) {
         return response()->json(['status' => 0, 'message' => $e->getMessage()], 403);
        }
    
    }
}