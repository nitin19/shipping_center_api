<?php
namespace App\Http\Services;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\Models\Store; 
use App\Models\StoreOrder;
use App\Models\StoreOrderItem;
use Validator;
use URL;
use Illuminate\Support\Facades\Auth; 
use Sonnenglas\AmazonMws\AmazonOrderList;
use Sonnenglas\AmazonMws\AmazonOrderItemList;
use Sonnenglas\AmazonMws\AmazonServiceStatus;
use App\MarketPlace\amazon\Services;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

/**
  * In store Controller amazon common functionality is implemented here
  *
  *@params 
  *@return 
  */ 


trait Amazon
{
    
    public $successStatus = 200;    
     
    /**
     * Checks seller is present in amazonmws or not
     *
     *@params array sellerId
     *@return Boolean
     */
    public function amusCheckAccountStatus($data){
        $sellerId = $data['id'];
        $url = "https://www.amazon.com/sp?seller=".$sellerId;
        $handle = curl_init($url);
        curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($handle);
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        if($httpCode == 301 || $httpCode == 200 ) {
           $status = true;
        }else{
           $status = false; 
        }
        curl_close($handle);
        return response()->json(['status'=>$status], $this->successStatus); 
    } 

    public function amusAccountStatus($data){
        $sellerId = $data['id'];
        $url = "https://www.amazon.com/sp?seller=".$sellerId;
        $handle = curl_init($url);
        curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($handle);
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        if($httpCode == 301 || $httpCode == 200 ) {
           $status = true;
        }else{
           $status = false; 
        }
        curl_close($handle);
        return $status; 
    }    
    
    /**
     * Stores user Credential into our application
     *
     *@params array [$id,$key,storeName] , int $userId
     *@return Boolean
     */
    public function amusSaveStoreCredentials($data){
       $validator = Validator::make($data, [ 
           'id' => 'required',
           'storeName' => 'required',
           'key' => 'required',
            
       ]);
       if ($validator->fails()) { 
             return $response = [
            'requestId' => strtolower(Str::random(30)),
            'statusCode' => 205,
            'message' => "Validation error",
            'status' => "0",
            ];          
        }
       else{   
         $val['merchantId'] = $data['id'];
         $val['awsKey'] = $data['key'];         
         $val['marketplaceId'] = config('amazon-mws.awsMarketPlaceId');
         $auth = json_encode($val);
         $stores = new Store;        
         $stores->owner_id = $data['userid'];
         $stores->store_system_id = 1;
         $stores->slug = rand(34,98).time().rand(340,99999);
         $stores->auth = $auth;
         $stores->store_name = $data['storeName'];
         $stores->status = 1;
         $stores->save();
         $success = 1;
             return $response = [
            'requestId' => strtolower(Str::random(30)),
            'statusCode' => 201,
            'message' => "success",
            'status' => "1",
            ]; 
        }
    }

    /**
     * Retriving auth credentials from database for a store
     *
     *@params int $storeId
     *@return array [merchantId,marketplaceId,awsKey]
     */ 
    public function amusgetAuthCredentials($storeId){
        $stores = Store::find($storeId);
        $auth = json_decode($stores->auth);        
        $authVariable['merchantId'] = $auth->merchantId;
        $authVariable['marketplaceId'] = $auth->marketplaceId;
        $authVariable['keyId']      = $auth->awsKey;        
        $authVariable['secretKey'] = config('amazon-mws.awsDeveloperKey');        
        $authVariable['amazonServiceUrl'] = config('amazon-mws.awsServiceUrl');
        Config::set('amazon-mws.store.myStore.merchantId', $auth->merchantId);
        Config::set('amazon-mws.store.myStore.marketplaceId', $auth->marketplaceId);
        Config::set('amazon-mws.store.myStore.keyId', $auth->awsKey);
        Config::set('amazon-mws.store.myStore.secretKey', config('amazon-mws.awsDeveloperKey'));
        return $authVariable;
    } 
   
   
    /**
     * Retriving Orders Item from a amazon store
     *
     *@params int $orderId, int $storeOrderId , array $auth
     *@return int number of orders Items imported for a given order
     */ 
    public function amusGetOrderItem($orderId, $storeOrderId, $auth){        
        $amzorders = new AmazonOrderItemList('myStore');
        $amzorders->setOrderId($orderId);
        $amzorders->setUseToken();
        $amzorders->fetchItems();
        $listOrdersItems = $amzorders->getItems();
        $countOrderItem = 0;
        foreach($listOrdersItems as $item){
            $orderItemId = $item['OrderItemId'];
            $haverecords = StoreOrderItem::wherestoreOrderId($storeOrderId)->whereorderItemId($orderItemId)->count();
            if(!$haverecords)
            {
                $storeItems = new StoreOrderItem;
                $storeItems->store_order_id = $storeOrderId;
                $storeItems->asin = $item['ASIN'];
                $storeItems->item_name = $item['Title'];
                $storeItems->sellersku = $item['SellerSKU'];
                $storeItems->order_item_id = $orderItemId;
                $storeItems->item_details = json_encode($item);  
                $storeItems->item_status = 'new';
                $storeItems->weight = '';
                $storeItems->item_qty = $item['QuantityOrdered'];
                $storeItems->save();    
                $countOrderItem ++;        
           }
        }
        return $countOrderItem;
    }
    

    /**
     * Retriving Orders from a amazon store
     *
     *@params int $storeId
     *@return int number of orders imported
     */
    public function amusGetamazonorders($storeId) {
        $storecredentials = $this->amusgetAuthCredentials($storeId);
        $amz = new AmazonOrderList('myStore'); 
        $amz->setLimits('Modified', config('amazon-mws.awsDefaultPullTime'));
        $amz->setFulfillmentChannelFilter("MFN"); //no Amazon-fulfilled orders
        $amz->setOrderStatusFilter(config('amazon-mws.awsPullOrderTypes'));
        $amz->setUseToken(); //Amazon sends orders 100 at a time, but we want them all
        $amz->fetchOrders(); // var_dump($amz->getList());
        $list = $amz->getList();
        // print_r($list); exit;
        $numberofOrders = 0;
        $numberofOrderItems = 0;
        foreach ($list as $order) {
        $orderId = $order->getAmazonOrderId(); 
        $haverecords = StoreOrder::wherestoreId($storeId)->whereorderId($orderId)->count();
        if(!$haverecords){                
            $storeOrders = new StoreOrder;
            $storeOrders->order_date = trim(str_replace(['T', 'Z']," ",$order->getPurchaseDate()));
            $storeOrders->order_id = $orderId;
            $storeOrders->store_id = $storeId;
            $orderData['ShippingService'] = $order->getShipServiceLevel();
            $shippingAddress = $order->getShippingAddress();
            $orderData['ShippingAddress']["Name"] = $shippingAddress['Name'];
            $orderData['ShippingAddress']["Street1"] = $shippingAddress['AddressLine1'];
            $orderData['ShippingAddress']["Street2"] = $shippingAddress['AddressLine2'] . $shippingAddress['AddressLine3'];
            $orderData['ShippingAddress']["CityName"] = $shippingAddress['City'];
            $orderData['ShippingAddress']["StateOrProvince"] = $shippingAddress['StateOrRegion'];
            $orderData['ShippingAddress']["Country"] = $shippingAddress['CountryCode'];
            $orderData['ShippingAddress']["CountryName"] = $shippingAddress['County'];
            $orderData['ShippingAddress']["Phone"] = $shippingAddress['Phone'];
            $orderData['ShippingAddress']["PostalCode"] = $shippingAddress['PostalCode'];
            $orderData['ShippingAddress']["AddressID"] = 'NA';
            $orderData['ShippingAddress']["AddressOwner"] = 'NA';
            $orderData['ShippingAddress']["ExternalAddressID"] = 'NA';
            $orderData['BuyerName'] = $order->getBuyerName();        
            $orderData['BuyerEmail'] = $order->getBuyerEmail();
            $orderData['PurchaseDate'] = $order->getPurchaseDate();
            $orderData['OrderStatus'] = $order->getOrderStatus();
            $orderData['OrderTotal'] = $order->getOrderTotal();
            $storeOrders->order_details = json_encode($orderData); 
            $storeOrders->order_status = 'new';    
            $storeOrders->shipping_address = $shippingAddress['AddressLine1'].' '.$shippingAddress['AddressLine2'].' '.$shippingAddress['AddressLine3'].', '.$shippingAddress['City'].', '.$shippingAddress['StateOrRegion'].', '.$shippingAddress['CountryCode'].', '.$shippingAddress['PostalCode']; 
            $storeOrders->customer_name = $shippingAddress['Name'];
            $storeOrders->customer_email = $orderData['BuyerEmail'];
            $storeOrders->customer_phone = $shippingAddress['Phone'];           
            $storeOrders->save();
            $id = $storeOrders->id;
            $numberofOrderItem = $this->amusGetOrderItem($orderId,$id,$storecredentials);
            $numberofOrderItems += $numberofOrderItem;
            $numberofOrders++;
        }
       }    
       return array("status" => 1, "orders" =>$numberofOrders, "orderItems" => $numberofOrderItems, "message" => "Sync Successfully");
    }


}
