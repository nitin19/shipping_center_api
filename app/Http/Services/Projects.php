<?php 
namespace App\Http\Services;

require 'vendor/autoload.php';
use App\Http\Controllers\Controller; 
use Illuminate\Support\Facades\Redirect;
use App\Models\Store; 
use App\Models\StoreOrder;
use App\Models\StoreOrderItem;
use Illuminate\Http\Request; 
use Validator;
use URL;
use DB;

trait Projects
{
    public function CommonConditions($processor_id='',$store_id='',$order_id='',$order_item_id='',$search_keyword=''){

    	    $query = DB::table('store_orders');

      		if ($processor_id) {
		            $query->orwhere('processor_id', $processor_id);
         	}
         	if ($store_id) {
		            $query->orwhere('store_id', $store_id);
		    }
		    if ($order_id) {
		            $query->orwhere('order_id', $order_id);
		    }
		    if ($search_keyword) { 
		       $keyword = $search_keyword;

 		       $query->where('order_details', 'LIKE', '%' . $keyword . '%'); 
 		    }
            $query->orderBy('id','DESC');
            $data = $query->paginate(1);     
        	return $data; 
    }
}