<?php
namespace App\Http\Services;

require 'vendor/autoload.php';
use App\Http\Controllers\Controller; 
use Illuminate\Support\Facades\Redirect;
use App\Models\Store; 
use App\Models\StoreOrder;
use App\Models\StoreOrderItem;
use Illuminate\Http\Request;
use Illuminate\Support\Str; 
use \DTS\eBaySDK\Trading\Services;
use \DTS\eBaySDK\Trading\Types;
use \DTS\eBaySDK\Trading\Enums;
use Validator;
use URL;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Tinyrocket\Magento\Magento;
use App\Tinyrocket\Magento\MagentoServiceProvider;


trait MagentoService
{

  private $config;
  private $request;

     public function MagentoSaveStoreCredentials($data){

     	$storeUrl = $data['storeurl'];
     	$username = $data['username'];
     	$password = $data['password'];
		$userData = array("username" => $username, "password" => $password);
		$url   =  $storeUrl."/rest/V1/integration/admin/token";

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $userData);

		// execute!
		$response = curl_exec($ch);
		$responseMessage = json_decode($response);

		// close the connection, release resources used
		curl_close($ch);

	     if(is_array($responseMessage)){
			return $response = [
			'requestId' => strtolower(Str::random(30)),
			'statusCode' => 400,
			'message' => "error",
			'status' => "0",
			];
        } else {
        	if($responseMessage!=''){
        		$userData = array("username" => $username, "password" => $password, "storeurl" => $storeUrl, "access_token" => $responseMessage);
        		$stores = new Store;        
				$stores->owner_id = $data['userid'];
				$stores->store_system_id = 4;
				$stores->slug = rand(34,98).time().rand(340,99999);
				$stores->auth = json_encode($userData);
				$stores->store_name = $data['storeName'];
				$stores->status = 1;
				$stores->save();
				return $response = [
				'requestId' => strtolower(Str::random(30)),
				'statusCode' => 201,
				'message' => "success",
				'status' => "1",
				];

        	} else {
        		return $response = [
				'requestId' => strtolower(Str::random(30)),
				'statusCode' => 400,
				'message' => "error",
				'status' => "0",
				];
        	}
        }
    }

    public function ebayGetMagentoOrders($storeId){
    	$stores = Store::find($storeId);
        $auth = json_decode($stores->auth);
      	$access_token = $auth->access_token;
      	$magento_store_url  = $auth->storeurl;

        $url   =  $magento_store_url."/rest/V1/orders?searchCriteria=''";

		$headers = [
            'Authorization: Bearer '.$access_token,
            'Content-Type: application/json',
         ];

		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL            => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_CUSTOMREQUEST  => 'GET',
		CURLOPT_HTTPHEADER     => $headers
		));
	     
	     $responseMessage = json_decode(curl_exec($curl));		     
	     $err = curl_error($curl);		     
	     curl_close($curl);
	     if(array_key_exists('message', $responseMessage)){
	     	return $response = [
			'requestId' => strtolower(Str::random(30)),
			'statusCode' => 400,
			'message' => "error",
			'status' => "0",
			];
	     } else {
	     	if($responseMessage->total_count > 0){
	     		$StoreOrders = $responseMessage->items;
	     		$orderitems = 0;
	     		$ordercount = 0;
	     		foreach($StoreOrders as $StoreOrder){
	     			$StoreOrderItems = $StoreOrder->items;
	     			$StoreOrderBillingAddress = $StoreOrder->billing_address;
	     			$orderStatus = $StoreOrder->status;

	     			$shippingaddress = implode(' ', $StoreOrderBillingAddress->street).', '.$StoreOrderBillingAddress->city. ', '.$StoreOrderBillingAddress->region.', '.$StoreOrderBillingAddress->country_id.', '.$StoreOrderBillingAddress->postcode;
	     			$shippingemail = $StoreOrderBillingAddress->email; 
	     			$shippingphone = $StoreOrderBillingAddress->telephone;
	     			$shippingcustomer = $StoreOrderBillingAddress->firstname.' '.$StoreOrderBillingAddress->lastname;

	     			$StoreOrderexist = StoreOrder::where('order_id','=', $StoreOrder->increment_id)->where('store_id','=', $storeId)->where('status','=', '1')->first();
	     			if($StoreOrderexist==''){
		     			$storeOrders = new StoreOrder;
		     			$storeOrders->order_date = $StoreOrder->created_at;
	              		$storeOrders->order_id = $StoreOrder->increment_id;
	              		$storeOrders->store_id = $storeId;
	              		$storeOrders->order_details = json_encode($StoreOrder);
	              		$storeOrders->order_status = 'new';
	              		$storeOrders->order_qty = $StoreOrder->total_qty_ordered;
	              		$storeOrders->shipping_address = $shippingaddress;
	              		$storeOrders->customer_name = $shippingcustomer;
	              		$storeOrders->customer_email = $shippingemail;
	              		$storeOrders->customer_phone = $shippingphone;
		     			$storeOrders->save();
		     			$ordercount++;
	              		$storeOrderId = $storeOrders->id;
	              		if($storeOrderId!=''){
			     			foreach($StoreOrderItems as $StoreOrderItem){
			     				$orderitemdetail = array('billing_address' => $StoreOrderBillingAddress, 'Item_details' => $StoreOrderItem);
			     				$storeItems = new StoreOrderItem;
			                    $storeItems->store_order_id = $storeOrderId;
			                    $storeItems->asin = 'NA';
			                    $storeItems->item_name = $StoreOrderItem->name;
			                    $storeItems->sellersku = $StoreOrderItem->sku;
			                    $storeItems->order_item_id = $StoreOrderItem->item_id;
			                    $storeItems->item_details = json_encode($orderitemdetail);  
			                    $storeItems->item_status = 'new';
			                    $storeItems->weight = $StoreOrderItem->weight;
			                    $storeItems->item_qty = $StoreOrderItem->qty_ordered;
			                    $storeItems->save();
			     				$orderitems ++;
			     			}
		     			}
	     			}
	     		}
	     		return $response = [
                'requestId' => strtolower(Str::random(30)),
                'statusCode' => 200,
                'message' => "success",
                'status' => "1",
                "orders" => $ordercount,
                "orderItems" => $orderitems 
                ];

	     	} else {
	     		return $response = [
	            'requestId' => strtolower(Str::random(30)),
	            'statusCode' => 200,
	            'message' => "success",
	            'status' => "1",
	            "orders" =>0,
	            "orderItems" => 0 
	            ];
	     	}
	     }


    }

}