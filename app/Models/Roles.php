<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
  const CREATED_AT = 'created';

  const UPDATED_AT = 'updated';

  protected $table = 'roles_users';

  protected $fillable = ['user_id', 'role_id'];

  
}
