<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreOrder extends Model
{
    protected $fillable = [
        'id', 'store_id', 'order_date', 'shipped_date', 'order_id', 'order_details', 'label_id', 'processed_by_owner', 'processor_id', 'status', 'order_status', 'tracking_id', 'order_qty', 'shipping_address', 'customer_name', 'customer_email', 'customer_phone'
    ];

    public function store()
    {
        return $this->belongsTo('App\Models\Store', 'store_id', 'id');
    }

    public function orderprocessor()
    {
        return $this->belongsTo('App\Models\OrderProcessor', 'processor_id', 'id');
    }

    public function orderItems()
    {
        return $this->hasMany('App\Models\StoreOrderItem','store_order_id','id');
    }

    public function orderStatus()
    {
        return $this->belongsTo(OrderStatus::class);
    }

    public function getOrderDetailsAttribute($value) 
    {
        return json_decode($value);
    }
}
