<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
  const CREATED_AT = 'created';

  const UPDATED_AT = 'updated';

  protected $table = 'profile';

  protected $fillable = ['user_id', 'last_name', 'first_name', 'middle_name', 'reg_last_name', 
    'reg_first_name', 'phone', 'created', 'updated', 'address', 'address_2', 'city', 'state', 
    'coutry_id', 'zip', 'skype', 'is_international', 'province', 'phone_number_info', 'phone_number_info_request', 'phone_number_info_error', 'driver_licence'];

  public function user()
  {
  	return $this->belongsTo(User::class);
  }  
}
