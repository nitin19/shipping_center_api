<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MailClass extends Model
{
  
  const CREATED_AT = 'created';

  const UPDATED_AT = 'updated';

  protected $table = 'label_mail_class';

  protected $fillable = [];

}
