<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    
    protected $hidden = array('owner_id', 'auth', 'created_by', 'updated_by', 'created_at', 'updated_at');

    protected $fillable = [
        'owner_id', 'store_system_id', 'auth', 'store_name', 'status', 'created_by', 'updated_by',
    ];

    public function system()
    {
        return $this->hasOne('App\Models\StoreSystem', 'id', 'store_system_id');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\StoreOrder', 'store_id', 'id');
    }

    public function orderProcessor()
    {
        return $this->hasMany('App\Models\StoreOrderProcessor', 'store_id', 'id');
    }

}
