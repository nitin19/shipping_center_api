<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCreditCards extends Model
{
  const CREATED_AT = 'created';

  const UPDATED_AT = 'updated';

  protected $table = 'user_credit_cards';

  protected $fillable = ['user_id', 'name', 'number', 'exp_month', 'exp_year', 'created', 'updated', 'address', 'is_default', 'is_verified', 'is_deleted'];

    
}
