<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlackListCards extends Model
{
  const CREATED_AT = 'created';

  const UPDATED_AT = 'updated';

  protected $table = 'black_list_cards';

  protected $fillable = ['number', 'created', 'updated'];

    
}
