<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Referrals extends Model
{
  
  const CREATED_AT = 'created';

  const UPDATED_AT = 'updated';

  protected $table = 'user_referrals';

  protected $fillable = ['referral_user_id', 'referrer_user_id', 'code', 'created', 'updated', 'inheriting_settings'];

   
}
