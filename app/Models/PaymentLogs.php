<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentLogs extends Model
{
  const CREATED_AT = 'created';

  protected $table = 'payment_logs';

  protected $fillable = ['user_id', 'service', 'txn_id', 'status', 'data', 'created'];

    
}
