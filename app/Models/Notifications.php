<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    protected $table = 'notifications'; 
    protected $fillable = [
        'notify_type',' user_id','sender','receiver','message','status'
        ];

    public function profile()
    {
        return $this->hasOne('App\Models\Profile', 'user_id', 'sender');
    }
    
    public function notification_types()
    {
        return $this->hasOne('App\Models\NotificationTypes', 'id', 'type');
    }
}