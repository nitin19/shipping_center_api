<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
	protected $table = 'order_status';
	
    protected $fillable = [
        'name', 'slug', 'status',  'created_by', 'updated_by',
    ];

    protected $hidden = [
        'created_by', 'updated_by', 'created_at','updated_at',
    ];

    public function storeOrder() {
        
        return $this->hasMany(StoreOrder::class);
    }
   
}
