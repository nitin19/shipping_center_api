<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ipinfo extends Model
{
  
  const CREATED_AT = 'created';

  const UPDATED_AT = 'updated';

  protected $table = 'ipinfo';

  protected $fillable = ['ip', 'country_code', 'org', 'hostname', 'city', 'region', 'lat', 'long', 'postal', 'updated'];

   
}
