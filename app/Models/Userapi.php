<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Userapi extends Model
{
  
  const CREATED_AT = 'created';

  const UPDATED_AT = 'updated';

  protected $table = 'user_api';

  protected $fillable = ['auth_token', 'local_token', 'user_id', 'active', 'last_request', 'created', 'updated'];

  
}
