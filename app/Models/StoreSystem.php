<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use URL;

class StoreSystem extends Model
{
    protected $fillable = [
        'name', 'status', 'title', 'logo_image', 'created_by', 'updated_by',
    ];

    protected $hidden = [
        'created_by', 'updated_by', 'created_at','updated_at',
    ]; 

    public function stores()
    {
        return $this->hasMany('App\Models\Store', 'id', 'store_system_id');
    }

    public function getLogoImageAttribute($value) 
    {
        return URL::to('/')."/public/images/marketplace_logo/".$value;
    }

}
