<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Http\Resources\UserResource;

use App\Models\UserRole;
use Carbon;

class OrderProcessor extends Authenticatable implements JWTSubject
{
  use Notifiable;

  protected $guard = 'orderprocesser';

  protected $table = 'order_processor';
  
  const CREATED_AT = 'created_at';

  const UPDATED_AT = 'updated_at';

   protected $fillable = [
        'first_name','last_name','email','phone','password','status','is_fulfillment_center','created_by','updated_by', 'created_at', 'updated_at',
    ];
  public function storeorderProcessor()
    {
        return $this->hasMany('App\Models\StoreOrderProcessor', 'processor_id', 'id');
    }
  public function getJWTIdentifier()
  {
      return $this->getKey();
  }

  public function getJWTCustomClaims()
  {
      return [];
  }

}
