<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
  
  const CREATED_AT = 'created';

  const UPDATED_AT = 'updated';

  protected $table = 'settings';

  protected $fillable = [];

  
}
