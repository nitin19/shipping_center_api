<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreOrderItem extends Model
{
    
	protected $table = 'store_order_items';

    protected $fillable = [
        'store_order_id', 'asin', 'item_name', 'sellersku', 'order_item_id', 'item_details', 'item_status', 'status', 'item_status', 'weight', 'item_qty'
    ];
    
    public function order()
    {
        return $this->belongsTo('App\Models\StoreOrder', 'store_order_id', 'id');
    }

    public function getOrderIdAttribute(){
        return $this->store_order_id;
    }

    public function getItemDetailsAttribute($value) {
        return json_decode($value);
    }
    
    public function getOrderTitleAttribute()
    {     
       if(!empty($this->item_details)) {
        return $this->item_details->Title;
       }              
    }
   
}
