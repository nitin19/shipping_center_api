<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreOrderProcessor extends Model
{
    protected $table = 'store_order_processor'; 
    protected $fillable = [
        'store_id', 'processor_id', 'owner_id', 'status'
    ];

    public function store()
    {
        return $this->belongsTo(Store::class);
    }
    
}
