<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Warehouses extends Model
{
    protected $table = 'warehouses'; 
    protected $fillable = [
        'store_owner_id','warehouse_name','warehouse_address','warehouse_city','warehouse_state','warehouse_country','warehouse_phone','warehouse_image','status','created_at','updated_at'
    ];

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function processor()
    {
        return $this->belongsTo(User::class);
    }
    
}