<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
  
  const CREATED_AT = 'created';

  const UPDATED_AT = 'updated';

  protected $table = 'states';

  protected $fillable = [];

  
}
