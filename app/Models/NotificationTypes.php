<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationTypes extends Model
{
    protected $table = 'notification_types'; 
    protected $fillable = [
        'name',' slug'
    ];
}