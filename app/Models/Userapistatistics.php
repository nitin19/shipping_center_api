<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Userapistatistics extends Model
{
  
  const CREATED_AT = 'created';

  const UPDATED_AT = 'updated';

  protected $table = 'user_api_statistics';

  protected $fillable = ['user_id', 'resource_path', 'count_requests', 'created', 'updated'];

  
}
