<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CardVerifications extends Model
{

  protected $table = 'card_verifications';

  protected $fillable = ['card_id', 'transaction_id_1', 'transaction_id_2', 'date', 'amount_1', 'amount_2', 'attempt', 'attempt_data', 'refund_1', 'refund_2'];

  public $timestamps = false; 
}
