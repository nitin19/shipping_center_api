<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Apirequests extends Model
{

  const CREATED_AT = 'created';

  const UPDATED_AT = 'updated';

  protected $table = 'apirequests';

  protected $fillable = ['user_id', 'active', 'created', 'updated', 'text', 'site_address', 'plan_time', 'parcels_amount'];

}
