<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\User;
use App\Models\Profile;
use Config;

class VerifyPhone
{
  public function __construct()
  {
    $this->nexmo_key  = Config::get('nexmo.api_key');
    $this->nexmo_secret = Config::get('nexmo.api_secret');
  }


  public function handle(UserRegistered $event)
  {
    //get user from database
    $user = User::where('id', $event->user->id)->first();
    //get user phone from database
    $number = $user->profile->phone;
    // create a nexmo client
    $client = new \Nexmo\Client(new \Nexmo\Client\Credentials\Basic($this->nexmo_key, $this->nexmo_secret));  
    // start phone verification
    try{
      $verification = $client->verify()->start(
      [
        'number' => $number,
        'brand'  => 'VIPparcel'
      ]);
      $sms_request_id = $verification->getRequestId(); // needs to be stored in user table
     
      $user->phone_verification_request_id = $sms_request_id;
      $user->update();
    }catch (\Exception $e) {
      $verification = $e->getEntity(); 
      $profile->phone_number_info_error = $verification['error_text'];
      $profile->update();    
    return response()->json(['error' => $verification['error_text'], 'code' =>$verification['status']]);
    }
  }
}
