<?php

use Illuminate\Http\Request;

Route::group(['middleware' => ['api','cors']], function ($router) {
  
 // Route::get('me', 'Api\AuthController@me');
  Route::post('login', 'Api\AuthController@login');
  Route::post('logout', 'Api\AuthController@logout');
  Route::post('register', 'Api\AuthController@register');
  Route::get('getCountries', 'Api\CountryController@get_Countries');
  Route::get('getStates', 'Api\StateController@get_States');

  Route::post('/password/create', 'Api\PasswordResetController@create');
  Route::get('/password/find/{token}', 'Api\PasswordResetController@find');
  Route::post('/password/reset', 'Api\PasswordResetController@reset');
  Route::get('getMailClasses', 'Api\MailClassController@get_Classes');

  Route::get('/stores/ebayUserToken', 'Api\StoreController@ebayUserToken');
  Route::get('/payment/status/{userid}', 'Api\Payment\PaymentController@getPaymentStatus');
  Route::get('/stores/shopifyUserToken', 'Api\StoreController@shopifyUserToken');

});

Route::group(['middleware' => ['jwt.verify','cors']], function() {

    Route::post('refresh', 'Api\AuthController@refresh');
    Route::post('verify-sms', 'Api\AuthController@smsVerify');
    Route::post('resend-sms', 'Api\AuthController@resendSms');

    Route::get('user', 'Api\UserController@getAuthenticatedUser');
    Route::put('update', 'Api\UserController@put_update');
    Route::post('requesttoken', 'Api\UserController@post_apitoken');
    Route::get('gettoken', 'Api\UserController@get_apitoken');

    Route::get('/accounts/card/getList', 'Api\Creditcard\CardsInfoController@getList');
    Route::post('/accounts/card/add', 'Api\Creditcard\CardsInfoController@create');
    Route::post('/accounts/card/verify', 'Api\Creditcard\CardsInfoController@card_verification');
    Route::post('/accounts/card/chargeverify', 'Api\Creditcard\CardsInfoController@charge_verification');
    Route::get('/accounts/card/iscardverified', 'Api\Creditcard\CardsInfoController@IsCardVerified');
    Route::get('/accounts/card/details/', 'Api\Creditcard\CardsInfoController@getdetails');
    Route::post('/accounts/card/delete', 'Api\Creditcard\CardsInfoController@deletecard');
    Route::post('/accounts/card/default', 'Api\Creditcard\CardsInfoController@defaultcard');
    Route::get('/accounts/card/getverifycard', 'Api\Creditcard\CardsInfoController@getverifycard');

    Route::post('/payment/paypal', 'Api\Payment\PaymentController@payWithpaypal');
    Route::post('/payment/card', 'Api\Payment\PaymentController@payWithcards');
    Route::get('/payment/balance', 'Api\Payment\PaymentController@getbalance');
    
});

Route::group(['middleware' => ['jwt.verify','cors'], 'prefix' => 'marketplace'], function(){
    Route::get('list', 'Api\MarketplaceController@list');  
    Route::post('search', 'Api\MarketplaceController@search');
});

Route::group(['middleware' => ['jwt.verify','cors'],'prefix' => 'stores'], function(){
    Route::post('save', 'Api\StoreController@saveCredentials');
    Route::get('myStores', 'Api\StoreController@myStores');
    Route::post('syncStores/{storeSlug}', 'Api\StoreController@syncStores');
    Route::get('storeOrders/{storeSlug}', 'Api\StoreOrderController@getStoreOrders');
    Route::get('storeOrders', 'Api\StoreOrderController@getAllStoreOrders');
    Route::post('orderpicklist', 'Api\StoreOrderController@getOrderPickList');    
    Route::get('order/details/{orderId}', 'Api\StoreOrderController@getOrderDetails');
    Route::post('checkAccountStatus', 'Api\StoreController@checkAccountStatus');
    Route::post('search', 'Api\StoreController@search');
    Route::post('ebayAuthUrl', 'Api\StoreController@ebayGetAuthrozieRedirectUrl');
    Route::post('ebayProductShipped', 'Api\StoreController@ebayProductShipped');
    Route::post('deletestore/{storeSlug}', 'Api\StoreController@deletestore');
    Route::get('orderitemdetail', 'Api\StoreOrderController@OrderItemDetail');
    Route::get('allstores', 'Api\StoreController@allStores');
    Route::get('getdetail', 'Api\StoreController@getdetail');
});

Route::group(['middleware' => ['jwt.verify','cors'],'prefix' => 'orders'], function(){  
    Route::post('search', 'Api\StoreOrderController@searchOrders');
    Route::get('status', 'Api\StoreOrderController@getOrderStatus');
    Route::post('changestatus', 'Api\StoreOrderController@updateorder');
    Route::post('changeItemstatus', 'Api\StoreOrderController@updateorderItem');
});


Route::group(['middleware' => ['jwt.verify','cors'],'prefix' => 'orderprocessor'], function(){
    Route::get('myprocessors', 'Api\OrderProcessorController@myProcessors');
    Route::get('viewprocessors/{id}', 'Api\OrderProcessorController@viewProcessor');
    Route::post('save', 'Api\OrderProcessorController@saveProcessor');
    Route::post('update/{id}', 'Api\OrderProcessorController@updateProcessor');
    Route::post('suspend/{id}', 'Api\OrderProcessorController@suspendProcessor');
    Route::post('allorders', 'Api\OrderProcessorController@allOrders');
    Route::post('assignstores', 'Api\OrderProcessorController@assignStores');
});

Route::group(['middleware' => ['jwt.verify','cors'],'prefix' => 'print'], function(){
    Route::get('picklist', 'Api\PrintController@picklist');
    Route::get('packaginglist', 'Api\PrintController@packagingList');
});

Route::group(['middleware' => ['jwt.verify','cors'],'prefix' => 'warehouse'], function(){
    Route::post('save', 'Api\WarehouseController@saveWarehouse');
    Route::post('update/{id}', 'Api\WarehouseController@updateWarehouse');
    Route::get('getwarehouse', 'Api\WarehouseController@getWarehouse');
    Route::post('suspended/{id}', 'Api\WarehouseController@deleteWarehouse');
    Route::post('viewwarehouse/{id}', 'Api\WarehouseController@viewWarehouse');
    Route::get('getwarehousecount', 'Api\WarehouseController@getMyWarehousesCount');
});
/* ************ For Processor Dashboard ******************************** */
Route::group(['middleware' => ['jwt.verify','cors'],'prefix' => 'processor'], function(){
    Route::get('storesCount', 'Api\ProcessorDashboardController@getStoreCount');
    Route::get('productsCount', 'Api\ProcessorDashboardController@getProductsCount');
    Route::get('ordersCount', 'Api\ProcessorDashboardController@getOrdersCount');
    Route::get('warehouseslist', 'Api\ProcessorDashboardController@getWarehousesList');
    Route::get('processorStores', 'Api\StoreController@processorStores');
    Route::get('proStores', 'Api\StoreController@proStores');
    Route::get('processorOrders', 'Api\StoreOrderController@processorStoreOrders');
});

/* ************ For Notification Section ******************* */
Route::group(['middleware' => ['jwt.verify','cors'],'prefix' => 'notification'], function(){
    Route::get('notify', 'Api\NotificationController@getNotify');
    Route::post('readnotify', 'Api\NotificationController@readNotify');
    Route::post('deletenotify/{id}', 'Api\NotificationController@deleteNotify');
});

Route::group(['middleware' => ['jwt.verify','cors'],'prefix' => 'label'], function(){
    Route::get('calculate', 'Api\LabelController@calculate');
});

Route::group(['middleware' => ['jwt.verify','cors'],'prefix' => 'dashboard'], function(){
    Route::get('storesCount', 'Api\DashboardController@getStoreCount');
    Route::get('ordersCount', 'Api\DashboardController@getOrdersCount');
    Route::get('productsCount', 'Api\DashboardController@getProductsCount');
    Route::get('warehousesCount', 'Api\DashboardController@getWarehousesCount');
    Route::get('warehouseslist', 'Api\DashboardController@getWarehousesList');
    Route::get('orderstatus', 'Api\DashboardController@getOrderStatus');

    Route::get('completedordercount', 'Api\DashboardController@getOrdersComplete');
    Route::get('pendingordersCount', 'Api\DashboardController@getOrdersPending');
    Route::get('partialordersCount', 'Api\DashboardController@getOrdersPartial');
    Route::get('newordersCount', 'Api\DashboardController@getOrdersnew');
    Route::get('orderprocessorcount', 'Api\DashboardController@getOrderprocessorCount');
    Route::get('allordercount', 'Api\DashboardController@getallOrderCount');
    Route::get('totalordercount', 'Api\DashboardController@getTotalOrderCount');
    
});

