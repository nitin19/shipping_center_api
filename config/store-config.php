<?php

return [

	// TO segerate small seller from Big seller we need time interval + orders 
	'SellerGetNewOrderTime' => '- 7 days',
	'SellerNewOrderCount' => 50, // More then this order sell will be converted to Big Seller automatically per week (Cron Job)

	// Time after which the seller will get Notifications that order has not shipped
	'SellerNewOrderNotificationAfterTime' => '- 1 days',
	'SellerAverageTimeForOrderVolume' => '- 7 days',

	// Minimum Order quantity in warehouse to notify seller for inventory decline 
	'inventoryDeclineQuantity' => '5',

];
