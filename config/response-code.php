<?php

return [
     /*
    |--------------------------------------------------------------------------
    | Default Response Code For REST API 
    |--------------------------------------------------------------------------
    |
    | This option controls the default REST API response code
    |
    |
    */
    'SUCCESS' => '200',
    'CREATED' => '201',
    'UPDATED' => '200',
    'DELETED' => '200',
    'NOTFOUND' => '404',
    'UNAUTHORISED' =>'401',
    'ERROR' => '400',
    'PERMISSION_DENIED' =>'403'
];
