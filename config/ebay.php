<?php

if(env('EBAY_SANDBOX_MODE')=='1'){
    return $credentials = array(
    'scope' => 'https://api.ebay.com/oauth/api_scope/sell.account',
    'clientID' => 'binaryda-vipparce-SBX-960a78903-9b686a8d',
    'clientSecret' => 'SBX-60a789039ac9-cf7c-42fc-9aa2-7123',
    'ruName' => 'binary_data-binaryda-vippar-wabsg',
    'devID' => 'ff4962c7-3013-4223-a31e-1101f1759fa4',
    'authTokenUrl' => 'https://api.sandbox.ebay.com/identity/v1/oauth2/token',
    'authUrl' => 'https://signin.sandbox.ebay.com/authorize',
    'sandbox' => true,
    'siteId' => env('EBAY_SITE_ID','0'),
    );
} else {
    return $credentials = array(
    'clientID' => 'binaryda-vipparce-PRD-460bef6c5-0d0c844b',
    'clientSecret' => 'PRD-60bef6c5cb71-7a1d-4948-9dcd-5784',
    'ruName' => 'binary_data-binaryda-vippar-znkzktx',
    'devID' => 'ff4962c7-3013-4223-a31e-1101f1759fa4',
    'sandbox' => false,
    'authUrl' => 'https://auth.ebay.com/oauth2/',
    'authTokenUrl' => 'https://api.ebay.com/identity/v1/oauth2/token',
    'siteId' => env('EBAY_SITE_ID','0'),
    );
}

//return [
    /*
    |--------------------------------------------------------------------------
    | Modes
    |--------------------------------------------------------------------------
    |
    | This package supports sandbox and production modes.
    | You may specify which one you're using throughout
    | your application here.
    |
    | Supported: "sandbox", "production"
    |
    */

    //'mode' => env('EBAY_MODE', 'sandbox'),


    /*
    |--------------------------------------------------------------------------
    | Site Id
    |--------------------------------------------------------------------------
    |
    | The unique numerical identifier for the eBay site your API requests are to be sent to.
    | For example, you would pass the value 3 to specify the eBay UK site.
    | A complete list of eBay site IDs is available
    |(http://developer.ebay.com/devzone/finding/Concepts/SiteIDToGlobalID.html).
    |
    |
    */

    //'siteId' => env('EBAY_SITE_ID','0'),

    /*
    |--------------------------------------------------------------------------
    | KEYS
    |--------------------------------------------------------------------------
    |
    | Get keys from EBAY. Create an app and generate keys.
    | (https://developer.ebay.com)
    | You can create keys for both sandbox and production also
    | User token can generated here.
    |
    */

    // 'sandbox' => [
    //     'credentials' => [
    //         'devId' => env('EBAY_SANDBOX_DEV_ID'),
    //         'appId' => env('EBAY_SANDBOX_APP_ID'),
    //         'certId' => env('EBAY_SANDBOX_CERT_ID'),
    //     ],
    //     'authToken' => env('EBAY_SANDBOX_AUTH_TOKEN'),
    //     'oauthUserToken' => env('EBAY_SANDBOX_OAUTH_USER_TOKEN'),
    // ],
   
    /*
    'sandbox' => [
        'credentials' => [
            'devId' => '17e14bb9-4f8f-44de-ac93-4003afd1dbf0',
            'appId' => 'Shipping-Shipping-SBX-282083935-dde1e643',
            'certId'=> 'SBX-820839357b20-baba-4ad1-aa9e-fa95',
        ],
        'authToken' => env('EBAY_SANDBOX_AUTH_TOKEN'),
        'oauthUserToken' => 'AgAAAA**AQAAAA**aAAAAA**sv6tWw**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4ajCpWDqQidj6x9nY+seQ**krUEAA**AAMAAA**6OeGbbTQ8YLUu8Gn9gPp35UxKKarP/F4DqILhc3WX3MfKMXADshlVr5PQvqElUPNAxr06Aj5fPPzbtGAxM7Za0ucDVjKhvGeKuWAYQs0ICP2LDzW7qt+h/AyF/M7TWDwBhqLa4G950GrFhWsQEM3nCLsUVH3VZhaN8LnDhVi+fIotmkF5XdOsh5Ci0f1oDrOQziG1kpXZv+5C6bs46wdkR3JcoSHzBnGl8KsGVnf30WRerigoKKChO2j+g5w6JAzg2fHCVCcXzv2A1QhW3AxSJuIAxgeDMBFPWx5PNCVBd9eWgGeXi1TsByS09ZZltzWviIuzJ0Oox1LguWskY4CXNyNv3xzBjp9Yr5CTaVyDactm5fJD1haatvjsOi+1G3/qsoWj2tHYelkkvcYTFH8Nxa3sNcmb9gqQkWygH6grDI14fHU6WVudRjdgTnoJaVygI2paLnOdeNKsb4xRdfpVJT4g0J+XNUwk/hCUJPHVXorJm8xAxrHEKnDnrNnENYnzd9Dn7sG931sRm397E3XjtHvj0X227/puOw0oKImE8vWcX1Bkmvcz1uhvvqPi+KBVwvxfbllzQHUHlVK5mw//FftHIsuM/gncARww45mMqSP0xoEPg/2KFHj7Q8BAoHYTS2X2+cT4Jqx13JzOn3DAI9Qrzp4Q1MMk39GTJoIPeIdluiYL5icY+RbzU4fMFFKaTBFpEvN99Wy6Yd1bjSUElXuacbyBKFharRBFT+TxyM7PoOrka+oeqJzuhf7TOo/',
    ],
    'production' => [
        'credentials' => [
            'devId' => env('EBAY_PROD_DEV_ID'),
            'appId' => env('EBAY_PROD_APP_ID'),
            'certId' => env('EBAY_PROD_CERT_ID'),
        ],
        'authToken' => env('EBAY_PROD_AUTH_TOKEN'),
        'oauthUserToken' => env('EBAY_PROD_OAUTH_USER_TOKEN'),
    ]
    */


    /* Ebay SandBoxAccountDetails */

    
    /*'scope' => 'https://api.ebay.com/oauth/api_scope/sell.account',
    'clientID' => 'binaryda-vipparce-SBX-960a78903-9b686a8d',
    'clientSecret' => 'SBX-60a789039ac9-cf7c-42fc-9aa2-7123',
    'ruName' => 'binary_data-binaryda-vippar-mjhbqrtz',
    'devID' => 'ff4962c7-3013-4223-a31e-1101f1759fa4',
    'authTokenUrl' => 'https://api.sandbox.ebay.com/identity/v1/oauth2/token',
    'authUrl' => 'https://signin.sandbox.ebay.com/authorize',
    'sandbox' => true,
    */

    /* Ebay ProductionAccountDetails */

    /*'clientID' => 'binaryda-vipparce-PRD-460bef6c5-0d0c844b',
    'clientSecret' => 'PRD-60bef6c5cb71-7a1d-4948-9dcd-5784',
    'ruName' => 'binary_data-binaryda-vippar-znkzktx',
    'devID' => 'ff4962c7-3013-4223-a31e-1101f1759fa4',
    'sandbox' => false,
    'authUrl' => 'https://auth.ebay.com/oauth2/',
    'authTokenUrl' => 'https://api.ebay.com/identity/v1/oauth2/token',
    'siteId' => env('EBAY_SITE_ID','0'),*/

//];