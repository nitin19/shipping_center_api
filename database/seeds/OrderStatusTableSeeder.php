<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class OrderStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_status')->insert([
            'name' => 'New',
            'slug' => 'new',
            'status' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('order_status')->insert([
            'name' => 'In Process',
            'slug' => 'in process',
            'status' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('order_status')->insert([
            'name' => 'Shipped',
            'slug' => 'shipped',
            'status' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('order_status')->insert([
            'name' => 'Cancel',
            'slug' => 'cancel',
            'status' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('order_status')->insert([
            'name' => 'Full Refund',
            'slug' => 'full_refund',
            'status' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('order_status')->insert([
            'name' => 'Partial Refund',
            'slug' => 'partial_refund',
            'status' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('order_status')->insert([
            'name' => 'Partial Shipped',
            'slug' => 'partial_shipped',
            'status' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('order_status')->insert([
            'name' => 'Completed',
            'slug' => 'completed',
            'status' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
