<?php

use Illuminate\Database\Seeder;

class NotificationTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('notification_types')->insert([
            'name' => 'New Order',
            'slug' => 'new_order',
        ]);
        DB::table('notification_types')->insert([
            'name' => 'Order not shipped',
            'slug' => 'order_not_shipped',
        ]);
        DB::table('notification_types')->insert([
            'name' => 'Order volume variation',
            'slug' => 'order_volume_variation',
        ]);
        DB::table('notification_types')->insert([
            'name' => 'Order shipment volume variation',
            'slug' => 'order_shipment_volume_variation',
        ]);
        DB::table('notification_types')->insert([
            'name' => 'Order volume product variation',
            'slug' => 'order_volume_product_variation',
        ]);
        DB::table('notification_types')->insert([
            'name' => 'API key setting',
            'slug' => 'apikey_setting',
        ]);
        DB::table('notification_types')->insert([
            'name' => 'Order packer notification',
            'slug' => 'order_packer',
        ]);
    }
}
