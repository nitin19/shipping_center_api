<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(OrderStatusTableSeeder::class);
         $this->call(StoreSystemsTableSeeder::class);
         $this->call(NotificationTypesTableSeeder::class);
    }
}
