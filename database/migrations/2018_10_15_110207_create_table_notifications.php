<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNotifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('notify_type',['0', '1', '2', '3'])->nullable()->comment="0=notification, 1=message, 2=call, 3=email";
            $table->integer('type')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('sender')->nullable();
            $table->integer('receiver')->nullable();
            $table->text('message')->nullable();
            $table->enum('status',['1', '0'])->default('1')->comment="1=Active,0=Deactive";
            $table->timestamps();
            $table->enum('is_read',['1', '0'])->default('0')->comment="1=read,0=unread";
            $table->enum('is_deleted',['1', '0'])->default('0')->comment="1=Deleted,0=Not Deleted";
            $table->timestamp('read_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
