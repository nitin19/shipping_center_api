<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_order_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_order_id');
            $table->string('asin', 191)->nullable();
            $table->string('item_name', 191)->nullable();
            $table->string('sellersku', 191)->nullable();
            $table->string('order_item_id', 191)->nullable();
            $table->text('item_details')->nullable();
            $table->enum('status',['1', '0'])->default('1')->comment="1=In Stock,0=Out of stock";
            $table->text('item_status', 191)->nullable();
            $table->string('weight', 191)->nullable();
            $table->string('item_qty', 191)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_order_items');
    }
}
