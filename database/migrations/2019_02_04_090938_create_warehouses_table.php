<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_owner_id')->nullable();
            $table->integer('store_order_id')->nullable();
            $table->text('warehouse_name')->nullable();
            $table->longText('warehouse_address')->nullable();
            $table->string('warehouse_city', 191)->nullable();
            $table->string('warehouse_state', 191)->nullable();
            $table->string('warehouse_country', 191)->nullable();
            $table->string('phone_number', 191)->nullable();
            $table->longText('warehouse_image')->nullable();
            $table->string('warehouse_lat', 191)->nullable();
            $table->string('warehouse_long', 191)->nullable();
            $table->enum('status',['1', '0'])->default('1')->comment="1=Active,0=Deactive";
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouses');
    }
}
