<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hash', 191)->nullable();
            $table->integer('vip_user_id')->nullable();
            $table->string('email', 191)->nullable();
            $table->string('username', 191)->nullable();
            $table->string('password', 191)->nullable();
            $table->integer('logins')->default('0');
            $table->integer('last_logins')->nullable();
            $table->tinyInteger('active')->default('1');
            $table->string('active_code', 191)->nullable();
            $table->string('phone_verification_request_id', 191)->nullable();
            $table->smallInteger('phone_verification_attempts')->nullable();
            $table->timestamp('phone_verification_last_attempt')->nullable();
            $table->string('new_pass_code', 191)->nullable();
            $table->timestamp('view_message')->nullable();
            $table->text('location_details')->nullable();
            $table->tinyInteger('update_referral_discount')->nullable();
            $table->decimal('lat', 10,5)->nullable();
            $table->decimal('lon', 10,5)->nullable();
            $table->string('salesman_code', 191)->nullable();
            $table->enum('type',['personal', 'business'])->default('personal');
            $table->timestamp('last_visit')->nullable();
            $table->integer('last_ipinfo_id')->nullable();
            $table->integer('register_ipinfo_id')->nullable();
            $table->bigInteger('ip_register')->nullable();
            $table->tinyInteger('test_mode')->default('0');
            $table->string('forgot_password_key', 191)->nullable();
            $table->timestamp('forgot_password_date')->nullable();
            $table->tinyInteger('subscription_disabled')->default('0');
            $table->integer('credit_card_subscription')->nullable();
            $table->tinyInteger('in_archive')->default('0');
            $table->string('hear_about_us', 191)->nullable();
            $table->decimal('insurance_commission', 5,2)->nullable();
            $table->tinyInteger('first_international_price_pro')->nullable();
            $table->integer('activation_expired')->nullable();
            $table->text('domestic_commission')->nullable();
            $table->text('domestic_commission_flat')->nullable();
            $table->text('domestic_commission_additional')->nullable();
            $table->text('domestic_commission_additional_flat')->nullable();
            $table->tinyInteger('autopayment_enabled')->default('1');
            $table->integer('credit_card_autopayment')->nullable();
            $table->text('international_discounts')->nullable();
            $table->tinyInteger('disable_check_print_label')->nullable();
            $table->text('note')->nullable();
            $table->tinyInteger('long_life_referrers')->default('0');
            $table->tinyInteger('verified')->default('1');
            $table->string('provider_account', 191)->nullable();
            $table->tinyInteger('enterprise_account')->default('0');
            $table->tinyInteger('referral_autoverification')->default('0');
            $table->tinyInteger('allow_card_american_express')->default('0');
            $table->tinyInteger('disabled_email_notify')->default('0');
            $table->tinyInteger('free_insured_provider_limit')->default('0');
            $table->tinyInteger('allow_ship_date')->default('0');
            $table->tinyInteger('is_flug_customer')->default('0');
            $table->decimal('balance', 10,2)->nullable();
            $table->integer('cnt_labels')->nullable();
            $table->tinyInteger('allow_min_dim_weight')->default('0');
            $table->decimal('commission_min_dim_weight', 10,2)->nullable();
            $table->tinyInteger('create_label_only_partner')->default('0');
            $table->tinyInteger('phone_verification_enabled')->default('1');
            $table->tinyInteger('is_blog_writer')->default('0');
            $table->tinyInteger('fedex_user')->default('0');
            $table->tinyInteger('can_print_international')->default('0');
            $table->string('ebay_auth_session_id', 191)->nullable();
            $table->string('referrer_url', 191)->nullable();
            $table->tinyInteger('is_required_dim_weight')->default('0');
            $table->tinyInteger('disable_check_payment_billing')->default('0');
            $table->integer('department_user_id')->nullable();
            $table->integer('last_label_id')->nullable();
            $table->integer('tariff_id')->nullable();
            $table->tinyInteger('custom_international_prices')->default('0');
            $table->tinyInteger('allow_custom_from_address_label')->default('0');
            $table->tinyInteger('first_class_commercial_plus')->default('0');
            $table->decimal('forc_domestic_fc_addit_commission', 8,2)->nullable();
            $table->text('inheritance_ignore_fields')->nullable();
            $table->integer('coupon_code_id')->nullable();
            $table->tinyInteger('coupon_code_is_used')->nullable();
            $table->decimal('enterprise_partner_commission', 8,2)->nullable();
            $table->decimal('enterprise_processing_fee', 8,2)->nullable();
            $table->decimal('sales_card_processing_fee', 4,2)->default('3.00');
            $table->decimal('sales_profit_percent', 4,2)->default('10.00');
            $table->string('gss_region', 191)->nullable();
            $table->tinyInteger('allow_same_credit_card')->default('1');
            $table->string('user_type', 191)->nullable();
            $table->timestamp('created')->nullable();
            $table->timestamp('updated')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
